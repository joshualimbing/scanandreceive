$(document).ready(function(){

	$('#next').click(function(){

		let dr_number 		= $('.select-dr').val();
		let del_location	= $('.select-del-to').val();
		let remarks 		= $('#Doc_Remarks').val();
		let requiredField 	= $(':input[required]:visible');
		let isComplete 		= true; 

		$(requiredField).each(function(){
			if($(this).val() == ""){
				isComplete = false;
			}
		})

		if(!isComplete){
			swal('Information', 'Complete all required fields', 'info');
		}
		else{
			swal({
				title				: "Please wait!",
				type 				: "info",
				showConfirmButton	: false
			});
			

			$.ajax({
				url 		: global.site_name + 'scan_ondemand/next',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'dr-number' 	: dr_number,
								'del-location' 	: del_location,
								'remarks' 		: remarks
							  },
				success 	: function(result){
					if(result.success){
						setTimeout(function(){
							window.location.href = global.site_name + 'scan_ondemand/item_entry?DRNo=' + result.dr_docno + '&delLoc=' + del_location; 
						}, 1000);
					}
					else{
						swal(result.title, result.message, result.type);
					}
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}

	});

	$('.select-del-to').select2({
		placeholder 	: 'Select Location',
		allowClear 		: true
	});

	$('.select-dr').select2({
		placeholder 	: 'Select DR Number',
		allowClear 		: true
	});

	$('[name="DH_DRNum"]').select2({
		placeholder 	: 'Select DR Number',
		allowClear 		: true,
		minimumInputLength: 2,
			ajax:
				{
					url: global.site_name + 'scan_ondemand/searchDR',
					dataType: 'json',
	                delay: 250,
	                data: function (params) {
	                  var queryParameters = {
	                           	'docno' 	: params.term
	                        ,   'action'        	: 'search'
	                    }
	                    return queryParameters;
	                },
	                processResults: function (data, params) {
	                    return {
	                        results: $.map(data, function(ref) {
	                            return {
	                                id: ref.DH_DRNum,
	                                text:ref['DH_DRNum']
	                            }
	                        })
	                    };
	                }
				}
	})

	$('.select-dr').change(function(){
		let dr_number 		= $('.select-dr').val();
		console.log(dr_number);


		if(dr_number != 'NEW DR' && dr_number != ''){
			$.ajax({
				url 		: global.site_name + 'scan_ondemand/getHeaderData',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
								'dr-number' 	: dr_number
							  },
				success 	: function(result){
					$('.select-del-to').val(result.PKFK_Location_Destin).trigger('change');
					$('#Doc_Remarks').text(result.ODH_Remarks);
					$('.select-del-to').prop('disabled', true);
					$('#Doc_Remarks').attr('readonly', true);
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			});
		}
		else{
			$('.select-del-to').val('').trigger('change');
			$('#Doc_Remarks').val('');
			$('.select-del-to').prop('disabled', false);
			$('#Doc_Remarks').attr('readonly', false);
		}
	})

	$('.datepicker').datepicker({
		todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "mm/dd/yyyy"
	});

	$('#reprocess').click(function() {
		
		$('[name="DH_DRNum"]').val('').trigger('change');
		$('#reprocess-list tbody tr').empty();
		$('#reprocess-details').prop('disabled', true)

		$('#reprocess-dr').modal('show');
		
	});

	$('[name="DH_DRNum"]').change(function(event) {

		let dr_docno = $(this).val();

		if ($(this).val() == '' || $(this).val() === null) {
			$('#reprocess-list tbody tr').empty();
			$('#reprocess-details').prop('disabled', true)
		}
		else{
			$('#reprocess-details').prop('disabled', false)

			$.ajax({
				url 		: global.site_name + 'scan_ondemand/getDetailsbyDR',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
					'dr-docno' 	: dr_docno
				},
				success 	: function(result){
					$('#reprocess-list tbody tr').empty();

					let reprocess_html = '';

					$(result).each(function(index){
						reprocess_html += '<tr class="reprocess-details">' + 
										  '<td style="font-size: 12px; padding: 5px">' + result[index].FK_InvMaster_Code + '</td>' + 
										  '<td style="font-size: 12px; padding: 5px">' + result[index].IM_Item_Desc + '</td>' + 
										  '<td style="font-size: 12px; padding: 5px">' + result[index].IM_ItemLocation + '</td>' + 
										  '<td class="text-right" style="font-size: 12px; padding: 5px">' + result[index].DT_DelQty + '</td>' +
										  '</tr>';
					});

					$('#reprocess-list tbody').html(reprocess_html);
				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			})
		}
	});

	$('#reprocess-details').click(function(){
		let dr_docno = $('[name="DH_DRNum"]').val();

		swal({
			title				: "Reprocess DR?",
			type 				: "warning",
			showCancelButton	: true,
			confirmButtonText	: "Yes",
			confirmButtonColor	: "#3989C8",
			closeOnConfirm		: false
		}, function(isConfirm){

			if(isConfirm){
				swal({
					title				: "Please wait!",
					type 				: "info",
					showConfirmButton	: false
				});

				$.ajax({
					url 		: global.site_name + 'scan_ondemand/reprocess',
					dataType 	: 'json',
					type 		: 'POST',
					data 		: {
						'dr-docno' 	: dr_docno
					},
					success 	: function(result){
						if(result.success){
							$('[name="DH_DRNum"]').val('').trigger('change');
							swal('Success', 'Reprocessed document successfully', 'success');
						}
						else{
							swal(result.title, result.message, result.type)
						}
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				})
			}
		})
	});

});