$(document).ready(function(){

	var is_closed_carton = false; 

	function init(){

		$(window).on("blur focus", function(e) {
		    location.reload(true);
		})

		// Scan through mobile camera or webcam - Created by Joshua Limbing (09/07/2020)
		
		load_quagga();
		$('#scanButton').click(function(event) {
			Quagga.start();
			$('#scan-barcode').modal('show')
		});

		$('#close-barcode').click(function(event) {
			Quagga.pause();
			$('#scan-barcode').modal('hide')
		});

		// End Scan

		$('#unpack-pl').click(function(){
			let status = $('#picklist-header-list tbody tr td.scan-status').text();

			if(status == 'Scanning'){ 
				$('#picklist-header-list tbody tr td.scan-status').text('Unpack');				
				$('#picklist-header-list tbody tr td.scan-status').css('color', '#FF0101');
			}
			else{
				$('#picklist-header-list tbody tr td.scan-status').text('Scanning');
				$('#picklist-header-list tbody tr td.scan-status').css('color', '#000000');
			}
		});

		$('[name="Barcode_Qty"]').keypress(function(e){
			if(e.keyCode == 13){
				$('[name="FK_IM_UPCCode"]').focus();
			}
		});

		// Recoded Barcode using UPC Code - Created by Joshua Limbing (11/20/2019)

		$('[name="FK_IM_UPCCode"]').keydown(function(e) {
			if(e.keyCode == 13){
				let status = $('#picklist-header-list tbody tr td.scan-status').text();
				let barcode = $('[name="FK_IM_UPCCode"]').val().trim();
				let qty = $('[name="Barcode_Qty"]').val();
				let upc_code_arr = [];
				$('.spinner').show();
				$('input').prop('readonly', true);
				$('[name="FK_IM_UPCCode"]').blur();

				// If no barcode present
				if(barcode == ""){
					$('.spinner').hide();
					$('input').prop('readonly', false);
					$('[name="FK_IM_UPCCode"]').val('');
					$('[name="Barcode_Qty"]').val(1);
					swal("Information", "Please input barcode to proceed", "info");
					// $('[name="FK_IM_UPCCode"]').focus();
				}
				// If barcode is present
				else{
					// If qty == 0 or blank
					if(qty == "" || qty <= 0){
						$('.spinner').hide();
						$('input').prop('readonly', false);
						$('[name="FK_IM_UPCCode"]').val('');
						$('[name="Barcode_Qty"]').val(1);
						swal("Information", "Invalid or Zero Quantity", "info");
						// $('[name="FK_IM_UPCCode"]').focus();
					}
					// If qty > 0
					else{
						// Get all UPC Code
						$('#picklist-detail-list tbody tr').each(function(){
							let upc_code = $(this).find('[data-upc-code]').data('upc-code');

							upc_code_arr.push(upc_code.toString());
						});

						if($.inArray(barcode, upc_code_arr) !== -1){
							let upc_length = $('#picklist-detail-list tbody tr').find('[data-upc-code="'+ barcode +'"]').length;

							$.ajax({
								url			: global.site_name + 'scan_pack/checkReqQty',
								dataType	: 'json',
								type 		: 'POST',
								data 		: {
												'picklist-no'		: picklist,
												'carton-no'			: carton_no,
												'barcode'			: barcode,
												'qty'				: parseFloat(qty),
												'status' 			: status
											  },
								success 	: function(result){
									if(result.success){
										
										$(result.required_qty).each(function(index, el) {
											$('#picklist-detail-list tbody tr').find('[data-loc-barcode = "' + result.required_qty[index].PKFK_Location_Source + ' - ' + barcode +'"]').text(number_format(result.required_qty[index].RequiredQty, 0, ".", ""));
										});

										$('#picklist-detail-list tbody tr').each(function() {
												let current_row = $(this).closest('.picklist-details');
												let loc_source = $(this).find('[data-loc-source]').data('loc-source');
												let upc_code = $(this).find('[data-upc-code]').data('upc-code');
												let packed_qty = parseFloat($(this).find('[name="pck-qty"]').text());
												let req_qty = parseFloat($(this).find('[name="required-qty"]').text());
												let item_code = $(this).find('[data-item-code]').data('item-code');
												let item_desc = $(this).find('[data-item-desc]').data('item-desc');
												let cat_code = $(this).find('[data-cat-code]').data('cat-code');
												let color = $(this).find('[data-color-code]').data('color-code');
												let size = $(this).find('[data-size-code]').data('size-code');
												let total_pck = 0;
												let total_req = 0;

												if(barcode == upc_code.toString()){
													let condition = '';

													if(status == 'Scanning'){
														condition = (req_qty > 0 == true ? true : false);
													}
													else{
														condition = ((packed_qty - parseFloat(qty) < 0) == false ? true : false);
													}

													if(upc_length > 1){
														$('#picklist-detail-list tbody tr').css('background', '#FFFFFF')	// Clear row backgroud color first
														$(this).css('background', '#ECECEC'); // Highlight item


														if(condition){
															// Change Pick Note Header status and Update Carton Tables
															$.ajax({
																url			: global.site_name + 'scan_pack/changePNHStatus',
																dataType	: 'json',
																type 		: 'POST',
																data 		: {
																				'picklist-no'		: picklist,
																				'carton-no'			: carton_no,
																				'loc-dest'			: loc_destin,
																				'loc-source'		: loc_source,
																				'total-pck'			: parseFloat($('#total-pck').text()),
																				'packed-qty'		: packed_qty,
																				'required-qty'		: req_qty - parseFloat(qty),
																				'qty'				: parseFloat(qty),
																				'scanning'			: 1,
																				'carton-status'		: 'SCN',
																				'upc-code'			: upc_code,
																				'item-code'			: item_code,
																				'item-desc'			: item_desc,
																				'cat-code'			: cat_code,
																				'color'				: color,
																				'size'				: size,
																				'status'			: status
																			  },
																success 	: function(output){
																	if(output.success){
																		current_row.find('[name="pck-qty"]').text(number_format(output.packed_qty, 0 , ".", ""));
																		current_row.find('[name="required-qty"]').text(output.required_qty);
																		let total_rq = 0;
																		let total_pk = 0;

																		$('#picklist-detail-list tbody tr').each(function() {
																			let pk = parseFloat($(this).find('[name="pck-qty"]').text());
																			let rq = parseFloat($(this).find('[name="required-qty"]').text());

																			total_pk += pk;
																			total_rq += rq;
																		});

																		console.log(total_rq);

																		$('#total-pck').text(total_pk);
																		$('#total-req').text(total_rq);

																		if(total_rq > 0){
																			$('.spinner').hide();
																			console.log('aaa')
																			$('input').prop('readonly', false);
																			$('[name="FK_IM_UPCCode"]').val('');
																			$('[name="FK_IM_UPCCode"]').focus();
																			$('[name="Barcode_Qty"]').val(1);
																		}
																		else{
																			swal({
																				title				: "Please wait!",
																				text 				: "Closing Picklist...",
																				type 				: "info",
																				showConfirmButton	: false
																			});

																			$('.spinner').hide();
																			$('input').prop('readonly', false);
																			$('[name="FK_IM_UPCCode"]').val('');
																			$('[name="FK_IM_UPCCode"]').focus();
																			$('[name="Barcode_Qty"]').val(1);

																			$.ajax({ // Close Carton
																				url			: global.site_name + 'scan_pack/closeCarton',
																				dataType	: 'json',
																				type 		: 'POST',
																				data 		: {
																								'picklist-no'		: picklist,
																								'carton-no'			: carton_no
																							  },
																				success		: function(data){
																					if(data.success){
																						$('[name="FK_IM_UPCCode"]').attr('readonly', true);
																						$('[name="Barcode_Qty"]').attr('readonly', true);

																						$.ajax({ // Close PL
																							url			: global.site_name + 'scan_pack/closePL',
																							dataType	: 'json',
																							type 		: 'POST',
																							data 		: {
																											'picklist-no'		: picklist,
																											'carton-no'			: carton_no
																										  },
																							success 	: function(rslt){
																									
																								if(rslt.success){
																									
																									swal({
																										type 				: "success",
				 																						title 				: "Closed PL Successfully",
				 																						showConfirmButton 	: false
				 																					});

				 																					setTimeout(function(){
				 																						window.location.href = global.site_name + 'scan_pack';
				 																					}, 1000);
																								}
																								else{
																									swal(output.title, output.message, output.type);
																									current_row.find('[name="required-qty"]').text(output.required_qty);
																									play();
																								}

																							},
																							error 		: function(jqXHR){
																								swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
																							}
																						}); // Close PL End
																					}
																					else{
																						swal(data.title, data.message, data.type);
																					}
																				}
																			}); // Close Carton End
																		}

																	}
																	else{
																		current_row.find('[name="pck-qty"]').text(number_format(output.packed_qty, 0 , ".", ""));
																		current_row.find('[name="required-qty"]').text(output.required_qty);
																		swal(output.title, output.message, output.type);
																		$('.spinner').hide();
																		$('input').prop('readonly', false);
																		$('[name="FK_IM_UPCCode"]').val('');
																		$('[name="FK_IM_UPCCode"]').focus();
																		$('[name="Barcode_Qty"]').val(1);
																		// $('[name="FK_IM_UPCCode"]').focus();
																		play();
																	}
																},
																error 		: function(jqXHR){
																	swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
																	$('.spinner').hide();
																	$('input').prop('readonly', false);
																	$('[name="FK_IM_UPCCode"]').val('');
																	$('[name="FK_IM_UPCCode"]').focus();
																	$('[name="Barcode_Qty"]').val(1);
																}
															});

															return false;
														}
													}
													else{
														$('#picklist-detail-list tbody tr').css('background', '#FFFFFF')	// Clear row backgroud color first
														$(this).css('background', '#ECECEC'); // Highlight item

														if(condition){
															// Change Pick Note Header status and Update Carton Tables
															$.ajax({
																url			: global.site_name + 'scan_pack/changePNHStatus',
																dataType	: 'json',
																type 		: 'POST',
																data 		: {
																				'picklist-no'		: picklist,
																				'carton-no'			: carton_no,
																				'loc-dest'			: loc_destin,
																				'loc-source'		: loc_source,
																				'total-pck'			: parseFloat($('#total-pck').text()),
																				'packed-qty'		: packed_qty,
																				'required-qty'		: req_qty - parseFloat(qty),
																				'qty'				: parseFloat(qty),
																				'scanning'			: 1,
																				'carton-status'		: 'SCN',
																				'upc-code'			: upc_code,
																				'item-code'			: item_code,
																				'item-desc'			: item_desc,
																				'cat-code'			: cat_code,
																				'color'				: color,
																				'size'				: size,
																				'status'			: status
																			  },
																success 	: function(output){
																	if(output.success){
																		current_row.find('[name="pck-qty"]').text(number_format(output.packed_qty, 0 , ".", ""));
																		current_row.find('[name="required-qty"]').text(output.required_qty);
																		let total_rq = 0;
																		let total_pk = 0;

																		$('#picklist-detail-list tbody tr').each(function() {
																			let pk = parseFloat($(this).find('[name="pck-qty"]').text());
																			let rq = parseFloat($(this).find('[name="required-qty"]').text());

																			total_pk += pk;
																			total_rq += rq;
																		});

																		console.log(total_rq);

																		$('#total-pck').text(total_pk);
																		$('#total-req').text(total_rq);

																		if(total_rq > 0){
																			$('.spinner').hide();
																			$('input').prop('readonly', false);
																			$('[name="FK_IM_UPCCode"]').val('');
																			$('[name="Barcode_Qty"]').val(1);
																			$('[name="FK_IM_UPCCode"]').focus();
																		}
																		else{
																			swal({
																				title				: "Please wait!",
																				text 				: "Closing Picklist...",
																				type 				: "info",
																				showConfirmButton	: false
																			});

																			$('.spinner').hide();
																			$('input').prop('readonly', false);
																			$('[name="FK_IM_UPCCode"]').val('');
																			$('[name="Barcode_Qty"]').val(1);

																			$.ajax({ // Close Carton
																				url			: global.site_name + 'scan_pack/closeCarton',
																				dataType	: 'json',
																				type 		: 'POST',
																				data 		: {
																								'picklist-no'		: picklist,
																								'carton-no'			: carton_no
																							  },
																				success		: function(data){
																					if(data.success){
																						$('[name="FK_IM_UPCCode"]').attr('readonly', true);
																						$('[name="Barcode_Qty"]').attr('readonly', true);

																						$.ajax({ // Close PL
																							url			: global.site_name + 'scan_pack/closePL',
																							dataType	: 'json',
																							type 		: 'POST',
																							data 		: {
																											'picklist-no'		: picklist,
																											'carton-no'			: carton_no
																										  },
																							success 	: function(rslt){
																									
																								if(rslt.success){
																									
																									swal({
																										type 				: "success",
				 																						title 				: "Closed PL Successfully",
				 																						showConfirmButton 	: false
				 																					});

				 																					setTimeout(function(){
				 																						window.location.href = global.site_name + 'scan_pack';
				 																					}, 1000);
																								}
																								else{
																									swal(output.title, output.message, output.type);
																									current_row.find('[name="required-qty"]').text(output.required_qty);
																									play();
																								}

																							},
																							error 		: function(jqXHR){
																								swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
																							}
																						}); // Close PL End
																					}
																					else{
																						swal(data.title, data.message, data.type);
																					}
																				},
																				error 		: function(jqXHR){
																					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
																				}
																			}); // Close Carton End
																		}

																	}
																	else{
																		current_row.find('[name="pck-qty"]').text(number_format(output.packed_qty, 0 , ".", ""));
																		current_row.find('[name="required-qty"]').text(output.required_qty);
																		swal(output.title, output.message, output.type);
																		$('.spinner').hide();
																		$('input').prop('readonly', false);
																		$('[name="FK_IM_UPCCode"]').val('');
																		$('[name="Barcode_Qty"]').val(1);
																		$('[name="FK_IM_UPCCode"]').focus();
																		play();
																	}
																},
																error 		: function(jqXHR){
																	swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
																}
															});

															return false;
														}
													}

												}
										});

									} // end of 'if (total pick - total pack) <= 0 (Scanning) || total pack - qty < 0' ajax request 
									else{
										$(result.required_qty).each(function(index, el) {
											$('#picklist-detail-list tbody tr').find('[data-loc-barcode = "' + result.required_qty[index].PKFK_Location_Source + ' - ' + barcode +'"]').text(number_format(result.required_qty[index].RequiredQty, 0, ".", ""));
										});
										$('.spinner').hide();
										$('input').prop('readonly', false)
										$('[name="FK_IM_UPCCode"]').val('');
										$('[name="Barcode_Qty"]').val(1);
										$('[name="FK_IM_UPCCode"]').focus();
										swal(result.title, result.message, result.type);
										play(); 
									}
								}
							})
						} // end condition of barcode checking in table
						else{
							$('.spinner').hide();
							$('input').prop('readonly', false)
							$('[name="FK_IM_UPCCode"]').val('');
							$('[name="Barcode_Qty"]').val(1);
							swal("Information", "Item does not exist", "info");
							play(); 
						}
					} // end condition of qty
				} // end condition of present barcode
			} // end condition of keypress
		});

		// End



		$('#back').click(function(event) {
			window.location.href = global.site_name + 'scan_pack'
		});

		// Functions for breakdown pop up

		$('.breakdown').click(function(event) {
			$('#breakdown-detail').modal('show');

			let loading = '<tr class="picknote-detail">' +
								'<td colspan="7" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
						  '</tr>';

			$('#picknote-detail-list tbody').html(loading);


			$.ajax({
				url			: global.site_name + 'scan_pack/getBreakdownDetails',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'picklist-no'	: picklist,
								'carton-no'		: carton_no
							  },
				success 	: function(result){

					let html_pn_dtl 	= '';
					let html_ctn_hdr 	= '';

					$(result.picknote_dtl).each(function(index) {


						html_pn_dtl += '<tr class="picknote-detail">'+
											'<td style="font-size: 12px; padding: 5px" data-loc-source="'+ result.picknote_dtl[index].PKFK_Location_Source +'">' + result.picknote_dtl[index].FK_IM_UPCCode + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].PKFK_InvMaster_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px">' + result.picknote_dtl[index].FK_Category_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].FK_Color_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].FK_Size_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format((parseFloat(result.picknote_dtl[index].PND_PickQty) - parseFloat(result.picknote_dtl[index].PND_PackedQty)), 2) +'</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result.picknote_dtl[index].PND_PackedQty, 2) + '</td>' +
									  '</tr>'
					});

					$('#picknote-detail-list tbody').html(html_pn_dtl);
					$('#carton-header-list tbody tr').empty();

				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			});
		});

		$('.refresh-breakdown').click(function(){
			let loading = '<tr class="picknote-detail">' +
								'<td colspan="7" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
						  '</tr>'

			$('#picknote-detail-list tbody').html(loading);


			$.ajax({
				url			: global.site_name + 'scan_pack/getBreakdownDetails',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'picklist-no'	: picklist,
								'carton-no'		: carton_no
							  },
				success 	: function(result){

					let html_pn_dtl 	= '';
					let html_ctn_hdr 	= '';

					$(result.picknote_dtl).each(function(index) {


						html_pn_dtl += '<tr class="picknote-detail">'+
											'<td style="font-size: 12px; padding: 5px" data-loc-source="'+ result.picknote_dtl[index].PKFK_Location_Source +'">' + result.picknote_dtl[index].FK_IM_UPCCode + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].PKFK_InvMaster_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px">' + result.picknote_dtl[index].FK_Category_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].FK_Color_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].FK_Size_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format((parseFloat(result.picknote_dtl[index].PND_PickQty) - parseFloat(result.picknote_dtl[index].PND_PackedQty)), 2) +'</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result.picknote_dtl[index].PND_PackedQty, 2) + '</td>' +
									  '</tr>'
					});

					$('#picknote-detail-list tbody').html(html_pn_dtl);
					$('#carton-header-list tbody tr').empty();

				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			});
		});

		$('.close-breakdown').click(function(event) {
			$('#breakdown-detail').modal('hide');
		});

		$('#picknote-detail-list tbody').on('click', '.picknote-detail', function(event) {
			var current_row 	= $(this).closest('.picknote-detail');
			let html_ctn_hdr 	= '';
			let barcode 		= current_row.find('td:eq(0)').text();
			let loc_source 		= current_row.find('[data-loc-source]').data('loc-source');
			let loading_carton 	= '<tr class="carton-header">' +
									 '<td colspan="3" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
								  '</tr>';

			$('#carton-header-list tbody').html(loading_carton);

			$.ajax({
				url			: global.site_name + 'scan_pack/getCartonHeader',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'picklist-no'	: picklist,
								'upc-code'		: barcode,
								'loc-source'	: loc_source
							  },
				success 	: function(result){

					$(result).each(function(index) {
						html_ctn_hdr += '<tr class="carton-header">'+
											'<td style="font-size: 12px; padding: 5px">' + result[index].CPH_CartonNo + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result[index].CPH_PackedQty, 2) + '</td>' +
											'<td style="font-size: 12px; padding: 5px">' + result[index].FK_Status_Code + '</td>' +
									  '</tr>';
					});

					$('#carton-header-list tbody').html(html_ctn_hdr);

				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			});

		});

		// End

		// Function for Show Carton popup

		$('#show-carton').click(function(){
			$('#show-carton-detail').modal('show');
			let loading_carton 	= '<tr class="show-carton-header">' +
									 '<td colspan="3" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
								  '</tr>';
			let html_ctn_hdr 	= '';

			$('#show-carton-header-list tbody').html(loading_carton);

			$.ajax({
				url			: global.site_name + 'scan_pack/getShowCartonDetails',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'picklist-no'	: picklist
							  },
				success 	: function(result){
					$('.sc-pick-qty').text(number_format(result.pn_hdr.PNH_TotalPickQty));
					$('.sc-pck-qty').text(number_format(result.pn_hdr.PNH_TotalPackedQty));

					$(result.ctn_hdr).each(function(index) {
						html_ctn_hdr += '<tr class="show-carton-header">'+
											'<td style="font-size: 12px; padding: 5px">' + result.ctn_hdr[index].CPH_CartonNo + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result.ctn_hdr[index].CPH_PackedQty, 2) + '</td>' +
											'<td style="font-size: 12px; padding: 5px">' + result.ctn_hdr[index].FK_Status_Code + '</td>' +
									  '</tr>';
					});

					$('#show-carton-header-list tbody').html(html_ctn_hdr);
					$('#show-carton-detail-list tbody tr').empty();

				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			});


		});

		$('#show-carton-header-list tbody').on('click', '.show-carton-header', function(){
			var current_row 	= $(this).closest('.show-carton-header');
			$('#show-carton-header-list tbody tr').removeClass('selected');
			current_row.addClass('selected');
			var carton_qty		= parseFloat(current_row.find('td:eq(1)').text());
			let cur_ctn_no		= current_row.find('td:eq(0)').text();
			let html_ctn_dtl 	= '';
			let loading_carton 	= '<tr class="show-carton-detail">' +
									 '<td colspan="7" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
								  '</tr>';

			$('#show-carton-detail-list tbody').html(loading_carton);
			$('.sc-carton-qty').text(number_format(carton_qty));

			$.ajax({
				url			: global.site_name + 'scan_pack/getCartonDetails',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'picklist-no'	: picklist,
								'carton-no'		: cur_ctn_no
							  },
				success 	: function(result){

					$(result).each(function(index) {
						html_ctn_dtl += '<tr class="show-carton-detail">'+
											'<td style="font-size: 12px; padding: 5px">' + result[index].FK_IM_UPCCode + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result[index].PKFK_InvMaster_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px">' + result[index].FK_Category_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result[index].FK_Color_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result[index].FK_Size_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(parseFloat(result[index].CPD_PackedQty), 2) + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(parseFloat(result[index].CPD_RequiredQty), 2) +'</td>' +
									  '</tr>'
					});

					$('#show-carton-detail-list tbody').html(html_ctn_dtl);

				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			});
		});


		$('.close-show-carton').click(function(event) {
			$('#show-carton-detail').modal('hide');
		});

		$('.close-carton-modal').click(function(){
			console.log('close');

			let crt_ctn_no = $('#show-carton-header-list tbody tr.selected').find('td:eq(0)').text();
			let crt_status = $('#show-carton-header-list tbody tr.selected').find('td:eq(2)').text();
			console.log(crt_ctn_no);

			if(crt_ctn_no == ""){
				swal("Information", "Select Carton No. to proceed", "info");
			}
			else{
				if(crt_status == "CCP"){
					swal("Information", "This Carton is already closed", "info");
				}
				else{
					swal({
						title				: "Proceed to Close Carton?",
						type 				: "warning",
						showCancelButton	: true,
						confirmButtonText	: "Yes",
						confirmButtonColor	: "#3989C8",
						closeOnConfirm		: false
					}, function(isConfirm){
						if(isConfirm){

swal({
																	title				: "Please wait!",
																	text 				: "Closing carton...",
																	type 				: "info",
																	showConfirmButton	: false
																});

							$.ajax({
								url			: global.site_name + 'scan_pack/closeCarton',
								dataType	: 'json',
								type 		: 'POST',
								data 		: {
												'picklist-no'		: picklist,
												'carton-no'			: crt_ctn_no,
											  },
								success		: function(result){
									if(result.success){
										swal("Success", "Closed Carton successfully", "success");
										is_closed_carton = true;
										$('#show-carton-header-list tbody tr.selected').find('td:eq(2)').text('CCP')
										location.reload(true);
									}
									else{
										swal(result.title, result.message, result.type);
									}
								},
								error 		: function(jqXHR){
									swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
								}
							});
						}
					});
				}
			}
		});

		// End

		$('.close-pl').click(function(event) {
			swal({
				title				: "Proceed to Close PL?",
				type 				: "warning",
				showCancelButton	: true,
				confirmButtonText	: "Yes",
				confirmButtonColor	: "#3989C8",
				closeOnConfirm		: false
			}, function(isConfirm){
				if(isConfirm){
					
					swal({
						title				: "Please wait!",
						text 				: "Closing Picklist...",
						type 				: "info",
						showConfirmButton	: false
					});

					$.ajax({
						url			: global.site_name + 'scan_pack/closePL',
						dataType	: 'json',
						type 		: 'POST',
						data 		: {
										'picklist-no'	: picklist,
										'carton-no'		: carton_no,
										'status'		: status
									  },
						success 	: function(output){
							if(output.success){
								swal({
									type 				: 'success',
									title 				: 'Closed PL Successfully',
									showConfirmButton 	: false
								});
								setTimeout(function(){
									window.location.href = global.site_name + 'scan_pack';
								}, 1000)
							}
							else{
								swal(output.title, output.message, output.type);
							}
						},
						error 		: function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
				}
			});
		});

		$('.close-carton').click(function(event) {
			let total_pck = 0;

			$('#picklist-detail-list tbody tr').each(function(){
				let pck = parseFloat($(this).find('[name="pck-qty"]').text());

				total_pck += pck; 
			});

			swal({
				title				: "Proceed to Close Carton?",
				type 				: "warning",
				showCancelButton	: true,
				confirmButtonText	: "Yes",
				confirmButtonColor	: "#3989C8",
				closeOnConfirm		: false
			}, function(isConfirm){
				if(isConfirm){

					swal({
						title				: "Please wait!",
						text 				: "Closing carton...",
						type 				: "info",
						showConfirmButton	: false
					});

					$.ajax({
						url			: global.site_name + 'scan_pack/closeCarton',
						dataType	: 'json',
						type 		: 'POST',
						data 		: {
										'picklist-no'		: picklist,
										'carton-no'			: carton_no,
									  },
						success		: function(result){
							if(result.success){
								swal("Success", "Closed Carton successfully", "success");
								is_closed_carton = true;
								$('[name="FK_IM_UPCCode"]').attr('readonly', true);
								$('[name="Barcode_Qty"]').attr('readonly', true);
								$('#picklist-detail-list tbody tr').empty();
								$('#back').attr('disabled', false);
							}
							else{
								swal(result.title, result.message, result.type);
							}
						},
						error 		: function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
				}
			});

		});

		// Filter Carton in show carton popup

		$('[name="From"]').on('keyup change', function(){

			let from = $('[name="From"]').val();
			let to 	 = $('[name="To"]').val();



			if((from == "" && to == "") || (from != "" && to != "")){
				let loading_carton 	= '<tr class="show-carton-header">' +
										 '<td colspan="3" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
									  '</tr>';
				let html_ctn_hdr 	= '';

				$('#show-carton-header-list tbody').html(loading_carton);
				
				$.ajax({
					url			: global.site_name + 'scan_pack/filterCarton',
					dataType	: 'json',
					type 		: 'POST',
					data 		: {
									'picklist-no'		: picklist,
									'from'				: $('[name="From"]').val(),
									'to'				: $('[name="To"]').val()
								  },
					success		: function(result){
						
						$(result).each(function(index) {
							html_ctn_hdr += '<tr class="show-carton-header">'+
												'<td style="font-size: 12px; padding: 5px">' + result[index].CPH_CartonNo + '</td>' +
												'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result[index].CPH_PackedQty, 2) + '</td>' +
												'<td style="font-size: 12px; padding: 5px">' + result[index].FK_Status_Code + '</td>' +
										  '</tr>';
						});
						
						$('#show-carton-header-list tbody').html(html_ctn_hdr);
						$('#show-carton-detail-list tbody tr').empty();
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				});
			}
		});

		$('[name="To"]').on('keyup change', function(){

			let from = $('[name="From"]').val();
			let to 	 = $('[name="To"]').val();



			if((from == "" && to == "") || (from != "" && to != "")){
				let loading_carton 	= '<tr class="show-carton-header">' +
										 '<td colspan="3" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
									  '</tr>';
				let html_ctn_hdr 	= '';

				$('#show-carton-header-list tbody').html(loading_carton);
				
				$.ajax({
					url			: global.site_name + 'scan_pack/filterCarton',
					dataType	: 'json',
					type 		: 'POST',
					data 		: {
									'picklist-no'		: picklist,
									'from'				: $('[name="From"]').val(),
									'to'				: $('[name="To"]').val()
								  },
					success		: function(result){
						
						$(result).each(function(index) {
							html_ctn_hdr += '<tr class="show-carton-header">'+
												'<td style="font-size: 12px; padding: 5px">' + result[index].CPH_CartonNo + '</td>' +
												'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result[index].CPH_PackedQty, 2) + '</td>' +
												'<td style="font-size: 12px; padding: 5px">' + result[index].FK_Status_Code + '</td>' +
										  '</tr>';
						});
						
						$('#show-carton-header-list tbody').html(html_ctn_hdr);
						$('#show-carton-detail-list tbody tr').empty();
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				});
			}
		});
	}

	init();

});

function maxLengthCheck(object){
    if (object.value.length > object.maxLength){
      object.value = object.value.slice(0, object.maxLength)
    }
}

function play(){
       var audio = document.getElementById("audio");
       audio.play();
}

function load_quagga(){
	if($('#barcode-scanner').length > 0 && navigator.mediaDevices && typeof navigator.mediaDevices.getUserMedia === 'function'){
		Quagga.init({
			inputStream : {
				name : "Live",
				type : "LiveStream",
				numOfWorkers : navigator.hardwareConcurrency,
				target : document.querySelector('#barcode-scanner')
			},
			decoder : {
				readers : ['ean_reader']
			},
			locate : true
		}, function(err) {
			if (err){ console.log(err); return }
		})

		Quagga.onDetected(function(result){
			var last_code = result.codeResult.code;
			var e = $.Event( "keypress", { keyCode: 13 } );
			Quagga.pause();
			$('#scan-barcode').modal('hide');
			$('[name="FK_IM_UPCCode"]').val(last_code);
			$('[name="FK_IM_UPCCode"]').focus();

			setTimeout(function(){
				$('[name="FK_IM_UPCCode"]').trigger(e);
			},500)
		})
	}
}