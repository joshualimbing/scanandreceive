$(document).ready(function(){

	function init(){
		$('.picklist').select2({
			placeholder: 'Select Picklist No.',
			allowClear: true
		});

		$('.slider').click(function() {
			let background = $('.slider').css('background-color');

			if(background == 'rgb(204, 204, 204)'){
				$('[name="Scan_Picklist"]').attr('readonly', true);
				$('.picklist').prop('disabled', false);
			}
			else{
				$('.picklist').val('').trigger('change');
				$('.picklist').prop('disabled', true);
				$('[name="Scan_Picklist"]').attr('readonly', false);
			}

			
		});

		$('#next').click(function(){
			let picklist_no = $('.picklist').val();
			let carton_no = $('[name="Carton_No"]').val();

			$.ajax({
				url			: global.site_name + 'scan_pack/checkPicklistHasMultipleLoc',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'picklist-no'	: picklist_no,
								'carton-no'		: carton_no
							  },
				success		: function(result){

					if(result.success){
						swal({
							title				: "Please wait!",
							type 				: "info",
							showConfirmButton	: false
						});

						setTimeout(function(){
							window.location.href = global.site_name + 'scan_pack/item_entry?picklist_no=' + picklist_no + '&carton_no=' + carton_no;
						}, 1000)
					}
					else{
						swal(result.title, result.message, result.type);
					}

				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			});
		});

		$('.picklist').change(function(){

			if($('.picklist').val() == ""){
				$('#next').prop('disabled', true);
			}
			else{
				if($('[name="Carton_No"]').val() != "" || $('[name="Carton_No"]').val() != 0){
					$('#next').prop('disabled', false);
				}
			}

		});

		$('[name="Carton_No"]').on('keyup change', function(){
			if($('[name="Carton_No"]').val() == "" || $('[name="Carton_No"]').val() == 0){
				$('#next').prop('disabled', true);
			}
			else{
				if($('.picklist').val() != ""){
					$('#next').prop('disabled', false);
				}
			}
		});

		$('[name="Scan_Picklist"]').keypress(function(e) {

			let barcode = $('[name="Scan_Picklist"]').val().trim();
			let carton_no = $('[name="Carton_No"]').val();

			if(e.keyCode == 13){
				if(barcode == ""){
					swal("Information", "Please input barcode to proceed", "info");
					$('[name="Scan_Picklist"]').val('');
				}
				else{

					if(carton_no == "" || carton_no == 0){
						swal("Information", "Please fill up Carton No. field to proceed", "info");
						$('[name="Scan_Picklist"]').val('');
					}
					else{

						swal({
							title				: "Please wait!",
							type 				: "info",
							showConfirmButton	: false
						});

						//Check if barcode exists in the database and check if that barcode has multiple locations - Created by Joshua Limbing (9/24/2019)

						$.ajax({
							url			: global.site_name + 'scan_pack/checkBarcodeifExisting',
							dataType	: 'json',
							type 		: 'POST',
							data 		: {
											'barcode'	: barcode,
											'carton-no'	: carton_no
										  },
							success		: function(result){

								if(result.success){
									window.location.href = global.site_name + 'scan_pack/item_entry?barcode=' + barcode + '&carton_no=' + carton_no;
								}
								else{
									swal(result.title, result.message, result.type);
									$('[name="Scan_Picklist"]').val('');
								}

							},
							error 		: function(jqXHR){
								swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
							}
						});

					}
				}
			}
		});

	}

	init();
});

function maxLengthCheck(object){
    if (object.value.length > object.maxLength){
      object.value = object.value.slice(0, object.maxLength)
    }
}	
