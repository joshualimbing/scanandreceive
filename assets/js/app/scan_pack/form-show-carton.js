$(document).ready(function(){

	$('[name="PNH_Picknum"]').select2();

	function init(){

		$('.breakdown-btn').click(function(){
			if($('[name="PNH_Picknum"]').val() == ""){

			}
		});

		$('[name="PNH_Picknum"]').select2({

			placeholder 		: 'Type a picklist no.',
			allowClear 			: true,
			minimumInputLength	: 3,
			ajax : 
				{

					url: global.site_name + "scan_pack/getPLSearchResults",
	                dataType: 'json',
	                delay: 250,
	                data: function (params) {
	                  var queryParameters = {
	                            'filter-type'   : 'PNH_Picknum'
	                        ,   'filter-search' : params.term
	                        ,   'action'        : 'search'
	                    }
	                    return queryParameters;
	                },
	                processResults: function (data, params) {
	                    return {
	                        results: $.map(data, function(pl) {
	                            return {
	                                id: pl.PNH_Picknum,
	                                text:pl['PNH_Picknum']
	                            }
	                        })
	                    };
	                }

				}

		});

		// Functions for breakdown pop up

		$('.breakdown-btn').click(function(event) {

			if($('[name="PNH_Picknum"]').val() == ""){
				swal("Information", "Please input Picklist No. to see the breakdown", "info")
			}
			else{
				$('#breakdown-detail').modal('show');

				let loading = '<tr class="picknote-detail">' +
									'<td colspan="7" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
							  '</tr>';

				$('#picknote-detail-list tbody').html(loading);


				$.ajax({
					url			: global.site_name + 'scan_pack/getBreakdownDetails',
					dataType	: 'json',
					type 		: 'POST',
					data 		: {
									'picklist-no'	: $('[name="PNH_Picknum"]').val(),
									'carton-no' 	: $('#show-carton-header-list tbody tr.selected').find('td:eq(0)').text()
								  },
					success 	: function(result){

						let html_pn_dtl 	= '';
						let html_ctn_hdr 	= '';

						$(result.picknote_dtl).each(function(index) {


							html_pn_dtl += '<tr class="picknote-detail">'+
												'<td style="font-size: 12px; padding: 5px" data-loc-source="'+ result.picknote_dtl[index].PKFK_Location_Source +'">' + result.picknote_dtl[index].FK_IM_UPCCode + '</td>' +
												'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].PKFK_InvMaster_Code + '</td>' +
												'<td style="font-size: 12px; padding: 5px">' + result.picknote_dtl[index].FK_Category_Code + '</td>' +
												'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].FK_Color_Code + '</td>' +
												'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].FK_Size_Code + '</td>' +
												'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format((parseFloat(result.picknote_dtl[index].PND_PickQty) - parseFloat(result.picknote_dtl[index].PND_PackedQty)), 2) +'</td>' +
												'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result.picknote_dtl[index].PND_PackedQty, 2) + '</td>' +
										  '</tr>'
						});

						$('#picknote-detail-list tbody').html(html_pn_dtl);
						$('#carton-header-list tbody tr').empty();

					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				});
			}

		});

		$('.refresh-breakdown').click(function(){
			let loading = '<tr class="picknote-detail">' +
								'<td colspan="7" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
						  '</tr>'

			$('#picknote-detail-list tbody').html(loading);


			$.ajax({
				url			: global.site_name + 'scan_pack/getBreakdownDetails',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'picklist-no'	: $('[name="PNH_Picknum"]').val()
							  },
				success 	: function(result){

					let html_pn_dtl 	= '';
					let html_ctn_hdr 	= '';

					$(result.picknote_dtl).each(function(index) {


						html_pn_dtl += '<tr class="picknote-detail">'+
											'<td style="font-size: 12px; padding: 5px" data-loc-source="'+ result.picknote_dtl[index].PKFK_Location_Source +'">' + result.picknote_dtl[index].FK_IM_UPCCode + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].PKFK_InvMaster_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px">' + result.picknote_dtl[index].FK_Category_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].FK_Color_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result.picknote_dtl[index].FK_Size_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format((parseFloat(result.picknote_dtl[index].PND_PickQty) - parseFloat(result.picknote_dtl[index].PND_PackedQty)), 2) +'</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result.picknote_dtl[index].PND_PackedQty, 2) + '</td>' +
									  '</tr>'
					});

					$('#picknote-detail-list tbody').html(html_pn_dtl);
					$('#carton-header-list tbody tr').empty();

				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			});
		});

		$('.close-breakdown').click(function(event) {
			$('#breakdown-detail').modal('hide');
		});

		$('#picknote-detail-list tbody').on('click', '.picknote-detail', function(event) {
			var current_row 	= $(this).closest('.picknote-detail');
			let html_ctn_hdr 	= '';
			let barcode 		= current_row.find('td:eq(0)').text();
			let loc_source 		= current_row.find('[data-loc-source]').data('loc-source');
			let loading_carton 	= '<tr class="carton-header">' +
									 '<td colspan="3" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
								  '</tr>';

			$('#carton-header-list tbody').html(loading_carton);

			$.ajax({
				url			: global.site_name + 'scan_pack/getCartonHeader',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'picklist-no'	: $('[name="PNH_Picknum"]').val(),
								'upc-code'		: barcode,
								'loc-source'	: loc_source
							  },
				success 	: function(result){

					$(result).each(function(index) {
						html_ctn_hdr += '<tr class="carton-header">'+
											'<td style="font-size: 12px; padding: 5px">' + result[index].CPH_CartonNo + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result[index].CPH_PackedQty, 2) + '</td>' +
											'<td style="font-size: 12px; padding: 5px">' + result[index].FK_Status_Code + '</td>' +
									  '</tr>';
					});

					$('#carton-header-list tbody').html(html_ctn_hdr);

				}
			});

		});

		// End


		// Function for Show Carton

		$('[name="PNH_Picknum"]').change(function(){

			if($('[name="PNH_Picknum"]').val() != ""){

				$('[name="From"]').attr('readonly', false);
				$('[name="To"]').attr('readonly', false);

				let loading_carton 	= '<tr class="show-carton-header">' +
										 '<td colspan="3" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
									  '</tr>';
				let html_ctn_hdr 	= '';

				$('#show-carton-header-list tbody').html(loading_carton);

				$.ajax({
					url			: global.site_name + 'scan_pack/getShowCartonDetails',
					dataType	: 'json',
					type 		: 'POST',
					data 		: {
									'picklist-no'	: $('[name="PNH_Picknum"]').val()
								  },
					success 	: function(result){
						$('.pick-qty').text(number_format(result.pn_hdr.PNH_TotalPickQty));
						$('.pck-qty').text(number_format(result.pn_hdr.PNH_TotalPackedQty));

						$(result.ctn_hdr).each(function(index) {
							html_ctn_hdr += '<tr class="show-carton-header">'+
												'<td style="font-size: 12px; padding: 5px">' + result.ctn_hdr[index].CPH_CartonNo + '</td>' +
												'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result.ctn_hdr[index].CPH_PackedQty, 2) + '</td>' +
												'<td style="font-size: 12px; padding: 5px">' + result.ctn_hdr[index].FK_Status_Code + '</td>' +
										  '</tr>';
						});

						$('#show-carton-header-list tbody').html(html_ctn_hdr);
						$('#show-carton-detail-list tbody tr').empty();

					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				});
			}

			else{
				$('#show-carton-header-list tbody tr').empty();
				$('#show-carton-detail-list tbody tr').empty();
				
				$('[name="From"]').attr('readonly', true);
				$('[name="To"]').attr('readonly', true);

				$('[name="From"]').val('');
				$('[name="To"]').val('');

			}

		});

		$('#show-carton-header-list tbody').on('click', '.show-carton-header', function(){
			var current_row 	= $(this).closest('.show-carton-header');
			$('#show-carton-header-list tbody tr').removeClass('selected');
			current_row.addClass('selected');
			var carton_qty		= parseFloat(current_row.find('td:eq(1)').text());
			let cur_ctn_no		= current_row.find('td:eq(0)').text();
			let html_ctn_dtl 	= '';
			let loading_carton 	= '<tr class="show-carton-detail">' +
									 '<td colspan="7" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
								  '</tr>';

			$('#show-carton-detail-list tbody').html(loading_carton);
			$('.carton-qty').text(number_format(carton_qty));

			$.ajax({
				url			: global.site_name + 'scan_pack/getCartonDetails',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'picklist-no'	: $('[name="PNH_Picknum"]').val(),
								'carton-no'		: cur_ctn_no
							  },
				success 	: function(result){

					$(result).each(function(index) {
						html_ctn_dtl += '<tr class="show-carton-detail">'+
											'<td style="font-size: 12px; padding: 5px">' + result[index].FK_IM_UPCCode + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result[index].PKFK_InvMaster_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px">' + result[index].FK_Category_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result[index].FK_Color_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + result[index].FK_Size_Code + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(parseFloat(result[index].CPD_PackedQty), 2) + '</td>' +
											'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(parseFloat(result[index].CPD_RequiredQty), 2) +'</td>' +
									  '</tr>'
					});

					$('#show-carton-detail-list tbody').html(html_ctn_dtl);

				},
				error 		: function(jqXHR){
					swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
				}
			});
		});

		// End

		// Close Carton 

		$('.carton-btn').click(function(){
			console.log('close');

			let crt_ctn_no = $('#show-carton-header-list tbody tr.selected').find('td:eq(0)').text();
			let crt_status = $('#show-carton-header-list tbody tr.selected').find('td:eq(2)').text();
			console.log(crt_ctn_no);

			if(crt_ctn_no == ""){
				swal("Information", "Select Carton No. to proceed", "info");
			}
			else{
				if(crt_status == "CCP"){
					swal("Information", "This Carton is already closed", "info");
				}
				else{
					swal({
						title				: "Proceed to Close Carton?",
						type 				: "warning",
						showCancelButton	: true,
						confirmButtonText	: "Yes",
						confirmButtonColor	: "#3989C8",
						closeOnConfirm		: false
					}, function(isConfirm){
						if(isConfirm){

							swal({
								title				: "Please wait!",
								text 				: "Closing carton...",
								type 				: "info",
								showConfirmButton	: false
							});

							$.ajax({
								url			: global.site_name + 'scan_pack/closeCarton',
								dataType	: 'json',
								type 		: 'POST',
								data 		: {
												'picklist-no'		: $('[name="PNH_Picknum"]').val(),
												'carton-no'			: crt_ctn_no,
											  },
								success		: function(result){
									if(result.success){
										swal("Success", "Closed Carton successfully", "success");
										is_closed_carton = true;
										$('#show-carton-header-list tbody tr.selected').find('td:eq(2)').text('CCP')
									}
									else{
										swal(result.title, result.message, result.type);
									}
								},
								error 		: function(jqXHR){
									swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
								}
							});
						}
					});
				}
			}
		});

		// End

		$('.close-pl').click(function(event) {
			if($('[name="PNH_Picknum"]').val() == ""){
				swal('Information', 'Select a picklist first to proceed', 'info');
			}
			else{
				swal({
					title				: "Proceed to Close PL?",
					type 				: "warning",
					showCancelButton	: true,
					confirmButtonText	: "Yes",
					confirmButtonColor	: "#3989C8",
					closeOnConfirm		: false
				}, function(isConfirm){
					if(isConfirm){

						swal({
							title				: "Please wait!",
							text 				: "Closing Picklist...",
							type 				: "info",
							showConfirmButton	: false
						});

						$.ajax({
							url			: global.site_name + 'scan_pack/closePL',
							dataType	: 'json',
							type 		: 'POST',
							data 		: {
											'picklist-no'	: $('[name="PNH_Picknum"]').val(),
											'carton-no'		: '',
											'status'		: ''
										  },
							success 	: function(output){
								if(output.success){
									swal({
										type 				: 'success',
										title 				: 'Closed PL Successfully',
										showConfirmButton 	: false
									});
									setTimeout(function(){
										window.location.href = global.site_name + 'scan_pack';
									}, 1000)
								}
								else{
									swal(output.title, output.message, output.type);
								}
							},
							error 		: function(jqXHR){
								swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
							}
						});
					}
				});
			}
		});

		// Filter

		$('[name="From"]').on('keyup change', function(){

			let from = $('[name="From"]').val();
			let to 	 = $('[name="To"]').val();



			if((from == "" && to == "") || (from != "" && to != "")){
				let loading_carton 	= '<tr class="show-carton-header">' +
										 '<td colspan="3" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
									  '</tr>';
				let html_ctn_hdr 	= '';

				$('#show-carton-header-list tbody').html(loading_carton);
				
				$.ajax({
					url			: global.site_name + 'scan_pack/filterCarton',
					dataType	: 'json',
					type 		: 'POST',
					data 		: {
									'picklist-no'		: $('[name="PNH_Picknum"]').val(),
									'from'				: $('[name="From"]').val(),
									'to'				: $('[name="To"]').val()
								  },
					success		: function(result){
						
						$(result).each(function(index) {
							html_ctn_hdr += '<tr class="-show-carton-header">'+
												'<td style="font-size: 12px; padding: 5px">' + result[index].CPH_CartonNo + '</td>' +
												'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result[index].CPH_PackedQty, 2) + '</td>' +
												'<td style="font-size: 12px; padding: 5px">' + result[index].FK_Status_Code + '</td>' +
										  '</tr>';
						});
						
						$('#show-carton-header-list tbody').html(html_ctn_hdr);
						$('#show-carton-detail-list tbody tr').empty();
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				});
			}
		});

		$('[name="To"]').on('keyup change', function(){

			let from = $('[name="From"]').val();
			let to 	 = $('[name="To"]').val();



			if((from == "" && to == "") || (from != "" && to != "")){
				let loading_carton 	= '<tr class="show-carton-header">' +
										 '<td colspan="3" class="text-center" style="font-size: 48px">'+ '<span class="fa fa-spinner fa-spin"></span>' +'</td>'
									  '</tr>';
				let html_ctn_hdr 	= '';

				$('#show-carton-header-list tbody').html(loading_carton);
				
				$.ajax({
					url			: global.site_name + 'scan_pack/filterCarton',
					dataType	: 'json',
					type 		: 'POST',
					data 		: {
									'picklist-no'		: $('[name="PNH_Picknum"]').val(),
									'from'				: $('[name="From"]').val(),
									'to'				: $('[name="To"]').val()
								  },
					success		: function(result){
						
						$(result).each(function(index) {
							html_ctn_hdr += '<tr class="show-carton-header">'+
												'<td style="font-size: 12px; padding: 5px">' + result[index].CPH_CartonNo + '</td>' +
												'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result[index].CPH_PackedQty, 2) + '</td>' +
												'<td style="font-size: 12px; padding: 5px">' + result[index].FK_Status_Code + '</td>' +
										  '</tr>';
						});
						
						$('#show-carton-header-list tbody').html(html_ctn_hdr);
						$('#show-carton-detail-list tbody tr').empty();
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				});
			}
		});
	}

	init();

});

function maxLengthCheck(object){
    if (object.value.length > object.maxLength){
      object.value = object.value.slice(0, object.maxLength)
    }
}