$(document).ready(function(){

	function init(){

		$('.select-doc-type').select2({
			placeholder: 'Choose an option...',
			allowClear: true
		})

		$('.select-recv-from').select2({
			placeholder: 'Choose an option...',
			allowClear: true
		})

		$('.select-ref-no').select2();
		$('.select-ref-no').select2({
			placeholder: "Enter reference no.",
			allowClear: true,
			minimumInputLength: 2,
			ajax:
				{
					url: global.site_name + 'scan_verify/getReferenceNo',
					dataType: 'json',
	                delay: 250,
	                data: function (params) {
	                  var queryParameters = {
	                           	'filter-search' 	: params.term
	                        ,   'filter-doctype' 	: $('.select-doc-type').val()
	                        ,   'filter-recvfrom' 	: $('.select-recv-from').val()
	                        ,   'action'        	: 'search'
	                    }
	                    return queryParameters;
	                },
	                processResults: function (data, params) {
	                    return {
	                        results: $.map(data, function(ref) {
	                            return {
	                                id: ref.Ref_No,
	                                text:ref['Ref_No']
	                            }
	                        })
	                    };
	                }
				}
		});

		$('.select-recv-from').change(function() {
			let doc_type 	= $('.select-doc-type').val();
			let recv_from	= $('.select-recv-from').val();

			if(doc_type != "" && recv_from != ""){
				$('.select-ref-no').prop('disabled', false);
			}
			else{
				$('.select-ref-no').val('');
				$('.select-ref-no').prop('disabled', true);
			}

		});

		$('.select-doc-type').change(function() {
			let doc_type 	= $('.select-doc-type').val();
			let recv_from	= $('.select-recv-from').val();
			let html 		= '';

			if(doc_type != ""){
				$.ajax({
					url 		: global.site_name + 'scan_verify/getRecvFrom',
					dataType 	: 'json',
					type 		: 'POST',
					data 		: {
									'doc-type' 		: doc_type,
								  },
					success 	: function(result){
						$('.select-recv-from').prop('disabled', false);
						$('.select-recv-from').empty();
						html = '<option value=""></option>'
						$(result).each(function(index){
							html += '<option value="' + result[index].Loc_Code + '">' + result[index].Description + '</option>';
						});
						$('.select-recv-from').append(html);
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				})
			}
			else{
				$('.select-recv-from').val('').trigger('change');
				$('.select-recv-from').empty();
				$('.select-ref-no').val('');
				$('.select-recv-from').prop('disabled', true);
				$('.select-ref-no').prop('disabled', true);
			}

		});

		$('.fake-btn').click(function(){
			$('[name="file"]').trigger('click');
		});

		$('[name="file"]').change(function(event) {
			let file_detail = $('[name="file"]').prop('files')[0];

			let filename = file_detail["name"]
			let filename_arr = extractFile(filename);
			let file_ext = filename_arr.slice(-1)[0];

			console.log(file_ext);

			if(file_ext != 'zip' && file_ext != 'rar'){
				swal('Error', 'Imported doc must be in .zip or .rar extension file', 'error');
				$('[name="file"]').val(undefined);
			}
			else{
				$('#submit-form').trigger('click');
			}
		});

		$('.back').click(function(){
			window.location.href = global.site_name + 'user/menu';
		});

		$('.next').click(function(){
			let doc_type 	= $('.select-doc-type').val();
			let recv_from	= $('.select-recv-from').val();
			let ref_no 		= $('.select-ref-no').val();
			let remarks 	= $('#Doc_Remarks').val();

			let rem 		= remarks.replace("&", "%26");

			if(doc_type == "" || recv_from == "" || ref_no == ""){
				swal("Information", "Complete all required fields to proceed", "info")
			}
			else{
				swal({
					title				: "Please wait!",
					type 				: "info",
					showConfirmButton	: false
				});

				$.ajax({
					url 		: global.site_name + 'scan_verify/next',
					dataType 	: 'json',
					type 		: 'POST',
					data 		: {
									'doc-type' 		: doc_type,
									'ref-no' 		: ref_no,
									'recv-from'		: recv_from,
									'remarks'		: rem
								  },
					success 	: function(result){

						if(result.success){
							setTimeout(function(){
								window.location.href = global.site_name + 'scan_verify/item_entry?doc_type=' + doc_type + '&recv_from=' + recv_from + '&ref_no=' + ref_no + '&remarks=' + rem;  
							}, 1000);
						}
						else{
							swal(result.title, result.message, result.type);
						}
					},
					error 		: function(jqXHR){
						swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
					}
				});

			}
		});

	}

	init();

});

function extractFile(file_name){

	return file_name.split('.');

}