$(document).ready(function(){

	function init(){

		// $(window).on("blur focus", function(e) {
		//     location.reload(true);
		// })

		// Scan through mobile camera or webcam - Created by Joshua Limbing (09/07/2020)
		
		load_quagga();
		$('#scanButton').click(function(event) {
			Quagga.start();
			$('#scan-barcode').modal('show')
		});

		$('#close-barcode').click(function(event) {
			Quagga.pause();
			$('#scan-barcode').modal('hide')
		});

		// End Scan

		$('.back-btn').click(function(event) {
			window.location.href = global.site_name + 'scan_verify';
		});

		$('.undo-btn').click(function(){
			$('.scn-status').text('Unscanning');
			$('.scn-status').css('color', '#FF0101');
		});

		$('.scan-btn').click(function(){
			$('.scn-status').text('Scanning');
			$('.scn-status').css('color', '#000000');
		});

		$('[name="Barcode_Qty"]').keypress(function(e){
			if(e.keyCode == 13){
				$('[name="FK_IM_UPCCode"]').focus();
			}
		});

		$('[name="Bin_Location"]').select2({
			placeholder: 'Choose an option...',
			allowClear: true
		})

		$('#verify-detail-list tbody tr').on('click', '.null-loc', function(){
			var current_row = $(this).closest('.verified-details');
			let item_code = current_row.find('td:eq(1)').text();
			let actual_loc = current_row.find('td:eq(2)').text();
			console.log(actual_loc);

			$('.item-code-input').val(item_code);

			let list = '';
			$('[name="Bin_Location"]').empty();
			list += '<option value=""></option>';

			$(bin_list).each(function(index) {
				list += '<option value="'+ bin_list[index].Loc_Code +'" '+ (bin_list[index].Loc_Code == actual_loc ? 'selected' : '') +'>'+ bin_list[index].Loc_Code +'</option>'
			});

			$('[name="Bin_Location"]').append(list).trigger('change');
			$('#bin-loc-detail').modal('show');
		});

		$('.close-list').click(function(event) {
			$('#bin-loc-detail').modal('hide');
		});


		// Choose Bin for Warehouse Location Type - Created by Joshua Limbing (10/17/2019)
		$('#bin-loc-detail').on('change', '[name="Bin_Location"]', function(){
			var current_row = $(this).closest('.verified-details');
			let item_code = $('.item-code-input').val();
			let bin_loc = $('[name="Bin_Location"]').val();

			$.ajax({
					url			: global.site_name + 'scan_verify/updateLocationSource',
					dataType 	: 'json',
					type 		: 'POST',
					data 		: {
									'doc-type'		: doc_type,
									'ref-no' 		: ref_doc,
									'recv-from' 	: recv_from,
									'item-code'		: item_code,
									'bin-loc'		: bin_loc
								  }
			});

			$('#verify-detail-list tbody tr td[data-item-code="'+item_code+'"]').text(bin_loc);

		});

		// Reset Button - Created by Joshua Limbing (10/3/2019)

		$('.reset-btn').click(function() {
			swal({
				title				: "Do you want to reset the table?",
				type 				: "warning",
				showCancelButton	: true,
				confirmButtonText	: "Reset",
				confirmButtonColor	: "#3989C8",
				closeOnConfirm		: false
			},function(isConfirm){

				if(isConfirm){
					swal({
						title				: "Please wait!",
						text 				: "Resetting table...",
						type 				: "info",
						showConfirmButton	: false
					});

					$.ajax({
						url			: global.site_name + 'scan_verify/reset',
						dataType 	: 'json',
						type 		: 'POST',
						data 		: {
										'doc-type'		: doc_type,
										'ref-no' 		: ref_doc,
										'recv-from' 	: recv_from
									  },
						success 	: function(result){

							if(result.success){
								swal("Success", "Reset table successfully", "success")

								let html_sv_row = '';

								$(result.detail).each(function(index) {
									let loc_src = result.detail[index].Location_Source;

									html_sv_row += '<tr class="verified-details">' +
															'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + result.detail[index].UPCCode + '</td>' +
															'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + result.detail[index].PKFK_InvMaster_Code + '</td>'

									if(loc_type == 'STR'){
										html_sv_row +=	'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + result.detail[index].Location_Source + '</td>';
									}
									else{
										if(result.detail[index].IM_ItemLocation !== null){
											html_sv_row +=	'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + result.detail[index].Location_Source + '</td>';
										}
										else{
											html_sv_row +=	'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;" class="null-loc" data-item-code="' + result.detail[index].PKFK_InvMaster_Code + '">' + result.detail[index].Location_Source + '</td>';
										}
									}

															
									html_sv_row +=			'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + number_format(result.detail[index].RequiredQty) + '</td>' +
															'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">0</td>'
														  '</tr>';

								});
								

								$('#verify-detail-list tbody').html(html_sv_row);
								$('.scn-qty').text(0)

								$('#verify-detail-list tbody tr').on('click', '.null-loc', function(){
									var current_row = $(this).closest('.verified-details');
									let item_code = current_row.find('td:eq(1)').text();
									let actual_loc = current_row.find('td:eq(2)').text();
									console.log(actual_loc);

									$('.item-code-input').val(item_code);

									let list = '';
									$('[name="Bin_Location"]').empty();
									list += '<option value=""></option>';

									$(bin_list).each(function(index) {
										list += '<option value="'+ bin_list[index].Loc_Code +'" '+ (bin_list[index].Loc_Code == actual_loc ? 'selected' : '') +'>'+ bin_list[index].Loc_Code +'</option>'
									});

									$('[name="Bin_Location"]').append(list).trigger('change');
									$('#bin-loc-detail').modal('show');
								});
							}
							else{
								swal(result.title, result.message, result.type);
							}

						},
						error 		: function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
				}


			});
		});

		// Barcode for Scan Verify - Created by Joshua Limbing (10/3/2019)

		$('[name="FK_IM_UPCCode"]').keypress(function(event) {

			if(event.keyCode == 13){
				let barcode 	= $('[name="FK_IM_UPCCode"]').val().trim();
				let qty 		= parseFloat($('[name="Barcode_Qty"]').val());
				let status 		= $('.scn-status').text();
				let total_scn 	= parseFloat($('.scn-qty').text());
				let html_recv	= '';
				let barcode_arr = [];

				$('.spinner').show();
				$('input').attr('readonly', true);
				
				if(barcode == ""){
					$('.spinner').hide();
					$('[name="FK_IM_UPCCode"]').prop('readonly', false);
					$('[name="Barcode_Qty"]').prop('readonly', false)
					$('.item-code-input').prop('readonly', true);
					swal("Information", "No barcode found", "info");
					play();
					$('[name="FK_IM_UPCCode"]').val('');
					$('[name="Barcode_Qty"]').val(1);
				}
				else{
					if(qty == "" || qty == 0){
						$('.spinner').hide();
						$('[name="FK_IM_UPCCode"]').prop('readonly', false);
						$('[name="Barcode_Qty"]').prop('readonly', false)
						$('.item-code-input').prop('readonly', true);
						swal("Error", "No Qty input found", "error");
						play();
						$('[name="FK_IM_UPCCode"]').val('');
						$('[name="Barcode_Qty"]').val(1);
					}
					else{
						
						let barcode_arr = [];

						$('#verify-detail-list tbody tr').each(function() {
							let upc_code = $(this).find('td:eq(0)').text();

							barcode_arr.push(upc_code);
						});

						// For existing item in the table

						if($.inArray(barcode, barcode_arr) !== -1){
							if(status == 'Scanning'){
								$('#verify-detail-list tbody tr').each(function() {
									let current_row	= $(this).closest('.verified-details');
									let upc_code 	= $(this).find('td:eq(0)').text();
									let item_code 	= $(this).find('td:eq(1)').text();
									let req_qty 	= parseFloat($(this).find('td:eq(3)').text());
									let scn_qty 	= parseFloat($(this).find('td:eq(4)').text());

									if(barcode == upc_code){

										$.ajax({
											url			: global.site_name + 'scan_verify/scanItem',
											dataType 	: 'json',
											type 		: 'POST',
											data 		: {
															'doc-type'		: doc_type,
															'ref-no' 		: ref_doc,
															'recv-from' 	: recv_from,
															'upc-code'		: upc_code,
															'item-code' 	: item_code,
															'req-qty' 		: req_qty,
															'scn-qty' 		: scn_qty + qty,
															'status' 		: status
														  },
											success 	: function(result_scn){
												current_row.find('td:eq(4)').text(number_format(result_scn, 0, ".", ""));

												let sv_scan_qty = 0;

												$('#verify-detail-list tbody tr').each(function() {
													let sv_scn = parseFloat($(this).find('td:eq(4)').text());

													sv_scan_qty += sv_scn;

												});

												$('.scn-qty').text(sv_scan_qty);

												$('.spinner').hide();
												$('[name="FK_IM_UPCCode"]').prop('readonly', false);
												$('[name="Barcode_Qty"]').prop('readonly', false);
												$('.item-code-input').prop('readonly', true);
												
												$('[name="FK_IM_UPCCode"]').val('');
												$('[name="Barcode_Qty"]').val(1);
											},
											error 		: function(jqXHR){
												swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
											}
										});

									}
								});
							}
							else if(status == 'Unscanning'){
								$('#verify-detail-list tbody tr').each(function() {
									let current_row = $(this).closest('.verified-details')
									let upc_code 	= $(this).find('td:eq(0)').text();
									let item_code 	= $(this).find('td:eq(1)').text();
									let req_qty 	= parseFloat($(this).find('td:eq(3)').text());
									let scn_qty 	= parseFloat($(this).find('td:eq(4)').text());

									if(barcode == upc_code){
										if( (scn_qty - qty) < 0 ){
											swal("Error", "Scanned Qty cannot be less than zero", "error");
											play();
											$('[name="FK_IM_UPCCode"]').val('');
											$('[name="Barcode_Qty"]').val(1);
										}
										else{

											$.ajax({
												url			: global.site_name + 'scan_verify/scanItem',
												dataType 	: 'json',
												type 		: 'POST',
												data 		: {
																'doc-type'		: doc_type,
																'ref-no' 		: ref_doc,
																'recv-from' 	: recv_from,
																'upc-code'		: upc_code,
																'item-code' 	: item_code,
																'req-qty' 		: req_qty,
																'scn-qty' 		: scn_qty - qty,
																'status' 		: status
															  },
												success 	: function(result_scn){
													current_row.find('td:eq(4)').text(number_format(result_scn, 0, ".", ""));

													let sv_scan_qty = 0;

													$('#verify-detail-list tbody tr').each(function() {
														let sv_scn = parseFloat($(this).find('td:eq(4)').text());

														sv_scan_qty += sv_scn;

													});

													$('.scn-qty').text(sv_scan_qty);

													if(req_qty == 0 && (result_scn == 0) ){
														$(this).remove();
													}

													$('.spinner').hide();
													$('[name="FK_IM_UPCCode"]').prop('readonly', false);
													$('[name="Barcode_Qty"]').prop('readonly', false)
													$('.item-code-input').prop('readonly', true);
													
													$('[name="FK_IM_UPCCode"]').val('');
													$('[name="Barcode_Qty"]').val(1);
												},
												error 		: function(jqXHR){
													swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
												}
											});

										}

									}
								});
							}
						}

						// For non-existing item in the table

						else{
							if(status == 'Scanning'){
								let html_sv_row = ''
								let total_scn 	= parseFloat(number_format($('.scn-qty').text(), 0, ".", ""));

								$.ajax({
									url			: global.site_name + 'scan_verify/createSVItem',
									dataType 	: 'json',
									type 		: 'POST',
									data 		: {
													'doc-type'		: doc_type,
													'ref-no' 		: ref_doc,
													'recv-from' 	: recv_from,
													'remarks' 		: remarks,
													'upc-code'		: barcode,
													'scn-qty' 		: qty
												  },
									success 	: function(output){

										if(output.success){
											let loc_src = output.item_loc;

											html_sv_row += '<tr class="verified-details">' +
															'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + barcode + '</td>' +
															'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + output.item_code + '</td>';

											if(loc_type == 'STR'){
												html_sv_row +=	'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + output.item_loc + '</td>';
											}
											else{
												if(output.item_loc !== null){
													html_sv_row +=	'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + output.item_loc + '</td>';
												}
												else{
													html_sv_row +=	'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;" class="null-loc" data-item-code="' + output.item_code + '">' + (output.item_loc === null ? '' : output.item_loc) + '</td>';
												}
											}

											html_sv_row +=  '<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">0</td>' +
															'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + number_format(output.scn_qty, 0, ".", "") + '</td>'
														  '</tr>'

											$('#verify-detail-list tbody').append(html_sv_row);
											$('.scn-qty').text(total_scn + parseFloat( number_format(output.scn_qty, 0, ".", "") ) );

											$('#verify-detail-list tbody tr').on('click', '.null-loc', function(){
												var current_row = $(this).closest('.verified-details');
												let item_code = current_row.find('td:eq(1)').text();
												let actual_loc = current_row.find('td:eq(2)').text();
												console.log(actual_loc);

												$('.item-code-input').val(item_code);

												let list = '';
												$('[name="Bin_Location"]').empty();
												list += '<option value=""></option>';

												$(bin_list).each(function(index) {
													list += '<option value="'+ bin_list[index].Loc_Code +'" '+ (bin_list[index].Loc_Code == actual_loc ? 'selected' : '') +'>'+ bin_list[index].Loc_Code +'</option>'
												});

												$('[name="Bin_Location"]').append(list).trigger('change');
												$('#bin-loc-detail').modal('show');
											});

										}
										else{
											swal(output.title, output.message, output.type);
											play();
										}

										$('.spinner').hide();
										$('[name="FK_IM_UPCCode"]').prop('readonly', false);
										$('[name="Barcode_Qty"]').prop('readonly', false)
										$('.item-code-input').prop('readonly', true);

										$('[name="FK_IM_UPCCode"]').val('');
										$('[name="Barcode_Qty"]').val(1);
									},
									error 		: function(jqXHR){
										swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
									}
								});

							}
							else if(status == 'Unscanning'){
								swal("Error", "No item found", "error");
								play();
								$('.spinner').hide();
								$('[name="FK_IM_UPCCode"]').prop('readonly', false);
								$('[name="Barcode_Qty"]').prop('readonly', false)
								$('.item-code-input').prop('readonly', true);
								$('[name="FK_IM_UPCCode"]').val('');
								$('[name="Barcode_Qty"]').val(1);
							}
						}
					}
				}

			}
		});

		// Post button - Created by Joshua Limbing (10/3/2019)

		$('.post-btn').click(function(){

			swal({
				title				: "Post Items to be received?",
				type 				: "warning",
				showCancelButton	: true,
				confirmButtonText	: "Yes",
				confirmButtonColor	: "#3989C8",
				closeOnConfirm		: false
			}, function(isConfirm){
				if(isConfirm){
					let hasMissingLoc = 0;

					$('#verify-detail-list tbody tr').each(function() {
						let item_loc 	= $(this).find('td:eq(2)').text();
						let scn_qty 	= parseFloat($(this).find('td:eq(4)').text());

						if(scn_qty > 0){
							if(item_loc == ""){
								hasMissingLoc += 1;
							}
						}
					});

					if(hasMissingLoc > 0){
						swal('Information', 'All items must have actual location before posting.', 'info');
					}
					else{
						swal({
							title				: "Please wait!",
							text 				: "Posting items to receive",
							type 				: "info",
							showConfirmButton	: false
						});

						$.ajax({
							url			: global.site_name + 'scan_verify/postReceived',
							dataType	: 'json',
							type 		: 'POST',
							data 		: {
											'doc-type'		: doc_type,
											'recv-from'		: recv_from,
											'remarks'		: remarks,
											'ref-no'		: ref_doc,
										  },
							success		: function(result){
								if(result.success){
									swal("Success", "Posted items successfully", "success");
									setTimeout(function(){
										window.location.href = global.site_name + 'scan_verify'
									}, 1000)
								}
								else{
									swal(result.title, result.message, result.type);
								}
							},
							error 		: function(jqXHR){
								swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
							}
						});
					}

				}
			});

		});
		
	}

	init();

});

function maxLengthCheck(object){
    if (object.value.length > object.maxLength){
      object.value = object.value.slice(0, object.maxLength)
    }
}
function play(){
       var audio = document.getElementById("audio");
       audio.play();
}

function load_quagga(){
	if($('#barcode-scanner').length > 0 && navigator.mediaDevices && typeof navigator.mediaDevices.getUserMedia === 'function'){
		Quagga.init({
			inputStream : {
				name : "Live",
				type : "LiveStream",
				numOfWorkers : navigator.hardwareConcurrency,
				target : document.querySelector('#barcode-scanner')
			},
			decoder : {
				readers : ['ean_reader']
			},
			locate : true
		}, function(err) {
			if (err){ console.log(err); return }
		})

		Quagga.onDetected(function(result){
			var last_code = result.codeResult.code;
			var e = $.Event( "keypress", { keyCode: 13 } );
			Quagga.pause();
			$('#scan-barcode').modal('hide');
			$('[name="FK_IM_UPCCode"]').val(last_code);
			$('[name="FK_IM_UPCCode"]').focus();

			setTimeout(function(){
				$('[name="FK_IM_UPCCode"]').trigger(e);
			},500)
		})
	}
}