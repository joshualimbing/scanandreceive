$(document).ready(function(){

	function init(){

		$('[name="Company"]').select2({
			placeholder: 'Select a company',
			allowClear: true
		});

		$('[name="Location"]').select2({
			placeholder: 'Select Location',
			allowClear: true
		});

		$('[name="Company"]').change(function(){

			if($('[name="Company"]').val() != ""){
				$('[name="U_UserID"]').attr('readonly', false);
				$('[name="U_Password"]').attr('readonly', false);
				$('[name="Location"]').prop('disabled', false);

				$.ajax({
					url			: global.site_name + 'user/getCompanyDB',
					dataType	: 'json',
					type 		: 'POST',
					data 		: {
									'company-id'	: $('[name="Company"]').val()
								  },
					success		: function(result){

						$('[name="Location"]').empty();

						var html = ''

						$(result).each(function(index){

							html += '<option value="'+ result[index].Loc_Code +'">'+ result[index].Loc_Desc +'</option>'

						});

						$('[name="Location"]').append(html).trigger('change');

					}
				});
			}
			else{
				$('[name="U_UserID"]').attr('readonly', true);
				$('[name="U_Password"]').attr('readonly', true);
				$('[name="Location"]').empty();
				$('[name="Location"]').prop('disabled', true);
			}

		});

		$('#login').click(function(){

			$.ajax({
				url			: global.site_name + 'user/login',
				dataType	: 'json',
				type 		: 'POST',
				data 		: {
								'company-id'	: $('[name="Company"]').val(),
								'username'		: $('[name="U_UserID"]').val(),
								'password'		: $('[name="U_Password"]').val(),
								'location'		: $('[name="Location"]').val(),
							  },
				success		: function(result){

					if(result.success){
						window.location.href = global.site_name + 'user/menu'
					}
					else{
						swal(result.title, result.message, result.type);
					}

				}
			});

		});

		$('[name="U_UserID"]').keypress(function(e) {
			if(e.keyCode == 13){
				if($('[name="U_UserID"]').val() != "" && $('[name="U_Password"]').val() != ""){
					$('#login').trigger('click');
				}
				else{
					swal("Information", 'Incomplete Username/Password', "info");
				}
			}
		});

		$('[name="U_Password"]').keypress(function(e) {
			if(e.keyCode == 13){
				if($('[name="U_UserID"]').val() != "" && $('[name="U_Password"]').val() != ""){
					$('#login').trigger('click');
				}
				else{
					swal("Information", 'Incomplete Username/Password', "info");
				}
			}
		});

	}

	init();
	
})