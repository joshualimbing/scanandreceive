$(document).ready(function(){

	function init(){
		$('.select-recv-from').select2();

		$('.select-recv-type').select2({
			placeholder: 'Choose an option...',
			allowClear: true
		});

		$('.select-recv-from').select2({
			placeholder: "Enter Receive From...",
			allowClear: true,
			minimumInputLength: 1,
			ajax:
				{
					url: global.site_name + 'scan_received/getReferenceFrom',
					dataType: 'json',
	                delay: 250,
	                data: function (params) {
	                  var queryParameters = {
	                            'filter-type'   : 'Loc_Code'
	                        ,   'filter-search' : params.term
	                        ,   'action'        : 'search'
	                        ,   'recv-type'     : $('.select-recv-type').val()
	                    }
	                    return queryParameters;
	                },
	                processResults: function (data, params) {
	                    return {
	                        results: $.map(data, function(loc) {
	                            return {
	                                id: loc.Loc_Code,
	                                text:loc['Description']
	                            }
	                        })
	                    };
	                }
				}
		});

		$('.select-recv-type').change(function(){
			let recv_type = $('.select-recv-type').val();

			if(recv_type != ''){
				$('.select-recv-from').prop('disabled', false);
				$('.select-recv-from').val('').trigger('change');				
			}
			else{
				$('.select-recv-from').prop('disabled', true);
			}
		});

		$('[name="Ref_No"], #Doc_Remarks').change(function(event) {
			console.log($(this).val())
			let field_input = $(this).val();
			console.log(field_input.indexOf('#'))
			if (field_input.indexOf('#') !== -1) {
				swal('Information', '"#" are not allowed.', 'info');
				$(this).val('')
			}
		});

		$('#next').click(function(){

			let recv_from	= $('.select-recv-from').val();
			let recv_type	= $('.select-recv-type').val();
			let doc_ref 	= $('[name="Ref_No"]').val();
			let pack_no 	= $('[name="Pack_No"]').val();
			let remarks 	= $('#Doc_Remarks').val();

			let rem 		= remarks.replace("&", "%26");

			if($('.select-recv-from').val() == "" || $('.select-recv-type').val() == "" || pack_no == "" || $('[name="Ref_No"]').val() == ""){
				swal("Information", "Complete all required fields", "info");
			}
			else{
				swal({
					title				: "Please wait!",
					type 				: "info",
					showConfirmButton	: false
				});
				
				setTimeout(function(){
					window.location.href = global.site_name + 'scan_received/item_entry?recvfrom=' + recv_from + '&recvtype=' + recv_type + '&pack_no=' + pack_no + '&ref=' + doc_ref + '&remarks=' + rem; 
				}, 1000);
			}
		});
	}

	init();
});

function maxLengthCheck(object){
    if (object.value.length > object.maxLength){
      object.value = object.value.slice(0, object.maxLength)
    }
}