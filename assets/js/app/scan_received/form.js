$(document).ready(function(){

	function init(){
		$(window).on("blur focus", function(e) {
		    location.reload(true);
		})

		// Scan through mobile camera or webcam - Created by Joshua Limbing (09/07/2020)
		
		load_quagga();
		$('#scanButton').click(function(event) {
			Quagga.start();
			$('#scan-barcode').modal('show')
		});

		$('#close-barcode').click(function(event) {
			Quagga.pause();
			$('#scan-barcode').modal('hide')
		});

		// End Scan
		
		$('.undo-btn').click(function(){
			$('.scn-status').text('Unscanning');
			$('.scn-status').css('color', '#FF0101');
		});

		$('.scan-recv-btn').click(function(){
			$('.scn-status').text('Scanning');
			$('.scn-status').css('color', '#000000');
		});

		$('[name="Barcode_Qty"]').keypress(function(e){
			if(e.keyCode == 13){
				$('[name="FK_IM_UPCCode"]').focus();
			}
		});

		$('[name="Bin_Location"]').select2({
			placeholder: 'Choose an option...',
			allowClear: true
		})

		$('#received-detail-list tbody tr').on('click', '.null-loc', function(){
			var current_row = $(this).closest('.received-details');
			let item_code = current_row.find('td:eq(1)').text();
			let actual_loc = current_row.find('td:eq(2)').text();
			console.log(actual_loc);

			$('.item-code-input').val(item_code);

			let list = '';
			$('[name="Bin_Location"]').empty();
			list += '<option value=""></option>';

			$(bin_list).each(function(index) {
				list += '<option value="'+ bin_list[index].Loc_Code +'" '+ (bin_list[index].Loc_Code == actual_loc ? 'selected' : '') +'>'+ bin_list[index].Loc_Code +'</option>'
			});

			$('[name="Bin_Location"]').append(list).trigger('change');
			$('#bin-loc-detail').modal('show');
		});

		$('.close-list').click(function(event) {
			$('#bin-loc-detail').modal('hide');
		});

		// Choose Bin for Warehouse Location Type - Created by Joshua Limbing (10/17/2019)
		$('#bin-loc-detail').on('change', '[name="Bin_Location"]', function(){
			var current_row = $(this).closest('.received-details');
			let item_code = $('.item-code-input').val();
			let bin_loc = $('[name="Bin_Location"]').val();

			$.ajax({
					url			: global.site_name + 'scan_received/updateLocationSource',
					dataType 	: 'json',
					type 		: 'POST',
					data 		: {
									'ref-doc'		: ref_doc,
									'pack-no'		: pack_no,
									'recv-from'		: recv_fr,
									'recv-type'		: recv_type,
									'item-code'		: item_code,
									'bin-loc'		: bin_loc
								  }
			});

			$('#received-detail-list tbody tr td[data-item-code="'+item_code+'"]').text(bin_loc);

		});

		// Barcode for Scan Received - Created by Joshua Limbing (10/1/2019)

		$('[name="FK_IM_UPCCode"]').keypress(function(event) {


			if(event.keyCode == 13){
				let barcode 	= $('[name="FK_IM_UPCCode"]').val().trim();
				let qty 		= $('[name="Barcode_Qty"]').val();
				let status 		= $('.scn-status').text();
				let html_recv	= '';
				let barcode_arr = [];
				
				$('.spinner').show();
				$('input').attr('readonly', true);

				if(barcode == ""){
					$('.spinner').hide();
					$('[name="FK_IM_UPCCode"]').prop('readonly', false);
					$('[name="Barcode_Qty"]').prop('readonly', false);
					$('.item-code-input').prop('readonly', true);
					swal("Information", "No barcode found", "info");
					play();
					$('[name="FK_IM_UPCCode"]').val('');
					$('[name="Barcode_Qty"]').val(1);
				}
				else{
					$.ajax({
						url			: global.site_name + 'scan_received/checkAndgetBarcodeDetails',
						dataType	: 'json',
						type 		: 'POST',
						data 		: {
										'upc-code'		: barcode,
										'qty'			: qty,
										'ref-doc'		: ref_doc,
										'remarks'		: remarks,
										'pack-no'		: pack_no,
										'recv-from'		: recv_fr,
										'recv-type'		: recv_type,
										'status'		: status
									  },
						success 	: function(result){

							if(result.success){

								$('.spinner').hide();
								$('[name="FK_IM_UPCCode"]').prop('readonly', false);
								$('[name="Barcode_Qty"]').prop('readonly', false)
								$('.item-code-input').prop('readonly', true);

								if(status == "Scanning"){
									$('#received-detail-list tbody tr').each(function(){
										let upc_code = $(this).find('td:eq(0)').text();

										barcode_arr.push(upc_code);
									});

									if($.inArray(barcode, barcode_arr) !== -1){

										// Add qty to the current received qty
										$('#received-detail-list tbody tr').each(function(){
											let rec_qty = parseFloat($(this).find('td:eq(3)').text());
											let upc_code = $(this).find('td:eq(0)').text();
											
											if(barcode == upc_code.toString()){
												$(this).find('td:eq(3)').text(parseFloat(result.scn_qty));
											}
										});

										//Get total received qty
										let total_qty = 0;
										$('#received-detail-list tbody tr').each(function(){
											let rec_qty = parseFloat($(this).find('td:eq(3)').text());

											total_qty += rec_qty;
										});

										$('.tot-rec-qty').text(number_format(total_qty));

										$('[name="FK_IM_UPCCode"]').val('');
										$('[name="Barcode_Qty"]').val(1);


									}
									else{
										// Add new row
										html_recv += '<tr class="received-details">' +
														'<td style="font-size: 12px; padding: 5px" data-actual-location="'+ result.loc_source +'">' + barcode + '</td>' +
														'<td style="font-size: 12px; padding: 5px">' + result.item_code + '</td>';
														
										if(loc_type == 'STR'){
											html_recv +=	'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + result.loc_source + '</td>';
										}
										else{
											if(result.loc_source !== null){
												html_recv +=	'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;">' + result.loc_source + '</td>';
											}
											else{
												html_recv +=	'<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;" class="null-loc" data-item-code="' + result.item_code + '">' + (result.loc_source === null ? "" : result.loc_source) + '</td>';
											}
										}
													
										html_recv +=	'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result.scn_qty, 0, ".", "") + '</td>' +
													'</tr>';

										$('#received-detail-list tbody').append(html_recv);

										$('#received-detail-list tbody tr').on('click', '.null-loc', function(){
											var current_row = $(this).closest('.received-details');
											let item_code = current_row.find('td:eq(1)').text();
											let actual_loc = current_row.find('td:eq(2)').text();
											console.log(actual_loc);

											$('.item-code-input').val(item_code);

											let list = '';
											$('[name="Bin_Location"]').empty();
											list += '<option value=""></option>';

											$(bin_list).each(function(index) {
												list += '<option value="'+ bin_list[index].Loc_Code +'" '+ (bin_list[index].Loc_Code == actual_loc ? 'selected' : '') +'>'+ bin_list[index].Loc_Code +'</option>'
											});

											$('[name="Bin_Location"]').append(list).trigger('change');
											$('#bin-loc-detail').modal('show');
										});

										//Get total received qty
										let total_qty = 0;
										$('#received-detail-list tbody tr').each(function(){
											let rec_qty = parseFloat($(this).find('td:eq(3)').text());

											total_qty += rec_qty;
										});

										$('.tot-rec-qty').text(number_format(total_qty));

										$('[name="FK_IM_UPCCode"]').val('');
										$('[name="Barcode_Qty"]').val(1);

									}

								}
								else if(status == 'Unscanning'){
									$('#received-detail-list tbody tr').each(function(){
										let upc_code = $(this).find('td:eq(0)').text();

										barcode_arr.push(upc_code);
									});

									if($.inArray(barcode, barcode_arr) !== -1){

										// Add qty to the current received qty
										$('#received-detail-list tbody tr').each(function(){
											let rec_qty = parseFloat($(this).find('td:eq(3)').text());
											let upc_code = $(this).find('td:eq(0)').text();
											
											if(barcode == upc_code.toString()){
												$(this).find('td:eq(3)').text(parseFloat(result.scn_qty));

												if(rec_qty - parseFloat(qty) <= 0){
													$(this).remove();
												}
											}
										});

										//Get total received qty
										let total_qty = 0;
										$('#received-detail-list tbody tr').each(function(){
											let rec_qty = parseFloat($(this).find('td:eq(3)').text());

											total_qty += rec_qty;
										});

										$('.tot-rec-qty').text(number_format(total_qty));

										$('[name="FK_IM_UPCCode"]').val('');
										$('[name="Barcode_Qty"]').val(1);


									}
									else{
										$('.spinner').hide();
										$('[name="FK_IM_UPCCode"]').prop('readonly', false);
										$('[name="Barcode_Qty"]').prop('readonly', false)
										$('.item-code-input').prop('readonly', true);
										swal("Information", "No item found", "info");
										play();
										$('[name="FK_IM_UPCCode"]').val('');
										$('[name="Barcode_Qty"]').val(1);
									}
								}
								

							}
							else{
								$('.spinner').hide();
								$('[name="FK_IM_UPCCode"]').prop('readonly', false);
								$('[name="Barcode_Qty"]').prop('readonly', false)
								$('.item-code-input').prop('readonly', true);
								swal(result.title, result.message, result.type);
								play();
							}

						},
						error 		: function(jqXHR){
							swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
						}
					});
				}

			}
		});
		
		$('.post-btn').click(function(event) {
			swal({
				title				: "Post Items to be received?",
				type 				: "warning",
				showCancelButton	: true,
				confirmButtonText	: "Yes",
				confirmButtonColor	: "#3989C8",
				closeOnConfirm		: false
			}, function(isConfirm){
				if(isConfirm){

					let hasMissingLoc = 0;

					$('#received-detail-list tbody tr').each(function() {
						let item_loc = $(this).find('td:eq(2)').text();

						if(item_loc == ""){
							hasMissingLoc += 1;
						}
					});

					if(hasMissingLoc > 0){
						swal('Information', 'All items must have actual location before posting.', 'info');
					}
					else{
						
						swal({
							title				: "Please wait!",
							text 				: "Posting items to receive",
							type 				: "info",
							showConfirmButton	: false
						});

						$.ajax({
							url			: global.site_name + 'scan_received/postReceived',
							dataType	: 'json',
							type 		: 'POST',
							data 		: {
											'recv-type'		: recv_type,
											'recv-from'		: recv_fr,
											'pack-no'		: pack_no,
											'remarks'		: remarks,
											'ref-doc'		: ref_doc,
											'total'			: parseFloat(number_format($('.tot-rec-qty').text(), 0, ".", ""))
										  },
							success		: function(result){
								if(result.success){
									swal("Success", "Posted items successfully", "success");
									setTimeout(function(){
										window.location.href = global.site_name + 'scan_received'
									}, 1000)
								}
								else{
									swal(result.title, result.message, result.type);
								}
							},
							error 		: function(jqXHR){
								swal('Error '+jqXHR.status, 'Contact your System Administrator', 'error');
							}
						});

					}

				}
			});
		});
		
	}

	init();

});

function maxLengthCheck(object){
    if (object.value.length > object.maxLength){
      object.value = object.value.slice(0, object.maxLength)
    }
}
function play(){
       var audio = document.getElementById("audio");
       audio.play();
}
function load_quagga(){
	if($('#barcode-scanner').length > 0 && navigator.mediaDevices && typeof navigator.mediaDevices.getUserMedia === 'function'){
		Quagga.init({
			inputStream : {
				name : "Live",
				type : "LiveStream",
				numOfWorkers : navigator.hardwareConcurrency,
				target : document.querySelector('#barcode-scanner')
			},
			decoder : {
				readers : ['ean_reader']
			},
			locate : true
		}, function(err) {
			if (err){ console.log(err); return }
		})

		Quagga.onDetected(function(result){
			var last_code = result.codeResult.code;
			var e = $.Event( "keypress", { keyCode: 13 } );
			Quagga.pause();
			$('#scan-barcode').modal('hide');
			$('[name="FK_IM_UPCCode"]').val(last_code);
			$('[name="FK_IM_UPCCode"]').focus();

			setTimeout(function(){
				$('[name="FK_IM_UPCCode"]').trigger(e);
			},500)
		})
	}
}