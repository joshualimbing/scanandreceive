$(document).ready(function(){

	function init(){
		$('.select-pcount-no').select2();
		$('.select-csheet-no').select2();

		$('.select-pcount-no').select2({
			placeholder: "Enter Plan Count No",
			allowClear: true,
			minimumInputLength: 2,
			ajax:
				{
					url: global.site_name + 'item_count/getPlanCountNo',
					dataType: 'json',
	                delay: 250,
	                data: function (params) {
	                  var queryParameters = {
	                            'filter-type'   : 'PCH_CountNum'
	                        ,   'filter-search' : params.term
	                    }
	                    return queryParameters;
	                },
	                processResults: function (data, params) {
	                    return {
	                        results: $.map(data, function(pcount) {
	                            return {
	                                id: pcount.PCH_CountNum,
	                                text:pcount['PCH_CountNum']
	                            }
	                        })
	                    };
	                }
				}
		});

		$('.select-csheet-no').select2({
			placeholder: "Enter Countsheet No",
			allowClear: true,
			minimumInputLength: 1,
			ajax:
				{
					url: global.site_name + 'item_count/getCountSheetNo',
					dataType: 'json',
	                delay: 250,
	                data: function (params) {
	                  var queryParameters = {
	                            'filter-type'   : 'CSH_CountSheetNo'
	                        ,   'filter-pcount' : $('.select-pcount-no').val()
	                        ,   'filter-search' : params.term
	                        ,   'action'        : 'search'
	                    }
	                    return queryParameters;
	                },
	                processResults: function (data, params) {
	                    return {
	                        results: $.map(data, function(cs) {
	                            return {
	                                id: cs.CSH_CountSheetNo,
	                                text:cs['Description']
	                            }
	                        })
	                    };
	                }
				}
		});

		$('.select-pcount-no').change(function() {
			let pcount 	= $('.select-pcount-no').val();

			if(pcount != ""){
				$('.select-csheet-no').prop('disabled', false);
			}
			else{
				$('.select-csheet-no').val('');
				$('.select-csheet-no').prop('disabled', true);
			}

		});

		$('.back').click(function(){
			window.location.href = global.site_name + 'user/menu';
		});

		$('#next').click(function(){
			let pcount 	= $('.select-pcount-no').val();
			let csheet 	= $('.select-csheet-no').val();

			if(pcount == "" || csheet == ""){
				swal("Information", "Complete all required fields", "info");
			}
			else{
				$.ajax({
					url			: global.site_name + 'item_count/next',
					dataType	: 'json',
					type 		: 'POST',
					data 		: {
									'pcount' 	: pcount,
									'csheet'	: csheet
								  },
					success 	: function(result){
						if(result.success){
							window.location.href = global.site_name + 'item_count/item_entry?pcount=' + pcount + '&csheet=' + csheet; 
						}	
						else{
							swal(result.title, result.message, result.type);
						}
					}
				});
			}
		});
	}

	init();

});

