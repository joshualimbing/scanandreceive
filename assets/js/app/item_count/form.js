$(document).ready(function(){

	function init(){

		$(window).on("blur focus", function(e) {
		    location.reload(true);
		})

		// Scan through mobile camera or webcam - Created by Joshua Limbing (09/07/2020)
		
		load_quagga();
		$('#scanButton').click(function(event) {
			Quagga.start();
			$('#scan-barcode').modal('show')
		});

		$('#close-barcode').click(function(event) {
			Quagga.pause();
			$('#scan-barcode').modal('hide')
		});

		// End Scan

		$('.back-btn').click(function(event) {
			window.location.href = global.site_name + 'item_count';
		});

		$('.undo-btn').click(function(){
			$('.scn-status').text('Unscanning');
			$('.scn-status').css('color', '#FF0101');
		});

		$('.scan-btn').click(function(){
			$('.scn-status').text('Scanning');
			$('.scn-status').css('color', '#000000');
		});

		$('[name="Barcode_Qty"]').keypress(function(e){
			if(e.keyCode == 13){
				$('[name="FK_IM_UPCCode"]').focus();
			}
		});

		// Reset Button - Created by Joshua Limbing (10/7/2019)

		$('.reset-btn').click(function() {
			swal({
				title				: "Do you want to reset the table?",
				type 				: "warning",
				showCancelButton	: true,
				confirmButtonText	: "Reset",
				confirmButtonColor	: "#3989C8",
				closeOnConfirm		: false
			},function(isConfirm){

				if(isConfirm){
					swal({
						title				: "Please wait!",
						text 				: "Resetting table...",
						type 				: "info",
						showConfirmButton	: false
					});

					$.ajax({
						url			: global.site_name + 'item_count/reset',
						dataType 	: 'json',
						type 		: 'POST',
						data 		: {
										'pcount'		: p_count,
										'csheet' 		: c_sheet
									  },
						success 	: function(result){

							if(result.success){
								swal("Success", "Reset table successfully", "success")

								let html_sv_row = '';

								$(result.detail).each(function(index) {
									html_sv_row += '<tr class="item-count-details">' +
														'<td style="font-size: 12px; padding: 5px">' + result.detail[index].IM_UPCCode + '</td>' +
														'<td style="font-size: 12px; padding: 5px">' + result.detail[index].PKFK_InvMaster_Code + '</td>' +
														'<td style="font-size: 12px; padding: 5px" class="text-right">' + number_format(result.detail[index].ScanQty) + '</td>' +
												   '</tr>'

								});
								
								$('#item-count-detail-list tbody').html(html_sv_row);
								$('.scn-qty').text(number_format(result.total_scn));
							}
							else{
								swal(result.title, result.message, result.type);
							}

						}
					});
				}


			});
		});

		// Barcode for Item Count - Created by Joshua Limbing (10/7/2019)

		$('[name="FK_IM_UPCCode"]').keypress(function(event) {

			if(event.keyCode == 13){
				let barcode 	= $('[name="FK_IM_UPCCode"]').val().trim();
				let qty 		= parseFloat($('[name="Barcode_Qty"]').val());
				let status 		= $('.scn-status').text();
				let total_scn 	= parseFloat($('.scn-qty').text());
				let html_recv	= '';
				let barcode_arr = [];

				$('.spinner').show();
				$('input').attr('readonly', true);
				
				if(barcode == ""){
					$('.spinner').hide();
					$('[name="FK_IM_UPCCode"]').prop('readonly', false);
					$('[name="Barcode_Qty"]').prop('readonly', false);
					swal("Information", "No barcode found", "info");
					play();
					$('[name="FK_IM_UPCCode"]').val('');
					$('[name="Barcode_Qty"]').val(1);
				}
				else{
					if(qty == "" || qty == 0){
						$('.spinner').hide();
						$('[name="FK_IM_UPCCode"]').prop('readonly', false);
						$('[name="Barcode_Qty"]').prop('readonly', false);
						swal("Error", "No Qty input found", "error");
						play();
						$('[name="FK_IM_UPCCode"]').val('');
						$('[name="Barcode_Qty"]').val(1);
					}
					else{
						
						let barcode_arr = [];

						$('#item-count-detail-list tbody tr').each(function() {
							let upc_code = $(this).find('td:eq(0)').text();

							barcode_arr.push(upc_code);
						});

						console.log(barcode_arr);

						// For existing item in the table

						if($.inArray(barcode, barcode_arr) !== -1){
							if(status == 'Scanning'){
								$('#item-count-detail-list tbody tr').each(function() {
									let current_row = $(this).closest('.item-count-details');
									let upc_code 	= $(this).find('td:eq(0)').text();
									let item_code 	= $(this).find('td:eq(1)').text();
									let scn_qty 	= parseFloat($(this).find('td:eq(2)').text());
									let isOriginal 	= $(this).data('is-original');

									if(barcode == upc_code){

										$.ajax({
											url			: global.site_name + 'item_count/scanItem',
											dataType 	: 'json',
											type 		: 'POST',
											data 		: {
															'pcount'		: p_count,
															'csheet' 		: c_sheet,
															'upc-code'		: upc_code,
															'item-code' 	: item_code,
															'scn-qty' 		: scn_qty + qty,
															'is-original'	: isOriginal,
															'status' 		: status
														  },
											success 	: function(result_scn){
												current_row.find('td:eq(2)').text(number_format(result_scn, 0, ".", ""));

												let ic_scan_qty = 0;

												$('#item-count-detail-list tbody tr').each(function() {
													let ic_scn = parseFloat($(this).find('td:eq(2)').text());

													ic_scan_qty += ic_scn;
												});

												$('.scn-qty').text(ic_scan_qty);

												$('.spinner').hide();
												$('[name="FK_IM_UPCCode"]').prop('readonly', false);
												$('[name="Barcode_Qty"]').prop('readonly', false);
												$('[name="FK_IM_UPCCode"]').val('');
												$('[name="Barcode_Qty"]').val(1);

											}
										});
									}
								});
							}
							else if(status == 'Unscanning'){
								$('#item-count-detail-list tbody tr').each(function() {
									let current_row = $(this).closest('.item-count-details');
									let upc_code 	= $(this).find('td:eq(0)').text();
									let item_code 	= $(this).find('td:eq(1)').text();
									let scn_qty 	= parseFloat($(this).find('td:eq(2)').text());
									let isOriginal 	= $(this).data('is-original');

									if(barcode == upc_code){
										if( (scn_qty - qty) < 0 ){
											$('.spinner').hide();
											$('[name="FK_IM_UPCCode"]').prop('readonly', false);
											$('[name="Barcode_Qty"]').prop('readonly', false);
											swal("Error", "Scanned Qty cannot be less than zero", "error");
											play();
											$('[name="FK_IM_UPCCode"]').val('');
											$('[name="Barcode_Qty"]').val(1);
										}
										else{
											$(this).find('td:eq(2)').text(scn_qty - qty);
											$('.scn-qty').text(total_scn - qty);

											$.ajax({
												url			: global.site_name + 'item_count/scanItem',
												dataType 	: 'json',
												type 		: 'POST',
												data 		: {
																'pcount'		: p_count,
																'csheet' 		: c_sheet,
																'upc-code'		: upc_code,
																'item-code' 	: item_code,
																'scn-qty' 		: scn_qty - qty,
																'is-original'	: isOriginal,
																'status' 		: status
															  },
												success 	: function(result_scn){
													current_row.find('td:eq(2)').text(number_format(result_scn, 0, ".", ""));

													let ic_scan_qty = 0;

													$('#item-count-detail-list tbody tr').each(function() {
														let ic_scn = parseFloat($(this).find('td:eq(2)').text());

														ic_scan_qty += ic_scn;
													});

													$('.scn-qty').text(ic_scan_qty);

													if( (result_scn == 0) && isOriginal == 'False' ){
														$(this).remove();
													}

													$('.spinner').hide();
													$('[name="FK_IM_UPCCode"]').prop('readonly', false);
													$('[name="Barcode_Qty"]').prop('readonly', false);
													$('[name="FK_IM_UPCCode"]').val('');
													$('[name="Barcode_Qty"]').val(1);
												}
											});

										}

									}
								});
							}
						}

						// For non-existing item in the table

						else{
							if(status == 'Scanning'){
								let html_ic_row = ''
								let total_scn 	= parseFloat($('.scn-qty').text());

								$.ajax({
									url			: global.site_name + 'item_count/createICItem',
									dataType 	: 'json',
									type 		: 'POST',
									data 		: {
													'pcount'		: p_count,
													'csheet' 		: c_sheet,
													'upc-code'		: barcode,
													'scn-qty' 		: qty,
													'is-original'	: false,
													'status' 		: status
												  },
									success 	: function(output){

										if(output.success){
											
											html_ic_row = '<tr class="item-count-details" data-is-original="False">' +
															'<td style="font-size: 12px; padding: 5px">' + barcode + '</td>' +
															'<td style="font-size: 12px; padding: 5px">' + output.item_code + '</td>' +
															'<td style="font-size: 12px; padding: 5px" class="text-right">' + qty + '</td>'
														  '</tr>'

											$('#item-count-detail-list tbody').append(html_ic_row);
											$('.scn-qty').text(total_scn + qty);

										}

										else{
											swal(output.title, output.message, output.type);
											play();
										}


										$('.spinner').hide();
										$('[name="FK_IM_UPCCode"]').prop('readonly', false);
										$('[name="Barcode_Qty"]').prop('readonly', false);
										$('[name="FK_IM_UPCCode"]').val('');
										$('[name="Barcode_Qty"]').val(1);
									}
								});

							}
							else if(status == 'Unscanning'){
								$('.spinner').hide();
								$('[name="FK_IM_UPCCode"]').prop('readonly', false);
								$('[name="Barcode_Qty"]').prop('readonly', false);
								swal("Error", "No item found", "error");
								play();
								$('[name="FK_IM_UPCCode"]').val('');
								$('[name="Barcode_Qty"]').val(1);
							}
						}
					}
				}

			}
		});

		// Save button - Created by Joshua Limbing (10/7/2019)

		$('.save-btn').click(function(){

			swal({
				title				: "Save Countsheet Items?",
				type 				: "warning",
				showCancelButton	: true,
				confirmButtonText	: "Yes",
				confirmButtonColor	: "#3989C8",
				closeOnConfirm		: false
			}, function(isConfirm){
				if(isConfirm){

					swal({
						title				: "Please wait!",
						text 				: "Saving items...",
						type 				: "info",
						showConfirmButton	: false
					});

					$.ajax({
						url			: global.site_name + 'item_count/saveCountSheet',
						dataType	: 'json',
						type 		: 'POST',
						data 		: {
										'pcount'		: p_count,
										'csheet' 		: c_sheet
									  },
						success		: function(result){
							if(result.success){
								swal("Success", "Saved items successfully", "success");
								setTimeout(function(){
									window.location.href = global.site_name + 'item_count'
								}, 1000)
							}
							else{
								swal(result.title, result.message, result.type);
							}
						}
					});
				}
			});

		});

	}

	init();

});

function maxLengthCheck(object){
    if (object.value.length > object.maxLength){
      object.value = object.value.slice(0, object.maxLength)
    }
}
function play(){
       var audio = document.getElementById("audio");
       audio.play();
}
function load_quagga(){
	if($('#barcode-scanner').length > 0 && navigator.mediaDevices && typeof navigator.mediaDevices.getUserMedia === 'function'){
		Quagga.init({
			inputStream : {
				name : "Live",
				type : "LiveStream",
				numOfWorkers : navigator.hardwareConcurrency,
				target : document.querySelector('#barcode-scanner')
			},
			decoder : {
				readers : ['ean_reader']
			},
			locate : true
		}, function(err) {
			if (err){ console.log(err); return }
		})

		Quagga.onDetected(function(result){
			var last_code = result.codeResult.code;
			var e = $.Event( "keypress", { keyCode: 13 } );
			Quagga.pause();
			$('#scan-barcode').modal('hide');
			$('[name="FK_IM_UPCCode"]').val(last_code);
			$('[name="FK_IM_UPCCode"]').focus();

			setTimeout(function(){
				$('[name="FK_IM_UPCCode"]').trigger(e);
			},500)
		})
	}
}