$(document).ready(function(){
	var timer = 0;
	var timeoutID;
 
	function setup() {
	    this.addEventListener("mousemove", resetTimer, false);
	    this.addEventListener("mousedown", resetTimer, false);
	    this.addEventListener("keypress", resetTimer, false);
	    this.addEventListener("DOMMouseScroll", resetTimer, false);
	    this.addEventListener("mousewheel", resetTimer, false);
	    this.addEventListener("touchmove", resetTimer, false);
	    this.addEventListener("MSPointerMove", resetTimer, false);
	 
	    startTimer();
	}
	setup();
	 
	 
	function resetTimer(e) {
	    window.clearTimeout(timeoutID);
	    timer = 0;
	}
	 
	function startTimer() {
	    setInterval(function(){
			timer += 10000;

			if((timer * 0.001) < 60){
				console.warn('You have been idle for ' + (timer * 0.001) + ' seconds');
			}
			else if((timer * 0.001) == 60){
				console.warn('You have been idle for 1 minute');
			}
			else if((timer * 0.001) > 60){
				if((timer * 0.001) % 60 == 0){
					console.warn('You have been idle for ' + ((timer * 0.001)/60) + ' minutes');
				}
				else{
					console.warn('You have been idle for ' + Math.floor((timer * 0.001)/60) + ' minutes and ' + ((timer * 0.001) % 60) + ' seconds');
				}
			}

			console.log(timer);
			$.ajax({
				url 		: global.site_name + 'user/checkSession',
				dataType 	: 'json',
				type 		: 'POST',
				data 		: {
								'timer' 	: timer
							  },
				success 	: function(result){
					if(result){
						if(timer == 600000){
							$('body').find('input').prop('disabled', true);
							$('body').find('select').prop('disabled', true);
							setTimeout(function(){
								window.location.href = global.site_name;
							}, 500);
						}
					}
					else{
						if(timer > 300000){
							swal("You have been idle for more than 5 minutes!", 'Click "OK" to continue', '');
						}
					}
				}
			});
		}, 10000)
	}

});
