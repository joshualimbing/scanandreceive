<?php 
defined('BASEPATH') or exit('No direct script access allowed');

/**
 Scanpack Model created by Joshua Limbing 9/24/2019
 */
class scan_pack_model extends MAIN_Model
{

	public function checkBarcode($location, $barcode){

		$query_string = 'SELECT     LTRIM(RTRIM(PNH_Picknum)) AS PNH_Picknum
						FROM         tblPROD_PickNote
						WHERE     (PNH_Location_Code = \''.$location.'\') AND (FK_Status_Code IN (\'PCK\', \'SCN\')) AND (PNH_BarcodeID = \''.$barcode.'\')';

		return $this->DB->query($query_string)->row_array();
	}

	public function checkSCNCarton($location, $pl, $carton_no){
		$query_string = 'SELECT		RTRIM(CPH_CartonNo) AS CPH_CartonNo, FK_PNH_PickNum, RTRIM(FK_Status_Code) AS FK_Status_Code
						FROM		tblPROD_CartonPacked
						WHERE		(CPH_CartonNo = \''.$carton_no.'\') AND (FK_PNH_PickNum = \''.$pl.'\') AND (CPH_LocCode = \''.$location.'\') AND FK_Status_Code = \'SCN\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->num_rows();
		}
	}

	public function getLastUser($location, $pl, $carton_no){
		$query_string = 'SELECT		CreatedBy
						 FROM 		tblPROD_CartonPacked
						 WHERE		CPH_LocCode = \''.$location.'\' AND FK_PNH_Picknum = \''.$pl.'\' AND CPH_CartonNo = \''.$carton_no.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['CreatedBy'];
		}
	}

	public function updateWSID($location, $pl, $current_user){
		$this->DB->where('PNH_Location_Code', $location)
				 ->where('PNH_Picknum', $pl)
				 ->set('PNH_FK_WSID', $current_user)
				 ->update('tblPROD_PickNote');
	}

	public function getAllBySearch($type, $filter){

		$where_clause = '';
        $where_clause .= "WHERE ".$type." LIKE '%". $filter ."%' " ;

		$query_string = 'SELECT PNH_Picknum
                        FROM tblPROD_PickNote
                        '.$where_clause.'AND FK_Status_Code = \'SCN\'
                        ORDER BY PNH_Picknum';
        
        if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array();
		}
	}

	public function checkCartonExist($location ,$pl, $carton_no){
		$query_string = 'SELECT		RTRIM(CPH_CartonNo) AS CPH_CartonNo, FK_PNH_PickNum, RTRIM(FK_Status_Code) AS FK_Status_Code
						FROM		tblPROD_CartonPacked
						WHERE		(CPH_CartonNo = \''.$carton_no.'\') AND (FK_PNH_PickNum = \''.$pl.'\') AND (CPH_LocCode = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}

	public function checkCartonDetailExist($location ,$pl, $carton_no, $item_code, $loc_source){
		$query_string = 'SELECT		*
						FROM		tblPROD_CartonPackedDetail
						WHERE		(PKFK_CPH_CartonNo = \''.$carton_no.'\') AND (PKFK_PNH_PickNum = \''.$pl.'\') AND (PKFK_CPH_Location_Code = \''.$location.'\') AND (PKFK_InvMaster_Code = \''.$item_code.'\') AND (CPD_Location_Source = \''.$loc_source.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}

	public function countLocation($location, $barcode){
		$query_string = 'SELECT     PKFK_InvMaster_Code, COUNT(PKFK_Location_Source) AS LocCount
						FROM         tblPROD_PickNoteDetail
						WHERE     (PKFK_PNH_LocCode = \''.$location.'\') AND (PKFK_PNH_PickNum = \''.$barcode.'\')
						GROUP BY PKFK_InvMaster_Code
						HAVING      (COUNT(PKFK_Location_Source) > 1)';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array();
		}
	}

	public function getPickListNo($location){


		$query_string = 'SELECT     LTRIM(RTRIM(PNH_Picknum)) AS PNH_Picknum
						FROM         tblPROD_PickNote
						WHERE     (PNH_Location_Code = \''.$location.'\') AND (FK_Status_Code IN (\'PCK\', \'SCN\'))';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array();
		}
	}

	public function getPNHeaderbyBarcode($barcode, $location, $carton_no = ''){
		$query_string = 'SELECT        	LTRIM(RTRIM(PNH.PNH_Picknum)) AS PNH_Picknum, PNH.PNH_TotalOrdQty, PNH.PNH_TotalPackedQty, PNH.FK_RH_Location_Destin, PNH.PNH_TotalPickQty, RTRIM(PNH.FK_RH_IRNum) AS FK_RH_IRNum,
                        				RTRIM(PNH.FK_RH_Location_Destin) AS FK_RH_Location_Destin ';
        if($carton_no != ''){
        	$query_string .= ', (SELECT ISNULL(CPH_PackedQty, 0) AS CartonQty FROM tblPROD_CartonPacked WHERE 
							  FK_PNH_Picknum = \''.$pl.'\' 
            				  AND CPH_CartonNo = \''.$carton_no.'\' AND CPH_LocCode = \''.$location.'\') 
            				  AS TotalPackedQty';
        }
        $query_string .= ' FROM            tblPROD_PickNote AS PNH 
						  WHERE 			(PNH_BarcodeID = \''.$barcode.'\') AND (PNH_Location_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}

	public function getPNHeaderbyPicklistNo($pl, $location, $carton_no = ''){
		$query_string = 'SELECT        	LTRIM(RTRIM(PNH.PNH_Picknum)) AS PNH_Picknum, PNH.PNH_TotalOrdQty, PNH.PNH_TotalPackedQty, PNH.FK_RH_Location_Destin, PNH.PNH_TotalPickQty, RTRIM(PNH.FK_RH_IRNum) AS FK_RH_IRNum,
                        				RTRIM(PNH.FK_RH_Location_Destin) AS FK_RH_Location_Destin ';
        if($carton_no != ''){
        	$query_string .= ', (SELECT ISNULL(CPH_PackedQty, 0) AS CartonQty FROM tblPROD_CartonPacked WHERE 
							  FK_PNH_Picknum = \''.$pl.'\' 
            				  AND CPH_CartonNo = \''.$carton_no.'\' AND CPH_LocCode = \''.$location.'\') 
            				  AS TotalPackedQty';
        }
		$query_string .= ' FROM            tblPROD_PickNote AS PNH 
						  WHERE 			(PNH_Picknum = \''.$pl.'\') AND (PNH_Location_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}


	public function getAllCartonHeaderByPND($location, $loc_source, $upc_code, $pl){
		$query_string = 'SELECT CPH_CartonNo, FK_Status_Code, CPH_PackedQty
						 FROM   tblPROD_CartonPacked
						 WHERE  CPH_LocCode = \''.$location.'\' 
						 		AND FK_PNH_Picknum = (SELECT PKFK_PNH_PickNum 
													  FROM tblPROD_CartonPackedDetail 
													  WHERE CPD_Location_Source = \''.$loc_source.'\' AND FK_IM_UPCCode = \''.$upc_code.'\' AND PKFK_CPH_CartonNo = CPH_CartonNo 
														    AND PKFK_PNH_PickNum = \''.$pl.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array();
		}

	}

	public function getPNDetails($pl, $location, $carton_no){
		$query_string = 'SELECT       RTRIM(PKFK_PNH_LocCode) AS PKFK_PNH_LocCode, PKFK_PNH_PickNum, RTRIM(PKFK_Location_Source) AS PKFK_Location_Source , 
									  RTRIM(PKFK_InvMaster_Code) AS PKFK_InvMaster_Code, 
									  RTRIM(FK_IM_Item_Desc) AS FK_IM_Item_Desc, RTRIM(FK_IM_UPCCode) AS FK_IM_UPCCode, RTRIM(FK_Color_Code) AS FK_Color_Code, PND_PickQty, PND_PackedQty, ';

		if($carton_no != ''){
			$query_string .= '			  ISNULL((SELECT ISNULL(CPD.CPD_PackedQty, 0) as CPD_PackedQty 
										  FROM tblPROD_CartonPackedDetail AS CPD
										  WHERE CPD.PKFK_CPH_Location_Code = \''.$location.'\' AND CPD.PKFK_PNH_PickNum = \''.$pl.'\' AND CPD.PKFK_CPH_CartonNo = \''.$carton_no.'\'
												AND CPD.PKFK_InvMaster_Code = PND.PKFK_InvMaster_Code), 0) AS ScanQty,';
		}

		$query_string .=			 'RTRIM(FK_Size_Code) AS FK_Size_Code, RTRIM(FK_Category_Code) AS FK_Category_Code, RTRIM(PKFK_Location_Destin) AS PKFK_Location_Destin, S_Seq
						FROM          tblPROD_PickNoteDetail AS PND LEFT OUTER JOIN
									  tblSizes AS S ON S.S_Code = PND.FK_Size_Code
						WHERE         (PKFK_PNH_LocCode = \''.$location.'\') AND (PKFK_PNH_PickNum = \''.$pl.'\')
						ORDER BY     FK_Category_Code, FK_Color_Code, S_Seq ASC';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array();
		}
	}

	public function getCurrentPNDPackedQty($pl, $item_code, $location, $loc_source){
		$query_string = 'SELECT 	PND_PackedQty 
						FROM		tblPROD_PickNoteDetail
						WHERE 		(PKFK_PNH_LocCode = \''.$location.'\') AND (PKFK_PNH_PickNum = \''.$pl.'\') AND (PKFK_InvMaster_Code = \''.$item_code.'\') AND (PKFK_Location_Source = \''.$loc_source.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['PND_PackedQty'];
		}
	}

	public function getCurrentPNDPickQty($pl, $item_code, $location, $loc_source){
		$query_string = 'SELECT 	PND_PickQty 
						FROM		tblPROD_PickNoteDetail
						WHERE 		(PKFK_PNH_LocCode = \''.$location.'\') AND (PKFK_PNH_PickNum = \''.$pl.'\') AND (PKFK_InvMaster_Code = \''.$item_code.'\') AND (PKFK_Location_Source = \''.$loc_source.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['PND_PickQty'];
		}
	}

	public function getPackQty($location, $pl){
		$query_string = 'SELECT 	PNH_TotalPackedQty
						FROM 		tblPROD_PickNote
						WHERE		(PNH_Picknum = \''.$pl.'\') AND (PNH_Location_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['PNH_TotalPackedQty'];
		}
	}

	public function getIRNumberByPickListNo($pl, $location){
		$query_string = 'SELECT		RTRIM(FK_RH_IRNum) AS FK_RH_IRNum
						FROM		tblPROD_PickNote
						WHERE		(PNH_Picknum = \''.$pl.'\') AND (PNH_Location_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['FK_RH_IRNum'];
		}
	}

	// Get the latest DR Number

	private function getCurrentDRNo($location){

		$this->updateDelLastRefDoc($location);

		$query_string = 'SELECT     ISNULL(TT_LastDocRef, 0) AS TT_LastDocRef
						FROM         tblTranstype
						WHERE     	(TT_Code = \'DEL\') AND (PKFK_Location_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			$lastrefdoc = $this->DB->query($query_string)->row_array()['TT_LastDocRef'];
			return $this->padLeftZero(10, $lastrefdoc);
		}
	}

	private function padLeftZero($num, $txt){

        $pcnt = $num - strlen($txt);
        $output = '';

        for ($i=0; $i < $pcnt; $i++) { 
            $output .= '0';
        }

        $output = $output.$txt;

        return $output;

    }

    // Update last DR Number

	private function updateDelLastRefDoc($location){
		$query_string = 'UPDATE tblTranstype
						SET TT_LastDocRef = TT_LastDocRef + 1, TT_DocBarcodeID = TT_DocBarcodeID + 1
						WHERE PKFK_Location_Code = \''.$location.'\' AND TT_Code = \'DEL\'';

		
		if($this->sess->isLogin()){
			$this->DB->query($query_string);
		}
	}

	// End

	private function getBarcodeByPicklistNo($location, $pl){
		$query_string = 'SELECT		PNH_BarcodeID
						 FROM 		tblPROD_PickNote
						 WHERE		PNH_Location_Code = \''.$location.'\' AND PNH_Picknum = \''.$pl.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['PNH_BarcodeID'];
		}
	}

	private function getOnholdQty($loc_source, $item_code){
		$query_string = 'SELECT		INV_Onhold
						 FROM 		tblInvdetails
						 WHERE		PKFK_Location_Code = \''.$loc_source.'\' AND PKFK_InvMaster_Code = \''.$item_code.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['INV_Onhold'];
		}
	}

	private function getOnhandQty($loc_source, $item_code){
		$query_string = 'SELECT		INV_Onhand
						 FROM 		tblInvdetails
						 WHERE		PKFK_Location_Code = \''.$loc_source.'\' AND PKFK_InvMaster_Code = \''.$item_code.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['INV_Onhand'];
		}
	}

	private function getTotalDelQty($ir_num, $location){
		$query_string = 'SELECT 	RH_TotDelQty, RH_TotPckQty
						 FROM 		tblRequesthdr
						 WHERE		RH_IRNum = \''.$ir_num.'\' AND RH_Location_Source = \''.$location.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}

	private function getDtlDelQty($location, $ir_num, $item_code){
		$query_string = 'SELECT 	RD_PckQty, RD_DelQty
						 FROM		tblRequestdet
						 WHERE		RD_PKFK_Source = \''.$location.'\' AND RD_PKFK_IRNum = \''.$ir_num.'\' AND FK_InvMaster_Code = \''.$item_code.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}

	public function on_close_PL($pl, $pn_hdr, $pn_dtl, $location, $dbase){

		$tot_del = $this->getTotalDelQty($pn_hdr['FK_RH_IRNum'], $location);

		$dbase->where('PNH_Location_Code', $location)
				 ->where('PNH_Picknum', $pl)
				 ->set('PNH_TotalUnScanQty', $pn_hdr['PNH_TotalPickQty'] - $pn_hdr['PNH_TotalPackedQty'])
				 ->update('tblPROD_PickNote');


		$update_rh_hdr = array(
			'FK_Status_Code' 	=> 'DEL',
			'RH_Locked' 		=> 1,
			'RH_TotDelQty' 		=> $tot_del['RH_TotDelQty'] + $pn_hdr['PNH_TotalPackedQty']
		);

		$dbase->where('RH_Location_Source', $location)
				 ->where('RH_IRNum', $pn_hdr['FK_RH_IRNum'])
				 ->where('RH_Location_Destin', $pn_hdr['FK_RH_Location_Destin'])
				 ->update('tblRequesthdr', $update_rh_hdr);

		foreach ($pn_dtl as $key => $detail) {

			// $onhold_qty = $this->getOnholdQty($detail['PKFK_Location_Source'], $detail['PKFK_InvMaster_Code']);
			// $onhand_qty = $this->getOnhandQty($detail['PKFK_Location_Source'], $detail['PKFK_InvMaster_Code']);

			$dbase->where('PKFK_PNH_LocCode', $location)
					 ->where('PKFK_PNH_PickNum', $pl)
					 ->where('PKFK_Location_Source', $detail['PKFK_Location_Source'])
					 ->where('PKFK_InvMaster_Code', $detail['PKFK_InvMaster_Code'])
					 ->set('PND_UnScanQty', $detail['PND_PickQty'] - $detail['PND_PackedQty'])
					 ->update('tblPROD_PickNoteDetail');

			$dbase->where('RD_PKFK_Source', $location)
					 ->where('RD_PKFK_Destin', $pn_hdr['FK_RH_Location_Destin'])
					 ->where('RD_PKFK_IRNum', $pn_hdr['FK_RH_IRNum'])
					 ->where('FK_InvMaster_Code', $detail['PKFK_InvMaster_Code'])
					 ->set('RD_DelQty', 'RD_DelQty + '.$detail['PND_PackedQty'], false)
					 ->update('tblRequestdet');


			$dbase->where('PKFK_Location_Code', $detail['PKFK_Location_Source'])
					 ->where('PKFK_InvMaster_Code', $detail['PKFK_InvMaster_Code'])
					 ->set('INV_Onhold', 'INV_Onhold - '.($detail['PND_PickQty'] - $detail['PND_PackedQty']), false)
					 ->set('INV_Onhand', 'INV_Onhand + '.($detail['PND_PickQty'] - $detail['PND_PackedQty']), false)
					 ->update('tblInvdetails');

		}

		$dbase->where('PNH_Location_Code', $location)
				 ->where('PNH_Picknum', $pl)
				 ->set('FK_Status_Code', 'CPL')
				 ->update('tblPROD_PickNote');


	}

	public function on_close_carton($location, $carton_hdr, $carton_dtl, $dbase){

		$dr_no = $this->getCurrentDRNo($location);
		$barcode = $this->getBarcodeByPicklistNo($location, $carton_hdr['FK_PNH_Picknum']);

		$save_del_hdr = array(
			'PKFK_CPH_LocCode' 				=> $carton_hdr['CPH_LocCode'],
			'PKFK_CPH_CartonNo' 			=> $carton_hdr['CPH_CartonNo'],
			'DH_DRNum' 						=> 'DR'.$dr_no,
			'FK_Location_Destin' 			=> $carton_hdr['FK_RH_LocDestin'],
			'FK_Status_Code' 				=> 'DEL',
			'DH_PreparedBy'					=> $this->session->userdata('current_user')['login-user'],
			'DH_Remarks'					=> '',	
			'DH_TotalDelQty'				=> $carton_hdr['CPH_PackedQty'],
			'FK_Users_Postedby'				=> $this->session->userdata('current_user')['login-user'],
			'FK_PickNoteHdr_PickNum'		=> $carton_hdr['FK_PNH_Picknum'],
			'DH_Manual'						=> False,
			'DH_BarcodeID'					=> $dr_no
		);

		$dbase->set('DH_Date', 'GETDATE()', FALSE);
		$dbase->set('DH_DatePosted', 'GETDATE()', FALSE);
		$dbase->insert('tblDeliveryhdr', $save_del_hdr);

		foreach ($carton_dtl as $key => $detail) {
			$save_del_dtl = array(
				'PKFK_CPH_LocCode'			=> $detail['PKFK_CPH_Location_Code'],
				'PKFK_CPH_CartonNo'			=> $detail['PKFK_CPH_CartonNo'],
				'DH_DRNum'					=> 'DR'.$dr_no,
				'DT_Count'					=> $key + 1,
				'FK_InvMaster_Code'			=> $detail['PKFK_InvMaster_Code'],
				'DT_DelQty' 				=> $detail['CPD_PackedQty'] 
			);

			$dbase->insert('tblDeliverydtl', $save_del_dtl);

			$save_inv_trans = array(
				'PKFK_Location_Code' 		=> $detail['PKFK_CPH_Location_Code'],
				'PKFK_Transtype_Code' 		=> 'DEL',
				'IT_TransNo' 				=> 'DR'.$dr_no,
				'IT_TransSeq' 				=> $key + 1,
				'FK_InvMaster_Code' 		=> $detail['PKFK_InvMaster_Code'],
				'IT_Qty' 					=> $detail['CPD_PackedQty'],
				'IT_UnitCost' 				=> $detail['IM_UnitCost'],
				'IT_UnitPrice' 				=> $detail['IM_UnitPrice'],
				'IT_PostedDate' 			=> date('Y-m-d h:i:s'),
				'FK_Users_Postedby' 		=> $this->session->userdata('current_user')['login-user'] 
			);

			$dbase->insert('tblInvTransaction', $save_inv_trans);

			$ungrp_ctn_dtl = $this->getAllCPDetailByItemCode($location, $carton_hdr['FK_PNH_Picknum'], $carton_hdr['CPH_CartonNo'], $detail['PKFK_InvMaster_Code']);

			foreach ($ungrp_ctn_dtl as $key => $ungrp_dtl) {
				$onhold_qty = $this->getOnholdQty($ungrp_dtl['CPD_Location_Source'], $detail['PKFK_InvMaster_Code']);

				$dbase->where('PKFK_Location_Code', $ungrp_dtl['CPD_Location_Source'])
						 ->where('PKFK_InvMaster_Code', $detail['PKFK_InvMaster_Code'])
						 ->set('INV_Onhold', $onhold_qty - $ungrp_dtl['CPD_PackedQty'])
						 ->update('tblInvdetails');
			}

		}

		$dbase->where('CPH_LocCode', $carton_hdr['CPH_LocCode'])
				 ->where('FK_PNH_Picknum', $carton_hdr['FK_PNH_Picknum'])
				 ->where('CPH_CartonNo', $carton_hdr['CPH_CartonNo'])
				 ->set('FK_Status_Code', 'CCP')
				 ->update('tblPROD_CartonPacked');

	}

	public function on_create_cartonHdr($data, $location, $ir_num, $wsid){
		$save_carton_hdr = array(
			'CPH_LocCode' 		=> $location,
			'FK_PNH_Picknum'  	=> $data['picklist-no'],
			'CPH_CartonNo' 		=> $data['carton-no'],
			'FK_RH_LocDestin' 	=> $data['loc-dest'],
			'FK_RH_IRNum' 		=> $ir_num,
			'CPH_PackedQty' 	=> 0,
			'FK_Status_Code' 	=> 'SCN',
			'CPH_Scanning' 		=> 0,
			'CPH_WSID' 			=> $wsid,
			'CreatedBy' 		=> $this->session->userdata('current_user')['login-user'] 
		);


		$this->DB->set('DateCreated', 'GETDATE()', FALSE);
		$this->DB->insert('tblPROD_CartonPacked', $save_carton_hdr);
	}

	public function on_create_cartonDtl($data, $location, $wsid, $dbase){
		
		$save_carton_dtl = array(
			'PKFK_CPH_Location_Code' 	=> $location,
			'PKFK_PNH_PickNum' 			=> $data['picklist-no'],
			'PKFK_CPH_CartonNo' 		=> $data['carton-no'],
			'CPD_SequenceNo' 			=> $this->getCurrentCartonSeqNo($data['picklist-no'], $location, $data['carton-no']) + 1,
			'PKFK_InvMaster_Code' 		=> $data['item-code'],
			'FK_IM_Item_Desc' 			=> $data['item-desc'],
			'FK_IM_UPCCode' 			=> $data['upc-code'],
			'FK_Category_Code' 			=> $data['cat-code'],
			'FK_Color_Code' 			=> $data['color'],
			'FK_Size_Code' 				=> $data['size'],
			'CPD_RequiredQty' 			=> $data['required-qty'],
			'CPD_PackedQty' 			=> $data['qty'],	
			'CPD_Scanning' 				=> $data['scanning'],	
			'CPD_WSID' 					=> $wsid,
			'CPD_Location_Source' 		=> $data['loc-source'],
			'CreatedBy' 				=> $this->session->userdata('current_user')['login-user']
		);

		$dbase->set('DateCreated', 'GETDATE()', FALSE);
		$dbase->insert('tblPROD_CartonPackedDetail', $save_carton_dtl);
	}

	public function on_update_cartonHdr($data, $location, $ir_num, $wsid, $dbase){

		$packed_qty = $this->getCartonPackedQty($data['carton-no'], $data['picklist-no'], $location);

		$update_carton_hdr = array(
			'FK_RH_LocDestin' 	=> $data['loc-dest'],
			'FK_RH_IRNum' 		=> $ir_num,
			'CPH_PackedQty' 	=> ($data['status'] == 'Scanning' ? $data['qty'] + $packed_qty : $packed_qty - $data['qty']),
			'FK_Status_Code' 	=> $data['carton-status'],
			'CPH_Scanning' 		=> $data['scanning'],
			'CPH_WSID' 			=> $wsid,
			'ModifiedBy' 		=> $this->session->userdata('current_user')['login-user']
		);

		$dbase->where('CPH_LocCode', $location)
				 ->where('FK_PNH_PickNum', $data['picklist-no'])
				 ->where('CPH_CartonNo', $data['carton-no'])
				 ->set('DateModified', 'GETDATE()', FALSE)
				 ->update('tblPROD_CartonPacked', $update_carton_hdr);

	}

	public function on_update_cartonDtl($data, $location, $wsid, $dbase){

		$qty = $this->getCartonDetailPackedQty($location, $data['picklist-no'], $data['carton-no'], $data['item-code']);

		$update_carton_dtl = array(
			'PKFK_CPH_Location_Code' 	=> $location,
			'PKFK_PNH_PickNum' 			=> $data['picklist-no'],
			'PKFK_CPH_CartonNo' 		=> $data['carton-no'],
			'PKFK_InvMaster_Code' 		=> $data['item-code'],
			'FK_IM_Item_Desc' 			=> $data['item-desc'],
			'FK_IM_UPCCode' 			=> $data['upc-code'],
			'FK_Category_Code' 			=> $data['cat-code'],
			'FK_Color_Code' 			=> $data['color'],
			'FK_Size_Code' 				=> $data['size'],
			'CPD_RequiredQty' 			=> ($data['status'] == 'Scanning' ? $qty['CPD_RequiredQty'] - $data['qty'] : $qty['CPD_RequiredQty'] + $data['qty']),
			'CPD_PackedQty' 			=> ($data['status'] == 'Scanning' ? $qty['CPD_PackedQty'] + $data['qty'] : $qty['CPD_PackedQty'] - $data['qty']),	
			'CPD_Scanning' 				=> $data['scanning'],	
			'CPD_WSID' 					=> $wsid,
			'ModifiedBy' 				=> $this->session->userdata('current_user')['login-user']	
		);

		$dbase->where('PKFK_CPH_Location_Code', $location)
			 	 ->where('PKFK_PNH_Picknum', $data['picklist-no'])
			 	 ->where('PKFK_CPH_CartonNo', $data['carton-no'])
			 	 ->where('PKFK_InvMaster_Code', $data['item-code'])
			 	 ->where('CPD_Location_Source', $data['loc-source'])
			 	 ->set('DateModified', 'GETDATE()', FALSE)
			 	 ->update('tblPROD_CartonPackedDetail', $update_carton_dtl); 
	}

	private function getCurrentCartonSeqNo($pl, $location, $carton_no){
		$query_string = 'SELECT		TOP (1) CPD_SequenceNo
						FROM		tblPROD_CartonPackedDetail
						WHERE		(PKFK_PNH_PickNum = \''.$pl.'\') AND (PKFK_CPH_Location_Code = \''.$location.'\') AND (PKFK_CPH_CartonNo = \''.$carton_no.'\')
						ORDER BY 	CPD_SequenceNo DESC';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['CPD_SequenceNo'];
		}
	}

	public function getCartonPackedQty($carton_no, $pl, $location){
		$query_string = 'SELECT		CPH_PackedQty
						FROM		tblPROD_CartonPacked
						WHERE		(CPH_CartonNo = \''.$carton_no.'\') AND (FK_PNH_PickNum = \''.$pl.'\') AND (CPH_LocCode = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['CPH_PackedQty'];
		}
	}

	public function getCartonDetailPackedQty($location ,$pl, $carton_no, $item_code){
		$query_string = 'SELECT		CPD_RequiredQty, CPD_PackedQty
						 FROM		tblPROD_CartonPackedDetail
						 WHERE		(PKFK_CPH_CartonNo = \''.$carton_no.'\') AND (PKFK_PNH_PickNum = \''.$pl.'\') AND (PKFK_CPH_Location_Code = \''.$location.'\') AND (PKFK_InvMaster_Code = \''.$item_code.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}

	}

	public function getAllCartonHeaderByCartonNo($location, $pl, $carton_no){
		$query_string = 'SELECT		RTRIM(CPH_LocCode) as CPH_LocCode, RTRIM(FK_PNH_Picknum) as FK_PNH_Picknum, RTRIM(CPH_CartonNo) as CPH_CartonNo, RTRIM(FK_RH_LocDestin) as FK_RH_LocDestin, 
									CPH_PackedQty, RTRIM(FK_Status_Code) as FK_Status_Code
						 FROM		tblPROD_CartonPacked
						 WHERE		CPH_LocCode = \''.$location.'\' AND FK_PNH_Picknum = \''.$pl.'\' AND CPH_CartonNo = \''.$carton_no.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}

	}

	private function getAllCPDetailByItemCode($location, $pl, $carton_no, $item_code){
		$query_string = 'SELECT		CPD_PackedQty, RTRIM(CPD_Location_Source) as CPD_Location_Source
						 FROM		tblPROD_CartonPackedDetail
						 WHERE		PKFK_CPH_Location_Code = \''.$location.'\' AND PKFK_PNH_PickNum = \''.$pl.'\' AND PKFK_CPH_CartonNo = \''.$carton_no.'\' AND 
						 			PKFK_InvMaster_Code = \''.$item_code.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array();
		}
	}

	public function getAllGroupedCartonDetailByCartonNo($location, $pl, $carton_no){
		$query_string = 'SELECT		RTRIM(CPD.PKFK_CPH_Location_Code) as PKFK_CPH_Location_Code, RTRIM(CPD.PKFK_PNH_PickNum) as PKFK_PNH_PickNum, RTRIM(CPD.PKFK_CPH_CartonNo) as PKFK_CPH_CartonNo, 
									RTRIM(CPD.PKFK_InvMaster_Code) as PKFK_InvMaster_Code, SUM(CPD.CPD_PackedQty) as CPD_PackedQty, IM.IM_UnitCost, IM.IM_UnitPrice
						 FROM		tblPROD_CartonPackedDetail as CPD LEFT OUTER JOIN
						 			tblInvMaster as IM ON CPD.PKFK_InvMaster_Code = IM.IM_Item_Code
						 WHERE		CPD.PKFK_CPH_Location_Code = \''.$location.'\' AND CPD.PKFK_PNH_PickNum = \''.$pl.'\' AND CPD.PKFK_CPH_CartonNo = \''.$carton_no.'\'
						 GROUP BY	CPD.PKFK_CPH_Location_Code, CPD.PKFK_PNH_PickNum, CPD.PKFK_CPH_CartonNo, CPD.PKFK_InvMaster_Code, IM.IM_UnitCost, IM.IM_UnitPrice';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array();
		}			 
	}

	public function getCartonHeaderbyPNH($location, $pl){
		$query_string = 'SELECT		RTRIM(CPH_CartonNo) AS CPH_CartonNo, CPH_PackedQty, RTRIM(FK_Status_Code) AS FK_Status_Code
						 FROM 		tblPROD_CartonPacked
						 WHERE		CPH_LocCode = \''.$location.'\' AND FK_PNH_Picknum = \''.$pl.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array();
		}
	}

	public function getCartonDetailsbyCartonNo($location, $pl, $carton_no){
		$query_string = 'SELECT		CPD_PackedQty, RTRIM(PKFK_InvMaster_Code) as PKFK_InvMaster_Code, RTRIM(FK_Category_Code) as FK_Category_Code, 
									RTRIM(FK_Color_Code) AS FK_Color_Code, RTRIM(FK_Size_Code) AS FK_Size_Code, CPD_RequiredQty, RTRIM(FK_IM_UPCCode) AS FK_IM_UPCCode
						 FROM		tblPROD_CartonPackedDetail
						 WHERE		PKFK_CPH_Location_Code = \''.$location.'\' AND PKFK_PNH_PickNum = \''.$pl.'\' AND PKFK_CPH_CartonNo = \''.$carton_no.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array();
		}
	}

	public function checkCartonsByPLIfExisting($pl, $location){
		$query_string = 'SELECT 	RTRIM(FK_Status_Code) as FK_Status_Code
						 FROM 		tblPROD_CartonPacked
						 WHERE 		CPH_LocCode = \''.$location.'\' AND FK_PNH_Picknum = \''.$pl.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->num_rows();
		}
	}

	public function checkAllCartonsIfClosed($pl, $location){
		$query_string = 'SELECT 	RTRIM(FK_Status_Code) as FK_Status_Code
						 FROM 		tblPROD_CartonPacked
						 WHERE 		FK_Status_Code = \'SCN\' AND CPH_LocCode = \''.$location.'\' AND FK_PNH_Picknum = \''.$pl.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->num_rows();
		}

	}

	public function getFilteredCarton($location, $pl, $from, $to){
		$query_string = 'SELECT		RTRIM(CPH_CartonNo) AS CPH_CartonNo, CPH_PackedQty, RTRIM(FK_Status_Code) AS FK_Status_Code
						 FROM 		tblPROD_CartonPacked
						 WHERE		CPH_LocCode = \''.$location.'\' AND FK_PNH_Picknum = \''.$pl.'\' AND CPH_CartonNo BETWEEN '.$from.' AND '.$to;

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array();
		}
	}

	public function getLocDestination($location, $pl){
		$query_string = 'SELECT        	FK_RH_Location_Destin
						 FROM          	tblPROD_PickNote
						 WHERE        	(PNH_Picknum = \''.$pl.'\') AND (PNH_Location_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['FK_RH_Location_Destin'];
		}
	}

	public function totalReqQty($pl,$barcode, $location, $db){
		$query_string = 'SELECT        	SUM(PND_PickQty - ISNULL(PND_PackedQty, 0)) AS RequiredQty
						 FROM           tblPROD_PickNoteDetail
						 WHERE        	(PKFK_PNH_PickNum = \''.$pl.'\') AND (FK_IM_UPCCode = \''.$barcode.'\') AND (PKFK_PNH_LocCode = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $db->query($query_string)->row_array()['RequiredQty'];
		}
		
	}

	public function totalScnQty($pl,$barcode, $location, $db){
		$query_string = 'SELECT        	SUM(ISNULL(PND_PackedQty, 0)) AS ScanQty
						 FROM           tblPROD_PickNoteDetail
						 WHERE        	(PKFK_PNH_PickNum = \''.$pl.'\') AND (FK_IM_UPCCode = \''.$barcode.'\') AND (PKFK_PNH_LocCode = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $db->query($query_string)->row_array()['ScanQty'];
		}
		
	}

	public function getPNDQtybyBarcode($pl,$barcode, $location, $db){
		$query_string = 'SELECT        	(PND_PickQty - ISNULL(PND_PackedQty, 0)) AS RequiredQty, RTRIM(PKFK_Location_Source) as PKFK_Location_Source
						 FROM           tblPROD_PickNoteDetail
						 WHERE        	(PKFK_PNH_PickNum = \''.$pl.'\') AND (FK_IM_UPCCode = \''.$barcode.'\') AND (PKFK_PNH_LocCode = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $db->query($query_string)->result_array();
		}
	}


}