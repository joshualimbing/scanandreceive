<?php 

defined('BASEPATH') or exit('No direct script access allowed');

/**
 Scan On-demand Model created by Joshua Limbing 9/30/2019
 */

class scan_ondemand_model extends MAIN_Model{

	public function getLocType($location){
		$query_string = 'SELECT		RTRIM(Loc_Type) as Loc_Type 
						 FROM 		tblLocation
						 WHERE 		Loc_Code = \''.$location.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['Loc_Type'];
		}
	}

	public function getTotalScanned($doc_no, $dbase){
		$query_string ='SELECT        ISNULL(SUM(ODD_ScanQty), 0) AS Total_ScanQty
						FROM          tblPROD_ScanOndemandDetail
						WHERE 		  ODD_DocNo = \''.$doc_no.'\' ';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->row_array()['Total_ScanQty'];
		}
	}

	public function getAllAvailableDR($location, $dbase){

		$query_string = 'SELECT 	ODH_DocNo
						 FROM 		tblPROD_ScanOndemandHeader
						 WHERE 		ODH_Status_Code = \'SCN\' AND PKFK_Location_Code = \''.$location.'\'';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->result_array();
		}
	}

	public function getPostedDR($location, $filter, $dbase){
		$query_string = 'SELECT 	DH_DRNum
						 FROM 		tblDeliveryhdr
						 WHERE 		FK_Status_Code = \'DEL\' AND PKFK_CPH_LocCode = \''.$location.'\' AND DH_DRNum LIKE \'%'.$filter.'%\'';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->result_array();
		}
	}

	public function getPostedDRDetails($docno, $dbase){
		$query_string = 'SELECT        DRD.FK_InvMaster_Code, IM.IM_Item_Desc, IM.IM_ItemLocation, DRD.DT_DelQty
						 FROM          tblDeliverydtl AS DRD LEFT OUTER JOIN
                         			   tblInvMaster AS IM ON DRD.FK_InvMaster_Code = IM.IM_Item_Code
						 WHERE         (DRD.DH_DRNum = \''.$docno.'\')';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->result_array();
		}
	}

	// Get the latest DR Number

	public function CreateODH_DocNo($location){

		$this->updateDelLastRefDoc($location);

		$query_string = 'SELECT     ISNULL(TT_LastDocRef, 0) AS TT_LastDocRef
						FROM         tblTranstype
						WHERE     	(TT_Code = \'DEL-OD\') AND (PKFK_Location_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			$lastrefdoc = $this->DB->query($query_string)->row_array()['TT_LastDocRef'];
			return $this->padLeftZero(10, $lastrefdoc);
		}
	}

	private function padLeftZero($num, $txt){

        $pcnt = $num - strlen($txt);
        $output = '';

        for ($i=0; $i < $pcnt; $i++) { 
            $output .= '0';
        }

        $output = $output.$txt;

        return $output;

    }

    // Update last DR Number

	private function updateDelLastRefDoc($location){
		$query_string = 'UPDATE tblTranstype
						SET TT_LastDocRef = TT_LastDocRef + 1, TT_DocBarcodeID = TT_DocBarcodeID + 1
						WHERE PKFK_Location_Code = \''.$location.'\' AND TT_Code = \'DEL-OD\'';

		
		if($this->sess->isLogin()){
			$this->DB->query($query_string);
		}
	}

	// End

	public function checkIfClosed($location, $doc_no, $dbase){
		$query_string = 'SELECT 	ODH_DocNo
						 FROM 		tblPROD_ScanOndemandHeader
						 WHERE 		PKFK_Location_Code = \''.$location.'\' AND ODH_DocNo = \''.$doc_no.'\' AND ODH_Status_Code = \'CCP\'';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->row_array();
		}

	}

	public function checkIfExisting($location, $doc_no, $dbase){
		$query_string = 'SELECT 	ODH_DocNo
						 FROM 		tblPROD_ScanOndemandHeader
						 WHERE 		PKFK_Location_Code = \''.$location.'\' AND ODH_DocNo = \''.$doc_no.'\'';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->row_array();
		}

	}

	public function getDeliverDetails($doc_no, $dbase){
		$query_string = 'SELECT 		RTRIM(ODD.PKFK_InvMaster_Code) as PKFK_InvMaster_Code, RTRIM(IM.IM_Item_Desc) as IM_Item_Desc, RTRIM(IM.IM_UPCCode) as IM_UPCCode, ISNULL(ODD.ODD_ScanQty, 0) as ODD_ScanQty, 
										RTRIM(IM.IM_ItemLocation) as IM_ItemLocation, IM_UnitCost, RTRIM(ODD.ODD_Location_Source) as ODD_Location_Source, IM_UnitPrice
						 FROM 			tblPROD_ScanOndemandDetail as ODD LEFT OUTER JOIN
						 				tblInvMaster as IM ON IM.IM_Item_Code = ODD.PKFK_InvMaster_Code
						 WHERE 			ODD_DocNo = \''.$doc_no.'\'';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->result_array();
		}

	}

	public function getLocationList($location){
		$query_string = 'SELECT     (RTRIM(Loc_Code) + \' - \' + RTRIM(Loc_Desc)) as Description, RTRIM(Loc_Code) as Loc_Code
						 FROM       tblLocation
						 WHERE      (LM_Level = 1) AND (Loc_Active = 1) AND (Loc_Code <> \''.$location.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function checkBarcodeifExisting($barcode, $dbase){
		$query_string = 'SELECT 	RTRIM(IM_UPCCode) as IM_UPCCode
						 FROM 		tblInvMaster
						 WHERE 		IM_UPCCode = \''.$barcode.'\'';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->row_array();
		}
	}

	public function getAllBinLocation(){
		$query_string = 'SELECT 	RTRIM(Loc_Code) AS Loc_Code
						 FROM 		tblLocation
						 WHERE 		LM_Level = 4 and [Loc_Type] = \'WSE\' and Loc_Active = 1
						 ORDER BY 	Loc_Code ASC';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	private function getLocMask($location){
		$query_string = 'SELECT        Loc_Code, LM_Level, FK_Company_Code, LEFT(Loc_Mask, 3) AS Loc_Mask
						 FROM          tblLocation
						 WHERE         (Loc_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['Loc_Mask'];
		}
	}

	public function countLocationByBarcode($location, $barcode, $item_code){

		$loc_mask = $this->getLocMask($location);

		$query_string = 'SELECT        id.PKFK_Location_Code, id.PKFK_InvMaster_Code, id.INV_Onhand, id.INV_Onhold, ISNULL(id.INV_Active, 0) AS Expr1, id.INV_Updatedby, 
									   id.INV_DateUpdated, l.Loc_Code, l.Loc_Receive, LEFT(l.Loc_Mask, 3) AS Loc_Mask
						 FROM          tblInvdetails AS id LEFT OUTER JOIN
						               tblLocation AS l ON id.PKFK_Location_Code = l.Loc_Code
						 WHERE         (ISNULL(id.INV_Active, 0) = 1) AND (LEFT(l.Loc_Mask, 3) = '.$loc_mask.') AND (id.PKFK_InvMaster_Code = \''.$item_code.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->num_rows();
		}
	}

	public function checkInvDetail($loc_source, $item_code, $dbase){
		$query_string = 'SELECT       	 PKFK_Location_Code, PKFK_InvMaster_Code
						 FROM            tblInvdetails
						 WHERE        	(PKFK_InvMaster_Code = \''.trim($item_code).'\') AND (PKFK_Location_Code = \''.trim($loc_source).'\')';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->result_array(); 
		}
	}

	public function getItemCodeByBarcode($barcode){
		$query_string = 'SELECT 	RTRIM(IM_Item_Code) as IM_Item_Code
						 FROM 		tblInvMaster
						 WHERE 		IM_UPCCode = \''.$barcode.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['IM_Item_Code'];
		}
	}

	public function getItemDescByBarcode($barcode){
		$query_string = 'SELECT 	RTRIM(IM_Item_Desc) as IM_Item_Desc
						 FROM 		tblInvMaster
						 WHERE 		IM_UPCCode = \''.$barcode.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['IM_Item_Desc'];
		}
	}

	public function getDefaultLocSource($item_code){
		$query_string =	'SELECT		IM_ItemLocation
						 FROM 		tblInvMaster
						 WHERE 		IM_Item_Code = \''.$item_code.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['IM_ItemLocation'];
		}
	}

	public function updateLocSource($data, $dbase){


		$dbase->where('ODD_DocNo', $data['doc-no'])
			  ->where('PKFK_InvMaster_Code', $data['item-code'])
			  ->set('ODD_Location_Source', $data['bin-loc'])
			  ->update('tblPROD_ScanOndemandDetail');

	}

	public function checkifODDItemExists($doc_no, $item_code, $dbase){
		$query_string = 'SELECT 	RTRIM(PKFK_InvMaster_Code) as PKFK_InvMaster_Code
						 FROM 		tblPROD_ScanOndemandDetail
						 WHERE		ODD_DocNo = \''.$doc_no.'\' AND PKFK_InvMaster_Code = \''.$item_code.'\'';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->row_array();
		}
	}

	public function on_save($del_dtl, $dbase){

		$dbase->set('DateCreated', date('Y-m-d'));
		$dbase->set('CreatedBy', $_SESSION['current_user']['login-user']);
		$dbase->insert('tblPROD_ScanOndemandDetail', $del_dtl);


	}

	public function on_create_DRODHeader($location, $doc_no, $del_location, $remarks, $dbase){

		$save_header = array(
			'ODH_DocNo'  			=> $doc_no,
			'PKFK_Location_Code'  	=> $location,
			'PKFK_Location_Destin'  => $del_location,
			'ODH_Remarks'  			=> $remarks,
			'ODH_DocDate'  			=> date('Y-m-d'),
			'ODH_Status_Code'  		=> 'SCN',
			'ODH_TotalQty'  		=> 0,
			'CreatedBy'   			=> $_SESSION['current_user']['login-user'],
			'DateCreated'  			=> date('Y-m-d') 	
		);

		$dbase->insert('tblPROD_ScanOndemandHeader', $save_header);

	}

	public function on_update($item_code, $doc_no, $scan_qty, $dbase){

		$current_scan_qty = $this->getCurrentScanQty($item_code, $doc_no, $dbase);
		

		if(($current_scan_qty + $scan_qty <= 0)){

			$dbase->where('ODD_DocNo', $doc_no)
				  ->where('PKFK_InvMaster_Code', $item_code)
				  ->delete('tblPROD_ScanOndemandDetail');

		}
		else{
			$dbase->where('ODD_DocNo', $doc_no)
				  ->where('PKFK_InvMaster_Code', $item_code)
				  ->set('ODD_ScanQty', 'ODD_ScanQty + '.$scan_qty, false)
				  ->set('ModifiedBy', $_SESSION['current_user']['login-user'])
				  ->set('DateModified', date('Y-m-d'))
				  ->update('tblPROD_ScanOndemandDetail');
		}

	}

	public function on_post_deliver($location, $header, $remarks, $details, $dbase){

		$save_del_hdr = array(
			'PKFK_CPH_LocCode' 				=> $location,
			'PKFK_CPH_CartonNo' 			=> 1,
			'DH_DRNum' 						=> $header['doc-no'],
			'FK_Location_Destin' 			=> $header['del-location'],
			'FK_Status_Code' 				=> 'DEL',
			'DH_PreparedBy'					=> $this->session->userdata('current_user')['login-user'],
			'DH_Remarks'					=> '',	
			'DH_TotalDelQty'				=> $header['total'],
			'FK_Users_Postedby'				=> $this->session->userdata('current_user')['login-user'],
			'FK_PickNoteHdr_PickNum'		=> '',
			'DH_Manual'						=> False,
			'DH_BarcodeID'					=> str_replace('DROD-0', '1', $header['doc-no'])
		);

		$dbase->set('DH_Date', 'GETDATE()', FALSE);
		$dbase->set('DH_DatePosted', 'GETDATE()', FALSE);
		$dbase->insert('tblDeliveryhdr', $save_del_hdr);

		foreach ($details as $key => $detail) {
			$save_del_dtl = array(
				'PKFK_CPH_LocCode'			=> $location,
				'PKFK_CPH_CartonNo'			=> 1,
				'DH_DRNum'					=> $header['doc-no'],
				'DT_Count'					=> $key + 1,
				'FK_InvMaster_Code'			=> $detail['PKFK_InvMaster_Code'],
				'DT_DelQty' 				=> $detail['ODD_ScanQty'] 
			);

			$dbase->insert('tblDeliverydtl', $save_del_dtl);

			$save_inv_trans = array(
				'PKFK_Location_Code' 		=> $location,
				'PKFK_Transtype_Code' 		=> 'DEL',
				'IT_TransNo' 				=> $header['doc-no'],
				'IT_TransSeq' 				=> $key + 1,
				'FK_InvMaster_Code' 		=> $detail['PKFK_InvMaster_Code'],
				'IT_Qty' 					=> $detail['ODD_ScanQty'],
				'IT_UnitCost' 				=> $detail['IM_UnitCost'],
				'IT_UnitPrice' 				=> $detail['IM_UnitPrice'],
				'FK_Users_Postedby' 		=> $this->session->userdata('current_user')['login-user'] 
			);

			$dbase->set('IT_PostedDate', 'GETDATE()', FALSE);
			$dbase->insert('tblInvTransaction', $save_inv_trans);

			$checkInvDtl = $this->scan_ondemand_model->checkInvDetail($detail['ODD_Location_Source'], $detail['PKFK_InvMaster_Code'], $dbase);

			$hasInvDtl = true;

			if(empty($checkInvDtl)){
				$hasInvDtl = false;
			}

			if(!$hasInvDtl){
					$save_inv_dtl = array(
						'PKFK_Location_Code' 		=> $detail['ODD_Location_Source'],
						'PKFK_InvMaster_Code' 		=> $detail['PKFK_InvMaster_Code'],
						'INV_Onhand' 				=> $detail['ODD_ScanQty'] * -1,
						'INV_Onhold' 				=> 0,
						'INV_Active' 				=> 1,
						'INV_Updatedby' 			=> $this->session->userdata('current_user')['login-user']
					);

				$dbase->set('INV_DateUpdated', 'GETDATE()', false);
				$dbase->insert('tblInvdetails', $save_inv_dtl);
			}
			else{
				$dbase->where('PKFK_Location_Code', $detail['ODD_Location_Source'])
					  ->where('PKFK_InvMaster_Code', $detail['PKFK_InvMaster_Code'])
					  ->set('INV_Onhand', 'INV_Onhand - '.$detail['ODD_ScanQty'], false)
					  ->update('tblInvdetails');
			}

		}

	}

	public function on_reprocess_DR($location, $doc_no, $details, $dbase){

		foreach ($details as $key => $detail) {

			$save_inv_trans = array(
				'PKFK_Location_Code' 		=> $location,
				'PKFK_Transtype_Code' 		=> 'DEL',
				'IT_TransNo' 				=> $doc_no,
				'IT_TransSeq' 				=> $key + 1,
				'FK_InvMaster_Code' 		=> $detail['FK_InvMaster_Code'],
				'IT_Qty' 					=> $detail['DT_DelQty'],
				'IT_UnitCost' 				=> $detail['IM_UnitCost'],
				'IT_UnitPrice' 				=> $detail['IM_UnitPrice'],
				'IT_PostedDate' 			=> $detail['DH_DatePosted'],
				'FK_Users_Postedby' 		=> $detail['FK_Users_Postedby'] 
			);

			$dbase->insert('tblInvTransaction', $save_inv_trans);

			$checkInvDtl = $this->scan_ondemand_model->checkInvDetail($detail['IM_ItemLocation'], $detail['FK_InvMaster_Code'], $dbase);

			$hasInvDtl = true;

			if(empty($checkInvDtl)){
				$hasInvDtl = false;
			}

			if(!$hasInvDtl){
					$save_inv_dtl = array(
						'PKFK_Location_Code' 		=> $detail['IM_ItemLocation'],
						'PKFK_InvMaster_Code' 		=> $detail['FK_InvMaster_Code'],
						'INV_Onhand' 				=> $detail['DT_DelQty'] * -1,
						'INV_Onhold' 				=> 0,
						'INV_Active' 				=> 1,
						'INV_Updatedby' 			=> $detail['FK_Users_Postedby']
					);

				$dbase->set('INV_DateUpdated', 'GETDATE()', false);
				$dbase->insert('tblInvdetails', $save_inv_dtl);
			}
			else{
				$dbase->where('PKFK_Location_Code', $detail['IM_ItemLocation'])
					  ->where('PKFK_InvMaster_Code', $detail['FK_InvMaster_Code'])
					  ->set('INV_Onhand', 'INV_Onhand - '.$detail['DT_DelQty'], false)
					  ->update('tblInvdetails');
			}

		}
	}

	public function getCurrentScanQty($item_code, $doc_no, $dbase){

		$query_string = 'SELECT 	ISNULL(ODD_ScanQty, 0) as ODD_ScanQty
						 FROM 		tblPROD_ScanOndemandDetail
						 WHERE 		PKFK_InvMaster_Code = \''.$item_code.'\' AND ODD_DocNo = \''.$doc_no.'\'';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->row_array()['ODD_ScanQty'];
		}

	}

	public function getRemarks($doc_no, $dbase){
		$query_string = 'SELECT 		ODH_Remarks
						 FROM 			tblPROD_ScanOndemandHeader
						 WHERE 			ODH_DocNo = \''.$doc_no.'\'';

		return $dbase->query($query_string)->row_array()['ODH_Remarks'];
	}

	public function checkIfPosted($location, $doc_no, $dbase){
		$query_string = 'SELECT 	DISTINCT IT_TransNo, PKFK_Location_Code
						 FROM       tblInvTransaction
						 WHERE      (PKFK_Location_Code = \''.$location.'\') AND (IT_TransNo = \''.$doc_no.'\')';

		return $dbase->query($query_string)->num_rows();
	}

	public function getReprocessDetails($location, $doc_no, $dbase){
		$query_string = 'SELECT        DRD.FK_InvMaster_Code, IM.IM_Item_Desc, IM.IM_ItemLocation, DRD.DT_DelQty, DRD.DT_Count, IM.IM_UnitCost, 
									   IM.IM_UnitPrice, DRH.FK_Users_Postedby, DRH.DH_DatePosted
						 FROM          tblDeliverydtl AS DRD LEFT OUTER JOIN
						               tblDeliveryhdr AS DRH ON DRD.PKFK_CPH_LocCode = DRH.PKFK_CPH_LocCode AND 
						               				  DRD.PKFK_CPH_CartonNo = DRH.PKFK_CPH_CartonNo AND DRD.DH_DRNum = DRH.DH_DRNum LEFT OUTER JOIN
						               tblInvMaster AS IM ON DRD.FK_InvMaster_Code = IM.IM_Item_Code
						 WHERE         (DRD.DH_DRNum = \''.$doc_no.'\') AND (DRD.PKFK_CPH_LocCode = \''.$location.'\')
						 ORDER BY      DRD.DT_Count ASC';

		return $dbase->query($query_string)->result_array();
	}

}