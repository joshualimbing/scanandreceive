<?php 
defined('BASEPATH') or exit('No direct script access allowed');

/**
 User Model created by Joshua Limbing 9/23/2019
 */
class user_model extends CI_Model
{
	
	public function getAllCompany(){
		$query_string = 'SELECT     LTRIM(C_PK_ID) as C_PK_ID, LTRIM(C_Name) as C_Name, C_Default_BOServer, C_Default_BODB
						FROM         tblCompany
						WHERE     (ISNULL(C_Active, 0) = 1)';

		return $this->db->query($query_string)->result_array();
	}

	public function getLocMask($db, $location){

		$dynamic_db = $this->load->database($db, true);

		$query_string = 'SELECT        Loc_Code, LM_Level, FK_Company_Code, LEFT(Loc_Mask, 3) AS Loc_Mask
						 FROM          tblLocation
						 WHERE         (Loc_Code = \''.$location.'\')';

		return $dynamic_db->query($query_string)->row_array()['Loc_Mask'];
	}

	public function getDBDetailsByCompanyId($company_id){
		$query_string = 'SELECT     C_Default_BOPassword, C_Default_BOUser, C_Default_BOServer, C_Default_BODB
						FROM         tblCompany
						WHERE 		C_PK_ID = \''.$company_id.'\'';

		return $this->db->query($query_string)->row_array();
	}

	public function getLocationbyCompany($db, $company_id){

		$dynamic_db = $this->load->database($db, true);

		$query_string = 'SELECT     LTRIM(Loc_Code) as Loc_Code, LTRIM(Loc_Desc) as Loc_Desc
						FROM         tblLocation
						WHERE     (LM_Level = 1) AND (ISNULL(Loc_Active, 0) = 1) AND FK_Company_Code = \''.$company_id.'\' AND (Loc_Type = \'WSE\')';



		return $dynamic_db->query($query_string)->result_array();

	}

	public function checkUsernameAndPassword($db, $username, $password){

		$dynamic_db = $this->load->database($db, true);

		$query_string = 'SELECT U_UserID, U_Name, U_Password 
						FROM tblUsers 
						WHERE U_UserID = \''.$username.'\' AND U_Password = \''.$password.'\'';

		return $dynamic_db->query($query_string)->row_array();
	}

	public function checkDisabled($db, $username, $password){
		$dynamic_db = $this->load->database($db, true);

		$query_string = 'SELECT U_Disabled 
						FROM tblUsers 
						WHERE U_UserID = \''.$username.'\' AND U_Password = \''.$password.'\'';

		return $dynamic_db->query($query_string)->row_array()['U_Disabled'];
	}
}