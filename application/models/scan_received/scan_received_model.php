<?php 

defined('BASEPATH') or exit('No direct script access allowed');

/**
 Scanpack Model created by Joshua Limbing 9/30/2019
 */

class scan_received_model extends MAIN_Model{

	public function getLocType($location){
		$query_string = 'SELECT		RTRIM(Loc_Type) as Loc_Type 
						 FROM 		tblLocation
						 WHERE 		Loc_Code = \''.$location.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['Loc_Type'];
		}
	}

	public function getTotalReceived($location, $pack_no, $ref_no){
		$query_string ='SELECT        ISNULL(SUM(SI_ScanQty), 0) AS Total_ScanQty
						FROM          tblPROD_ScanReceive
						WHERE 		  PKFK_Location_Code = \''.$location.'\' AND Pack_No = '.$pack_no.' AND Doc_Reference = \''.$ref_no.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['Total_ScanQty'];
		}
	}

	public function getTransType($location, $loc_type){

		$where_clause = '';

		if($loc_type == 'WSE'){
			$where_clause .= ' WHERE TT_Code IN (\'POE\',\'PUL\',\'BEG\') AND PKFK_Location_Code = \''.$location.'\'';	
		}
		else{
			$where_clause .= ' WHERE TT_Code IN (\'STF\',\'DEL\',\'BEG\') AND PKFK_Location_Code = \''.$location.'\'';
		}

		$query_string = 'SELECT RTRIM(TT_Code) as TT_Code, RTRIM(TT_Desc) as TT_Desc 
						 FROM tblTranstype'.$where_clause;

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function getReceivingDetails($location, $recv_type, $ref_no, $pack_no){
		$query_string = 'SELECT 	RTRIM(SR.SI_UPCCode) as SI_UPCCode, SR.SI_ScanQty, RTRIM(SR.PKFK_InvMaster_Code) as PKFK_InvMaster_Code, RTRIM(SR.Location_Source) as Location_Source,
									IM.IM_ItemLocation
						 FROM 		tblPROD_ScanReceive as SR LEFT OUTER JOIN
						 			tblInvMaster as IM ON IM.IM_Item_Code = SR.PKFK_InvMaster_Code
						 WHERE 		PKFK_Location_Code = \''.$location.'\' AND Doc_ReceivingType = \''.$recv_type.'\' AND Doc_Reference = \''.$ref_no.'\' AND Pack_No = \''.$pack_no.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function checkifSRBarcodeExists($location, $barcode, $pack_no, $ref_no){
		$query_string = 'SELECT 	RTRIM(SI_UPCCode) as SI_UPCCode
						 FROM 		tblPROD_ScanReceive
						 WHERE		PKFK_Location_Code = \''.$location.'\' AND SI_UPCCode = \''.$barcode.'\' AND Pack_No = \''.$pack_no.'\' AND Doc_Reference = \''.$ref_no.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}

	public function getLocationList(){
		$query_string = 'SELECT     LTRIM(RTRIM(Loc_Code)) as Loc_Code
						 FROM       tblLocation
						 WHERE      (LM_Level = 1) AND (Loc_Active = 1)';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function LocCodeSearchResults($type, $filter, $recv_type){
		$location = $this->session->userdata('current_user')['login-location'];

		if($recv_type != 'POE'){
			$query_string = 'SELECT     (RTRIM(Loc_Code) + \' - \' + RTRIM(Loc_Desc)) as Description, RTRIM(Loc_Code) as Loc_Code
							 FROM       tblLocation
							 WHERE      (LM_Level = 1) AND (Loc_Active = 1) AND Loc_Code <> \''.$location.'\' AND ('.$type.' LIKE \'%'.$filter.'%\' OR Loc_Desc LIKE \'%'.$filter.'%\')';			
		}
		else{
			$type = 'Sup_Code';

			$query_string = 'SELECT     (RTRIM(Sup_Code) + \' - \' + RTRIM(Sup_Desc)) as Description, RTRIM(Sup_Code) as Loc_Code
							 FROM       tblSupplier
							 WHERE      ('.$type.' LIKE \'%'.$filter.'%\' OR Sup_Desc LIKE \'%'.$filter.'%\')';
		}

		// $this->DB->query($query_string)->result_array();

		// $this->print_r($this->DB->last_query());
		// die();

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}

	}

	public function checkBarcodeifExisting($barcode){
		$query_string = 'SELECT 	RTRIM(IM_UPCCode) as IM_UPCCode
						 FROM 		tblInvMaster
						 WHERE 		IM_UPCCode = \''.$barcode.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}

	public function getAllBinLocation(){
		$query_string = 'SELECT 	RTRIM(Loc_Code) AS Loc_Code
						 FROM 		tblLocation
						 WHERE 		LM_Level = 4 and [Loc_Type] = \'WSE\' and Loc_Active = 1
						 ORDER BY 	Loc_Code ASC';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	private function getLocMask($location){
		$query_string = 'SELECT        Loc_Code, LM_Level, FK_Company_Code, LEFT(Loc_Mask, 3) AS Loc_Mask
						 FROM          tblLocation
						 WHERE         (Loc_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['Loc_Mask'];
		}
	}

	public function countLocationByBarcode($location, $barcode, $item_code){

		$loc_mask = $this->getLocMask($location);

		$query_string = 'SELECT        id.PKFK_Location_Code, id.PKFK_InvMaster_Code, id.INV_Onhand, id.INV_Onhold, ISNULL(id.INV_Active, 0) AS Expr1, id.INV_Updatedby, 
									   id.INV_DateUpdated, l.Loc_Code, l.Loc_Receive, LEFT(l.Loc_Mask, 3) AS Loc_Mask
						 FROM          tblInvdetails AS id LEFT OUTER JOIN
						               tblLocation AS l ON id.PKFK_Location_Code = l.Loc_Code
						 WHERE         (ISNULL(id.INV_Active, 0) = 1) AND (LEFT(l.Loc_Mask, 3) = '.$loc_mask.') AND (id.PKFK_InvMaster_Code = \''.$item_code.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->num_rows();
		}
	}

	private function checkInvDetail($loc_source, $item_code){
		$query_string = 'SELECT       	 PKFK_Location_Code, PKFK_InvMaster_Code, INV_Onhand, INV_Onhold
						 FROM            tblInvdetails
						 WHERE        	(INV_Active = 1) AND (PKFK_InvMaster_Code = \''.trim($item_code).'\') AND (PKFK_Location_Code = \''.trim($loc_source).'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function getItemCodeByBarcode($barcode){
		$query_string = 'SELECT 	RTRIM(IM_Item_Code) as IM_Item_Code
						 FROM 		tblInvMaster
						 WHERE 		IM_UPCCode = \''.$barcode.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['IM_Item_Code'];
		}
	}

	public function getDefaultLocSource($item_code){
		$query_string =	'SELECT		IM_ItemLocation
						 FROM 		tblInvMaster
						 WHERE 		IM_Item_Code = \''.$item_code.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['IM_ItemLocation'];
		}
	}

	public function updateLocSource($data, $location){


		$this->DB->where('PKFK_Location_Code', trim($location))
				 ->where('Doc_ReceivedFrom', $data['recv-from'])
				 ->where('Doc_Reference', $data['ref-doc'])
				 ->where('PKFK_InvMaster_Code', $data['item-code'])
				 ->where('Doc_ReceivingType', $data['recv-type'])
				 ->set('Location_Source', $data['bin-loc'])
				 ->update('tblPROD_ScanReceive');

	}

	public function on_save($recv_dtl, $dbase){

		$dbase->set('TransDate', 'GETDATE()', FALSE);
		$dbase->insert('tblPROD_ScanReceive', $recv_dtl);

	}

	public function on_update($location, $item_code, $ref_no, $recv_type, $recv_from, $pack_no, $scan_qty, $dbase){

		$current_scan_qty = $this->getCurrentScanQty($location, $item_code, $ref_no, $recv_type, $recv_from, $pack_no, $scan_qty);
		$item = $this->checkIfItemExists($location, $item_code, $ref_no, $pack_no);

		if(!empty($item)){

			if(($current_scan_qty + $scan_qty <= 0)){

				$dbase->where('PKFK_Location_Code', $location)
					 ->where('PKFK_InvMaster_Code', $item_code)
					 ->where('Doc_Reference', $ref_no)
					 ->where('Doc_ReceivingType', $recv_type)
					 ->where('Doc_ReceivedFrom', $recv_from)
					 ->where('Pack_No', $pack_no)
					 ->delete('tblPROD_ScanReceive');

			}
			else{
				$dbase->where('PKFK_Location_Code', $location)
						 ->where('PKFK_InvMaster_Code', $item_code)
						 ->where('Doc_Reference', $ref_no)
						 ->where('Doc_ReceivingType', $recv_type)
						 ->where('Doc_ReceivedFrom', $recv_from)
						 ->set('SI_ScanQty', ($current_scan_qty + $scan_qty) )
						 ->update('tblPROD_ScanReceive');
			}

		}
	}

	// Get the latest RR Number

	private function getCurrentRRNo($location){

		$this->updateRCVLastRefDoc($location);

		$query_string = 'SELECT     ISNULL(TT_LastDocRef, 0) AS TT_LastDocRef
						FROM         tblTranstype
						WHERE     	(TT_Code = \'RCV\') AND (PKFK_Location_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			$lastrefdoc = $this->DB->query($query_string)->row_array()['TT_LastDocRef'];
			return $this->padLeftZero(8, $lastrefdoc);
		}
	}

	private function padLeftZero($num, $txt){

        $pcnt = $num - strlen($txt);
        $output = '';

        for ($i=0; $i < $pcnt; $i++) { 
            $output .= '0';
        }

        $output = $output.$txt;

        return $output;

    }

    // Update last RR Number

	private function updateRCVLastRefDoc($location){
		$query_string = 'UPDATE tblTranstype
						SET TT_LastDocRef = TT_LastDocRef + 1
						WHERE PKFK_Location_Code = \''.$location.'\' AND TT_Code = \'RCV\'';

		
		if($this->sess->isLogin()){
			$this->DB->query($query_string);
		}
	}

	// End

	public function on_post_received($location, $header, $details, $dbase){

		$status = 'RCD';

		foreach ($details as $key => $value) {
			if($value['Location_Source'] == null){
				$status = 'NEW';
				break;
			}
		}
		
		$rr_no = $this->getCurrentRRNo($location);

		$save_rcv_hdr = array(
			'PKFK_Location_Code'  	=> $location,
			'RCVH_RRNum' 			=> 'RR'.$rr_no,
			'FK_Transtype_Code' 	=> $header['recv-type'],
			'RCVH_Preparedby' 		=> $this->session->userdata('current_user')['login-user'],
			'RCVH_Remarks' 			=> $header['remarks'], 	
			'RCVH_DocRef' 			=> $header['ref-doc'],
			'RCVH_From' 			=> $header['recv-from'],
			'FK_Status_Code' 		=> $status,
			'FK_Users_Postedby' 	=> $this->session->userdata('current_user')['login-user'],
			'RCVH_TotalRcvQty' 		=> $header['total'] 
		);


		$dbase->set('RCVH_Date', 'GETDATE()', false);
		$dbase->set('RCVH_Postdate', 'GETDATE()', false);
		$dbase->insert('tblReceivehdr', $save_rcv_hdr);

		foreach ($details as $key => $detail) {
			$save_rcv_dtl = array(
				'PKFK_Location_Code' 	=> $detail['PKFK_Location_Code'],
				'PKFK_RCVH_RRNum' 		=> 'RR'.$rr_no,
				'RCVD_Count' 			=> $key + 1,
				'FK_Location_Actual' 	=> $detail['Location_Source'],
				'FK_InvMaster_Code' 	=> $detail['PKFK_InvMaster_Code'],
				'RCVD_RcvQty' 			=> $detail['SI_ScanQty'],
				'RCVD_RefDoc' 			=> $detail['Doc_Reference'],
				'RCVD_Damaged' 			=> false,
				'RCVD_Remarks' 			=> $detail['Doc_Remarks']
			);

			$dbase->insert('tblReceivedet', $save_rcv_dtl);

			if($status == 'RCD'){
				$save_inv_trans = array(
					'PKFK_Location_Code' 		=> $detail['PKFK_Location_Code'],
					'PKFK_Transtype_Code' 		=> 'RCV',
					'IT_TransNo' 				=> 'RR'.$rr_no,
					'IT_TransSeq' 				=> $key + 1,
					'FK_InvMaster_Code' 		=> $detail['PKFK_InvMaster_Code'],
					'IT_Qty' 					=> $detail['SI_ScanQty'],
					'IT_UnitCost' 				=> $detail['IM_UnitCost'],
					'IT_UnitPrice' 				=> $detail['IM_UnitPrice'],
					'FK_Users_Postedby' 		=> $this->session->userdata('current_user')['login-user'] 
				);

				$dbase->set('IT_Posteddate', 'GETDATE()', false);
				$dbase->insert('tblInvTransaction', $save_inv_trans);

				$checkInvDtl = $this->checkInvDetail($detail['Location_Source'], $detail['PKFK_InvMaster_Code']);

				if(empty($checkInvDtl)){
					$save_inv_dtl = array(
						'PKFK_Location_Code' 		=> $detail['Location_Source'],
						'PKFK_InvMaster_Code' 		=> $detail['PKFK_InvMaster_Code'],
						'INV_Onhand' 				=> $detail['SI_ScanQty'],
						'INV_Onhold' 				=> 0,
						'INV_Active' 				=> 1,
						'INV_Updatedby' 			=> $this->session->userdata('current_user')['login-user']
					);

					$dbase->set('INV_DateUpdated', 'GETDATE()', false);
					$dbase->insert('tblInvdetails', $save_inv_dtl);
				}
				else{
					$dbase->where('PKFK_Location_Code', $detail['Location_Source'])
							 ->where('PKFK_InvMaster_Code', $detail['PKFK_InvMaster_Code'])
							 ->set('INV_Onhand', 'INV_Onhand + '.$detail['SI_ScanQty'], false)
							 ->update('tblInvdetails');
				}

			}
		}


		$dbase->where('PKFK_Location_Code', $location)
				 ->where('Doc_ReceivedFrom', $header['recv-from'])
				 ->where('Doc_ReceivingType', $header['recv-type'])
				 ->where('Pack_No', $header['pack-no'])
				 ->delete('tblPROD_ScanReceive');

	}

	public function getCurrentScanQty($location, $item_code, $ref_no, $recv_type, $recv_from, $pack_no, $scan_qty = 0){

		$query_string = 'SELECT 	SI_ScanQty
						 FROM 		tblPROD_ScanReceive
						 WHERE 		PKFK_Location_Code = \''.$location.'\' AND PKFK_InvMaster_Code = \''.$item_code.'\' AND Doc_Reference = \''.$ref_no.'\'
						 			AND Doc_ReceivingType = \''.$recv_type.'\' AND Doc_ReceivingType = \''.$recv_type.'\' AND Pack_No = '.$pack_no;

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['SI_ScanQty'];
		}

	}

	private function checkIfItemExists($location, $item_code, $ref_no, $pack_no){

		$query_string = 'SELECT 	RTRIM(PKFK_InvMaster_Code) as PKFK_InvMaster_Code
						 FROM 		tblPROD_ScanReceive
						 WHERE		PKFK_Location_Code = \''.$location.'\' AND PKFK_InvMaster_Code = \''.$item_code.'\' AND Doc_Reference = \''.$ref_no.'\' AND Pack_No = '.$pack_no;

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}

	}

	public function getAllReceivingDetails($location, $pack_no, $ref_no){
		$query_string = 'SELECT        	IM.IM_UnitCost, IM.IM_UnitPrice, SR.*
						 FROM          	tblPROD_ScanReceive AS SR LEFT OUTER JOIN
                      				   	tblInvMaster AS IM ON SR.PKFK_InvMaster_Code = IM.IM_Item_Code
						 WHERE  	   	SR.PKFK_Location_Code = \''.$location.'\' AND SR.Doc_Reference = \''.$ref_no.'\' AND SR.Pack_No = '.$pack_no;

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

}