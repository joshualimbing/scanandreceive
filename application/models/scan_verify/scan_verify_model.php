<?php 

defined('BASEPATH') or exit('No direct script access allowed');

/** 
	Scan Verify Model - Created by Joshua Limbing (10/2/2019)
*/

class scan_verify_model extends MAIN_Model
{
	
	public function getTransType($location, $loc_type){

		$where_clause = '';

		if($loc_type == 'WSE'){
			$where_clause .= ' WHERE TT_Code IN (\'PUL\', \'POR\') AND PKFK_Location_Code = \''.$location.'\'';	
		}
		else{
			$where_clause .= ' WHERE TT_Code IN (\'STF\',\'DEL\') AND PKFK_Location_Code = \''.$location.'\'';
		}

		$query_string = 'SELECT RTRIM(TT_Code) as TT_Code, RTRIM(TT_Desc) as TT_Desc 
						 FROM tblTranstype'.$where_clause;

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function getTotalRequiredQty($location, $doc_type, $recv_from, $ref_no){

		$query_string = 'SELECT 	SUM(RequiredQty) as RequiredQty
						 FROM 		tblPROD_ScanVerify
						 WHERE 		PKFK_Location_Code = \''.$location.'\' AND Doc_ReceivingType = \''.$doc_type.'\' AND ReceivedFrom = \''.$recv_from.'\' 
						 			AND ReferenceNo = \''.$ref_no.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['RequiredQty']; 
		}

	}

	public function getTotalScannedQty($location, $doc_type, $recv_from, $ref_no){

		$query_string = 'SELECT 	SUM(ScanQty) as ScanQty
						 FROM 		tblPROD_ScanVerify
						 WHERE 		PKFK_Location_Code = \''.$location.'\' AND Doc_ReceivingType = \''.$doc_type.'\' AND ReceivedFrom = \''.$recv_from.'\' 
						 			AND ReferenceNo = \''.$ref_no.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['ScanQty']; 
		}

	}

	public function getAllDetails($location, $doc_type, $recv_from, $ref_no, $dbase){

		$query_string = 'SELECT 	RTRIM(SV.UPCCode) as UPCCode, RTRIM(SV.PKFK_InvMaster_Code) as PKFK_InvMaster_Code, SV.RequiredQty, SV.ScanQty, 
									RTRIM(SV.Location_Source) as Location_Source, RTRIM(IM.IM_ItemLocation) as IM_ItemLocation
						 FROM 		tblPROD_ScanVerify as SV LEFT OUTER JOIN
						 			tblInvMaster as IM ON IM.IM_Item_Code = SV.PKFK_InvMaster_Code
						 WHERE 		PKFK_Location_Code = \''.$location.'\' AND Doc_ReceivingType = \''.$doc_type.'\' AND ReceivedFrom = \''.$recv_from.'\' 
						 			AND ReferenceNo = \''.$ref_no.'\'';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->result_array(); 
		}

	}

	public function getAllBinLocation(){
		$query_string = 'SELECT 	RTRIM(Loc_Code) AS Loc_Code
						 FROM 		tblLocation
						 WHERE 		LM_Level = 4 and [Loc_Type] = \'WSE\' and Loc_Active = 1
						 ORDER BY 	Loc_Code ASC';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function getLocType($location){
		$query_string = 'SELECT		RTRIM(Loc_Type) as Loc_Type 
						 FROM 		tblLocation
						 WHERE 		Loc_Code = \''.$location.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['Loc_Type'];
		}
	}

	public function getLocationList($location){
		$query_string = 'SELECT     (RTRIM(Loc_Code) + \' - \' + RTRIM(Loc_Desc)) as Description, RTRIM(Loc_Code) as Loc_Code
						 FROM       tblLocation
						 WHERE      (LM_Level = 1) AND (Loc_Active = 1) AND Loc_Code <> \''.$location.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function getSupplierList(){
		$query_string = 'SELECT     (RTRIM(Sup_Code) + \' - \' + RTRIM(Sup_Desc)) as Description, RTRIM(Sup_Code) as Loc_Code
						 FROM       tblSupplier';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function RefNoSearchResults($filter, $doc_type, $recv_from){
		$location = $this->session->userdata('current_user')['login-location'];
		$query_string = '';

		if ($doc_type == 'PUL') {
			$query_string = 'SELECT        RTRIM(POH_SlipNum) as Ref_No
							 FROM          tblPullOuthdr
							 WHERE         (PKFK_Location_Source = \''.$recv_from.'\') AND (FK_Status_Code = \'PUL\') AND POH_SlipNum LIKE \'%'.$filter.'%\'
							 			   AND POH_SlipNum NOT IN ((SELECT RTRIM(RCVH_DocRef) as RCVH_DocRef 
										   FROM tblReceivehdr 
										   WHERE RCVH_DocRef = POH_SlipNum AND RCVH_From = \''.$recv_from.'\' AND (FK_Status_Code = \'RCD\' OR FK_Status_Code = \'NEW\' OR FK_Status_Code = \'CAN\'))) ';
		}
		elseif ($doc_type == 'STF') {
			$query_string = 'SELECT        RTRIM(TSH_STFNum) as Ref_No
							 FROM          tblTrfStockhdr
							 WHERE         (FK_Status_Code = \'STF\') AND (PKFK_Location_Source = \''.$recv_from.'\') AND TSH_STFNum LIKE \'%'.$filter.'%\'';
		}
		elseif ($doc_type == 'DEL') {
			$query_string = 'SELECT        RTRIM(DH_DRNum) as Ref_No
							 FROM          tblDeliveryhdr
							 WHERE         (PKFK_CPH_LocCode = \''.$recv_from.'\') AND (FK_Status_Code = \'DEL\') AND DH_DRNum LIKE \'%'.$filter.'%\'';
		}
		elseif ($doc_type == 'POR') {
			$query_string = 'SELECT        	RTRIM(OH.OH_PONum) as Ref_No
							 FROM           tblOrderHeader AS OH LEFT OUTER JOIN
							                tblOrderDetails AS OD ON OH.OH_PONum = OD.PKFK_OrderHeader_PONum
							 WHERE        	(OH.FK_Status_Code IN (\'DEL\', \'APV\')) AND (OD.OD_OrderQty - OD.OD_RcvdQty > 0) AND OH_PONum LIKE \'%'.$filter.'%\' AND (FK_Supplier_Code = \''.$recv_from.'\')
							 GROUP BY 	   	OH.OH_PONum
							 HAVING        	(COUNT(OD.OD_Count) > 0)
							 ORDER BY 		OH.OH_PONum';
		}


		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}

	}

	public function getAllDetailsByDocType($doc_type, $recv_from, $ref_no, $loc_mask){

		if ($doc_type == 'PUL') {
			$query_string = 'SELECT        RTRIM(PUL.FK_InvMaster_Code) AS FK_InvMaster_Code, ISNULL(PUL.POD_PullOutQty, 0) AS RequiredQty, RTRIM(IM.IM_UPCCode) AS IM_UPCCode, 
										   ISNULL(SV.ScanQty, 0) AS ScanQty, RTRIM(IM.IM_ItemLocation) as IM_ItemLocation
							 FROM          tblPullOutdet AS PUL LEFT OUTER JOIN
							               tblPROD_ScanVerify AS SV ON PUL.FK_InvMaster_Code = SV.PKFK_InvMaster_Code AND PUL.PKFK_PullOut_SlipNum = SV.ReferenceNo AND 
							               PUL.PKFK_Location_Source = SV.ReceivedFrom LEFT OUTER JOIN
							               tblInvMaster AS IM ON PUL.FK_InvMaster_Code = IM.IM_Item_Code
							 WHERE         (PUL.PKFK_PullOut_SlipNum = \''.$ref_no.'\') AND (PUL.FK_Location_Actual = \''.$recv_from.'\')';
		}

		elseif ($doc_type == 'POR') {
			$query_string = 'SELECT        RTRIM(POR.FK_InvMaster_Code) AS FK_InvMaster_Code, ISNULL(POR.OD_OrderQty, 0) AS RequiredQty, RTRIM(IM.IM_UPCCode) AS IM_UPCCode, 
										   ISNULL(SV.ScanQty, 0) AS ScanQty, RTRIM(IM.IM_ItemLocation) as IM_ItemLocation
							 FROM          tblOrderDetails AS POR LEFT OUTER JOIN
							               tblPROD_ScanVerify AS SV ON POR.FK_InvMaster_Code = SV.PKFK_InvMaster_Code AND POR.PKFK_OrderHeader_PONum = SV.ReferenceNo LEFT OUTER JOIN
							               tblInvMaster AS IM ON POR.FK_InvMaster_Code = IM.IM_Item_Code
							 WHERE         (POR.PKFK_OrderHeader_PONum = \''.$ref_no.'\')';
		}

		if($this->sess->isLogin()){
			$result = $this->DB->query($query_string)->result_array();

			return $result;

		}

	}

	public function checkBarcodeExist($barcode){
		$query_string = 'SELECT 		RTRIM(IM_UPCCode) as IM_UPCCode
						 FROM 			tblInvMaster
						 WHERE 			IM_UPCCode = \''.$barcode.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array(); 
		}
	}

	public function checkReceived($ref_no, $recv_from){

		$query_string = 'SELECT		RCVH_DocRef
						 FROM 		tblReceivehdr
						 WHERE 		RCVH_DocRef = \''.$ref_no.'\' AND RCVH_FROM = \''.$recv_from.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}

	}

	public function checkScanVerify($location, $ref_no, $recv_from, $doc_type){
		$query_string = 'SELECT 	*
						 FROM 		tblPROD_ScanVerify
						 WHERE 		PKFK_Location_Code = \''.$location.'\' AND ReceivedFrom = \''.$recv_from.'\' AND ReferenceNo = \''.$ref_no.'\'
						 			AND Doc_ReceivingType = \''.$doc_type.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function countLocationByItemCode($loc_mask, $item_code){

		$query_string = 'SELECT        id.PKFK_Location_Code, id.PKFK_InvMaster_Code, id.INV_Onhand, id.INV_Onhold, ISNULL(id.INV_Active, 0) AS Expr1, id.INV_Updatedby, 
									   id.INV_DateUpdated, l.Loc_Code, l.Loc_Receive, LEFT(l.Loc_Mask, 3) AS Loc_Mask
						 FROM          tblInvdetails AS id LEFT OUTER JOIN
						               tblLocation AS l ON id.PKFK_Location_Code = l.Loc_Code
						 WHERE         (ISNULL(id.INV_Active, 0) = 1) AND (LEFT(l.Loc_Mask, 3) = '.$loc_mask.') AND (id.PKFK_InvMaster_Code = \''.$item_code.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->num_rows();
		}
	}

	public function getLocSourceSingleLocation($loc_mask, $item_code){

		$query_string = 'SELECT        id.PKFK_Location_Code, id.PKFK_InvMaster_Code, id.INV_Onhand, id.INV_Onhold, ISNULL(id.INV_Active, 0) AS Expr1, id.INV_Updatedby, 
									   id.INV_DateUpdated, l.Loc_Code, l.Loc_Receive, LEFT(l.Loc_Mask, 3) AS Loc_Mask
						 FROM          tblInvdetails AS id LEFT OUTER JOIN
						               tblLocation AS l ON id.PKFK_Location_Code = l.Loc_Code
						 WHERE         (ISNULL(id.INV_Active, 0) = 1) AND (LEFT(l.Loc_Mask, 3) = '.$loc_mask.') AND (id.PKFK_InvMaster_Code = \''.$item_code.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['PKFK_Location_Code'];
		}

	}

	public function getItemCodeByBarcode($barcode){
		$query_string = 'SELECT 	RTRIM(IM_Item_Code) as IM_Item_Code
						 FROM 		tblInvMaster
						 WHERE 		IM_UPCCode = \''.$barcode.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['IM_Item_Code'];
		}
	}

	public function getAllDetailsForPosting($location, $ref_no, $recv_from, $doc_type){
		$query_string = 'SELECT 	SV.*, IM.IM_UnitCost, IM.IM_UnitPrice
						 FROM 		tblPROD_ScanVerify AS SV LEFT OUTER JOIN
						 			tblInvMaster AS IM ON SV.PKFK_InvMaster_Code = IM.IM_Item_Code
						 WHERE		SV.PKFK_Location_Code = \''.$location.'\' AND SV.ReceivedFrom = \''.$recv_from.'\' AND SV.ReferenceNo = \''.$ref_no.'\'
						 			AND SV.Doc_ReceivingType = \''.$doc_type.'\' AND SV.ScanQty > 0';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}

	}

	public function getItemActualLocation($item_code){
		$query_string = 'SELECT		RTRIM(IM_ItemLocation) as IM_ItemLocation
						 FROM 		tblInvMaster
						 WHERE 		IM_Item_Code = \''.$item_code.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['IM_ItemLocation'];
		}

	}

	private function checkInvDetail($loc_source, $item_code){
		$query_string = 'SELECT       	 PKFK_Location_Code, PKFK_InvMaster_Code, INV_Onhand, INV_Onhold
						 FROM            tblInvdetails
						 WHERE        	(INV_Active = 1) AND (PKFK_InvMaster_Code = \''.trim($item_code).'\') AND (PKFK_Location_Code = \''.trim($loc_source).'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function updateLocSource($data, $location){


		$this->DB->where('PKFK_Location_Code', trim($location))
				 ->where('ReceivedFrom', $data['recv-from'])
				 ->where('ReferenceNo', $data['ref-no'])
				 ->where('PKFK_InvMaster_Code', $data['item-code'])
				 ->where('Doc_ReceivingType', $data['doc-type'])
				 ->set('Location_Source', $data['bin-loc'])
				 ->update('tblPROD_ScanVerify');

	}

	// Get the latest RR Number

	private function getCurrentRRNo($location){

		$this->updateRCVLastRefDoc($location);

		$query_string = 'SELECT     ISNULL(TT_LastDocRef, 0) AS TT_LastDocRef
						FROM         tblTranstype
						WHERE     	(TT_Code = \'RCV\') AND (PKFK_Location_Code = \''.$location.'\')';

		if($this->sess->isLogin()){
			$lastrefdoc = $this->DB->query($query_string)->row_array()['TT_LastDocRef'];
			return $this->padLeftZero(8, $lastrefdoc);
		}
	}

	private function padLeftZero($num, $txt){

        $pcnt = $num - strlen($txt);
        $output = '';

        for ($i=0; $i < $pcnt; $i++) { 
            $output .= '0';
        }

        $output = $output.$txt;

        return $output;

    }

    // Update last RR Number

	private function updateRCVLastRefDoc($location){
		$query_string = 'UPDATE tblTranstype
						SET TT_LastDocRef = TT_LastDocRef + 1
						WHERE PKFK_Location_Code = \''.$location.'\' AND TT_Code = \'RCV\'';

		
		if($this->sess->isLogin()){
			$this->DB->query($query_string);
		}
	}

	// End

	public function on_post_received($location, $header, $details, $total, $dbase){

		$status = 'RCD';

		foreach ($details as $key => $value) {
			if($value['Location_Source'] == null){
				$status = 'NEW';
				break;
			}
		}
		
		$rr_no = $this->getCurrentRRNo($location);

		$save_rcv_hdr = array(
			'PKFK_Location_Code'  	=> $location,
			'RCVH_RRNum' 			=> 'RR'.$rr_no,
			'FK_Transtype_Code' 	=> $header['doc-type'],
			'RCVH_Preparedby' 		=> $this->session->userdata('current_user')['login-user'],
			'RCVH_Remarks' 			=> $header['remarks'], 	
			'RCVH_DocRef' 			=> $header['ref-no'],
			'RCVH_From' 			=> $header['recv-from'],
			'FK_Status_Code' 		=> $status,
			'FK_Users_Postedby' 	=> $this->session->userdata('current_user')['login-user'],
			'RCVH_TotalRcvQty' 		=> $total 
		);

		if ($header['doc-type'] == 'PUL') {
			$dbase->where('PKFK_Location_Source', $location)
					 ->where('POH_SlipNum', $header['ref-no'])
					 ->set('POH_TotalPullOutQty', $total)
					 ->update('tblPullOuthdr');
		}
		elseif ($header['doc-type'] == 'POR'){

			$query = 'SELECT 	ISNULL(OH_TotalOrderQty, 0) as OH_TotalOrderQty
					  FROM  	tblOrderHeader
					  WHERE 	OH_PONum = \''.$header['ref-no'].'\'';

			$order_qty = $dbase->query($query)->row_array()['OH_TotalOrderQty'];

			$update_por = array(
				'OH_TotalRcvdQty'  	=> $total,
				'FK_Status_Code'   	=> ($order_qty <= $total ? 'POC' : 'DEL')
			);

			$dbase->where('OH_PONum', $header['ref-no'])
					 ->update('tblOrderHeader', $update_por);
		}


		$dbase->set('RCVH_Date', 'GETDATE()', false);
		$dbase->set('RCVH_Postdate', 'GETDATE()', false);
		$dbase->insert('tblReceivehdr', $save_rcv_hdr);

		foreach ($details as $key => $detail) {
			$save_rcv_dtl = array(
				'PKFK_Location_Code' 	=> $detail['PKFK_Location_Code'],
				'PKFK_RCVH_RRNum' 		=> 'RR'.$rr_no,
				'RCVD_Count' 			=> $key + 1,
				'FK_Location_Actual' 	=> $detail['Location_Source'],
				'FK_InvMaster_Code' 	=> $detail['PKFK_InvMaster_Code'],
				'RCVD_RcvQty' 			=> $detail['ScanQty'],
				'RCVD_RefDoc' 			=> $detail['ReferenceNo'],
				'RCVD_Damaged' 			=> false,
				'RCVD_Remarks' 			=> $detail['Doc_Remarks']
			);

			$dbase->insert('tblReceivedet', $save_rcv_dtl);

			if ($header['doc-type'] == 'PUL') {
				$dbase->where('PKFK_Location_Source', $location)
						 ->where('PKFK_PullOut_SlipNum', $header['ref-no'])
						 ->where('FK_InvMaster_Code', $detail['PKFK_InvMaster_Code'])
						 ->where('FK_Location_Actual', $detail['Location_Source'])
						 ->set('POD_RcvQty', $total)
						 ->update('tblPullOutdet');
			}
			elseif ($header['doc-type'] == 'POR'){
				$dbase->where('PKFK_OrderHeader_PONum', $header['ref-no'])
						 ->where('FK_InvMaster_Code', $detail['PKFK_InvMaster_Code'])
						 ->set('OD_RcvdQty', $total)
						 ->update('tblOrderDetails');
			}

			if($status == 'RCD'){
				$save_inv_trans = array(
					'PKFK_Location_Code' 		=> $detail['PKFK_Location_Code'],
					'PKFK_Transtype_Code' 		=> 'RCV',
					'IT_TransNo' 				=> 'RR'.$rr_no,
					'IT_TransSeq' 				=> $key + 1,
					'FK_InvMaster_Code' 		=> $detail['PKFK_InvMaster_Code'],
					'IT_Qty' 					=> $detail['ScanQty'],
					'IT_UnitCost' 				=> $detail['IM_UnitCost'],
					'IT_UnitPrice' 				=> $detail['IM_UnitPrice'],
					'FK_Users_Postedby' 		=> $this->session->userdata('current_user')['login-user'] 
				);

				$dbase->set('IT_PostedDate', 'GETDATE()', false);
				$dbase->insert('tblInvTransaction', $save_inv_trans);

				$checkInvDtl = $this->checkInvDetail($detail['Location_Source'], $detail['PKFK_InvMaster_Code']);

				if(empty($checkInvDtl)){
					$save_inv_dtl = array(
						'PKFK_Location_Code' 		=> $detail['Location_Source'],
						'PKFK_InvMaster_Code' 		=> $detail['PKFK_InvMaster_Code'],
						'INV_Onhand' 				=> $detail['ScanQty'],
						'INV_Onhold' 				=> 0,
						'INV_Active' 				=> 1,
						'INV_Updatedby' 			=> $this->session->userdata('current_user')['login-user']
					);

					$dbase->set('INV_DateUpdated', 'GETDATE()', false);
					$dbase->insert('tblInvdetails', $save_inv_dtl);
				}
				else{
					$dbase->where('PKFK_Location_Code', $detail['Location_Source'])
							 ->where('PKFK_InvMaster_Code', $detail['PKFK_InvMaster_Code'])
							 ->set('INV_Onhand', 'INV_Onhand + '.$detail['ScanQty'], false)
							 ->set('INV_Updatedby', $this->session->userdata('current_user')['login-user'])
							 ->set('INV_DateUpdated', 'GETDATE()', false)
							 ->update('tblInvdetails');
				}

			}
		}


		$dbase->where('PKFK_Location_Code', $location)
				 ->where('ReceivedFrom', $header['recv-from'])
				 ->where('Doc_ReceivingType', $header['doc-type'])
				 ->where('ReferenceNo', $header['ref-no'])
				 ->delete('tblPROD_ScanVerify');

	}

	private function checkSVDItemExist($location, $ref_no, $recv_from, $doc_type, $item_code){
		$query_string = 'SELECT 	PKFK_InvMaster_Code
						 FROM 		tblPROD_ScanVerify
						 WHERE 		PKFK_Location_Code = \''.$location.'\' AND ReceivedFrom = \''.$recv_from.'\' AND ReferenceNo = \''.$ref_no.'\'
						 			AND Doc_ReceivingType = \''.$doc_type.'\' AND PKFK_InvMaster_Code = \''.$item_code.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}

	public function on_save_scan_verify($details, $location, $data){

		$this->db->trans_begin();

		foreach($details as $key => $detail){

			$save_scn_vrfy = array(
				'PKFK_Location_Code'  	=> $location,
				'ReceivedFrom' 		  	=> $data['recv-from'],
				'ReferenceNo' 		  	=> $data['ref-no'],
				'PKFK_InvMaster_Code' 	=> $detail['FK_InvMaster_Code'],
				'Location_Source' 		=> ($this->getLocType($location) == 'STR' ? $location : $detail['IM_ItemLocation']),
				'UPCCode' 				=> $detail['IM_UPCCode'],
				'RequiredQty' 			=> $detail['RequiredQty'],
				'ScanQty' 				=> $detail['ScanQty'],
				'Doc_Remarks' 			=> $data['remarks'],
				'Doc_ReceivingType' 	=> $data['doc-type'],
				'PK_WSID' 				=> $this->session->userdata('current_user')['login-wsid']
			);

			$current_sv_item = $this->checkSVDItemExist($location, $data['ref-no'], $data['recv-from'], $data['doc-type'], $detail['FK_InvMaster_Code']);

			if(!empty($current_sv_item)){
				$this->db->where('PKFK_Location_Code', $location)
						 ->where('ReceivedFrom', $data['recv-from'])
						 ->where('ReferenceNo', $data['ref-no'])
						 ->where('Doc_ReceivingType', $data['doc-type'])
						 ->update('ScanQty', 'ScanQty + '.$detail['ScanQty']);
			}
			else{
				$this->DB->set('TransDate', 'GETDATE()', false);
				$this->DB->insert('tblPROD_ScanVerify', $save_scn_vrfy);
			}

		}


		if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $output = array(
                                'success'=>false
                            ,   'message'=>$this->db->error()
                        );
	    }
        else{
                $this->db->trans_commit();
        }

	}

	public function on_update_scan_verify($location, $data, $dbase){

		if($data['scn-qty'] == 0 && $data['req-qty'] == 0){
			$dbase->where('PKFK_Location_Code', $location)
					 ->where('ReceivedFrom', $data['recv-from'])
					 ->where('ReferenceNo', $data['ref-no'])
					 ->where('PKFK_InvMaster_Code', $data['item-code'])
					 ->where('Doc_ReceivingType', $data['doc-type'])
					 ->delete('tblPROD_ScanVerify');
		}
		else{
			$dbase->where('PKFK_Location_Code', $location)
					 ->where('ReceivedFrom', $data['recv-from'])
					 ->where('ReferenceNo', $data['ref-no'])
					 ->where('PKFK_InvMaster_Code', $data['item-code'])
					 ->where('Doc_ReceivingType', $data['doc-type'])
					 ->set('ScanQty', $data['scn-qty'])
					 ->update('tblPROD_ScanVerify');

		}

	}

	public function on_create_scan_verify($location, $data, $item_code, $loc_mask, $dbase){


		$count_loc = $this->countLocationByItemCode($loc_mask, $item_code);
		$actual_loc = $this->getItemActualLocation($item_code);

		$loc_source = null;

		if($count_loc == 1){
			$loc_source = $this->getLocSourceSingleLocation($loc_mask, $item_code);
		}
		else{
			$loc_type = $this->getLocType($location);

			if($loc_type == 'STR'){
				$loc_source = $location;
			}
			else{
				if(!empty($actual_loc)){
					$loc_source = $actual_loc;
				}
			}
		}

		$save_scn_vrfy = array(
			'PKFK_Location_Code'  	=> $location,
			'ReceivedFrom' 		  	=> $data['recv-from'],
			'ReferenceNo' 		  	=> $data['ref-no'],
			'PKFK_InvMaster_Code' 	=> $item_code,
			'Location_Source' 		=> $loc_source,
			'UPCCode' 				=> $data['upc-code'],
			'RequiredQty' 			=> 0,
			'ScanQty' 				=> $data['scn-qty'],
			'Doc_Remarks' 			=> $data['remarks'],
			'Doc_ReceivingType' 	=> $data['doc-type'],
			'PK_WSID' 				=> $this->session->userdata('current_user')['login-wsid']
		);

		$dbase->set('TransDate', 'GETDATE()', false);
		$dbase->insert('tblPROD_ScanVerify', $save_scn_vrfy);

	}

	public function on_reset_scan_verify($location, $data, $dbase){

		$dbase->where('PKFK_Location_Code', $location)
				 ->where('ReceivedFrom', $data['recv-from'])
				 ->where('ReferenceNo', $data['ref-no'])
				 ->where('RequiredQty', 0)
				 ->where('Doc_ReceivingType', $data['doc-type'])
				 ->delete('tblPROD_ScanVerify');

		$dbase->where('PKFK_Location_Code', $location)
				 ->where('ReceivedFrom', $data['recv-from'])
				 ->where('ReferenceNo', $data['ref-no'])
				 ->where('Doc_ReceivingType', $data['doc-type'])
				 ->set('ScanQty', 0)
				 ->update('tblPROD_ScanVerify');


        return $this->getAllDetails($location, $data['doc-type'], $data['recv-from'], $data['ref-no'], $dbase);

	}

	public function getCurrentScanQty($location, $item_code, $ref_no, $recv_from){

		$query_string = 'SELECT        	ScanQty
						 FROM         	tblPROD_ScanVerify
						 WHERE        	(ReferenceNo = \''.$ref_no.'\') AND (PKFK_InvMaster_Code = \''.$item_code.'\') AND (PKFK_Location_Code = \''.$location.'\') AND (ReceivedFrom = \''.$recv_from.'\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['ScanQty'];
		}

	}

}