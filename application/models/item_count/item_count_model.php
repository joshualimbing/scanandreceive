<?php 

defined('BASEPATH') or exit('No direct script access allowed');

/**
 Item Count Model - Created by Joshua Limbing (10/5/2019)
 */

class item_count_model extends MAIN_Model
{
	
	public function PCountSearchResults($location, $filter){
		$query_string = 'SELECT        	RTRIM(PCH_CountNum) AS PCH_CountNum
						 FROM           tblPlanCounthdr
						 WHERE        	(FK_Status_Code IN (\'NEW\', \'APV\')) AND (FK_Location_Code = \''.$location.'\') AND (PCH_CountNum LIKE \'%'.$filter.'%\')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function CSheetSearchResults($location, $pcount, $filter, $dbase){
		$query_string = 'SELECT        	RTRIM(CSH_CountSheetNo) AS CSH_CountSheetNo, 
										(RTRIM(CSH_CountSheetNo) + \' - \' + RTRIM(FK_Location_Code)) AS Description
						 FROM           tblCountSheethdr
						 WHERE        	(FK_Status_Code IN (\'NEW\', \'APV\')) AND (PKFK_PCH_Location_Code = \''.$location.'\') AND (PKFK_PCH_CountNum = \''.$pcount.'\') 
						 				AND ((CSH_CountSheetNo LIKE \'%'.$filter.'%\') OR FK_Location_Code LIKE \'%'.$filter
						 				.'%\')';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->result_array(); 
		}
	}

	public function getLastUser($location, $pcount, $csheet){
		$query_string = 'SELECT        	RTRIM(UserID) as UserID
						 FROM           tblPROD_ItemCount
						 WHERE        	(Location_Code = \''.$location.'\') AND (PKFK_PCH_CountNum = \''.$pcount.'\') AND (PKFK_CSH_CountSheetNo = '.$csheet.')
						 GROUP BY 		Location_Code, PKFK_PCH_CountNum, PKFK_CSH_CountSheetNo, UserID';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['UserID']; 
		}
	}

	public function checkBarcodeExist($barcode){
		$query_string = 'SELECT 		RTRIM(IM_UPCCode) as IM_UPCCode
						 FROM 			tblInvMaster
						 WHERE 			IM_UPCCode = \''.$barcode.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array(); 
		}
	}

	public function getICDetails($location, $pcount, $csheet){
		$query_string = 'SELECT			RTRIM(IC.PKFK_InvMaster_Code) as PKFK_InvMaster_Code, IC.ScanQty, RTRIM(IM.IM_UPCCode) as IM_UPCCode, 
										(CASE WHEN IC.isOriginal = \'True\' THEN \'True\' ELSE \'False\' END) as isOriginal, IC.ActualQty, IC.OnhandQty
						 FROM 			tblPROD_ItemCount as IC LEFT OUTER JOIN
						 				tblInvMaster as IM ON IC.PKFK_InvMaster_Code = IM.IM_Item_Code
						 WHERE			Location_Code = \''.$location.'\' AND PKFK_PCH_CountNum = \''.$pcount.'\' AND PKFK_CSH_CountSheetNo = \''.$csheet.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function getCountsheetLoc($location, $pcount, $csheet, $dbase){
		$query_string = 'SELECT 		RTRIM(FK_Location_Code) as FK_Location_Code
						 FROM 			tblCountSheethdr
						 WHERE 			PKFK_PCH_CountNum = \''.$pcount.'\' AND CSH_CountSheetNo = \''.$csheet.'\' AND PKFK_PCH_Location_Code = \''.$location.'\'';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->row_array()['FK_Location_Code']; 
		} 
	}

	public function getTotalScanQty($location, $pcount, $csheet){
		$query_string = 'SELECT			SUM(ScanQty) as ScanQty
						 FROM 			tblPROD_ItemCount
						 WHERE			Location_Code = \''.$location.'\' AND PKFK_PCH_CountNum = \''.$pcount.'\' AND PKFK_CSH_CountSheetNo = \''.$csheet.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['ScanQty']; 
		}
	}

	public function checkICDetailsExist($location, $pcount, $csheet){
		$query_string = 'SELECT        	Location_Code, PKFK_PCH_CountNum, PKFK_CSH_CountSheetNo, CountNo, PKFK_InvMaster_Code, ScanQty, PK_WSID, UserID
						 FROM           tblPROD_ItemCount
						 WHERE        	(Location_Code = \''.$location.'\') AND (PKFK_PCH_CountNum = \''.$pcount.'\') AND (PKFK_CSH_CountSheetNo = '.$csheet.')';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->result_array(); 
		}
	}

	public function getItemCodeByBarcode($barcode){
		$query_string = 'SELECT 	RTRIM(IM_Item_Code) as IM_Item_Code
						 FROM 		tblInvMaster
						 WHERE 		IM_UPCCode = \''.$barcode.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['IM_Item_Code'];
		}
	}

	private function getLastLineNo($location, $data){
		$query_string = 'SELECT        	TOP (1) CountNo
						 FROM           tblPROD_ItemCount
						 WHERE        	(PKFK_PCH_CountNum = \''.$data['pcount'].'\') AND (Location_Code = \''.$location.'\') AND (PKFK_CSH_CountSheetNo = '.$data['csheet'].')
						 ORDER BY CountNo DESC';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['CountNo'];
		}
	}

	private function getCountSheetLocationAndStatus($location, $data){
		$query_string = 'SELECT 		RTRIM(FK_Location_Code) as FK_Location_Code, RTRIM(FK_Status_Code) as FK_Status_Code 
						 FROM 			tblCountSheethdr
						 WHERE 			PKFK_PCH_Location_Code = \''.$location.'\' AND PKFK_PCH_CountNum = \''.$data['pcount'].'\' AND CSH_CountSheetNo = \''.$data['csheet'].'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}

	private function getPCountStatus($location, $pcount){
		$query_string = 'SELECT 		RTRIM(FK_Status_Code) as FK_Status_Code
						 FROM 			tblPlanCounthdr
						 WHERE			FK_Location_Code = \''.$location.'\' AND PCH_CountNum = \''.$pcount.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['FK_Status_Code'];
		}
	}

	private function getTotalCountsheetQtyByPCount($location, $pcount){
		$query_string = 'SELECT 		SUM(CSH_TotalActualQty) as CSH_TotalActualQty
						 FROM 			tblCountSheethdr
						 WHERE			PKFK_PCH_Location_Code = \''.$location.'\' AND PKFK_PCH_CountNum = \''.$pcount.'\'';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['CSH_TotalActualQty'];
		}
	}

	private function checkIfItemExist($location, $pcount, $actual_loc, $item_code){
		$query_string = 'SELECT 		FK_InvMaster_Code
						 FROM 			tblPlanCountdtl
						 WHERE 			(FK_Location_Actual = \''.$actual_loc.'\') AND (PKFK_PCH_Location_Code = \''.$location.'\') AND (PKFK_PCH_CountNum = \''.$pcount.'\')
						 				AND (FK_InvMaster_Code = \''.$item_code.'\')
						 GROUP BY 	FK_InvMaster_Code			';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array();
		}
	}

	private function getPCountLastLineNo($location, $pcount){
		$query_string = 'SELECT 		TOP (1) PCD_Count
						 FROM 			tblPlanCountdtl
						 WHERE 			PKFK_PCH_Location_Code = \''.$location.'\' AND PKFK_PCH_CountNum = \''.$pcount.'\'
						 ORDER BY 		PCD_Count DESC';

		if($this->sess->isLogin()){
			return $this->DB->query($query_string)->row_array()['PCD_Count'];
		}
	}	

	// INSERT/UPDATE/DELETE Functions

	public function delete_added_item_count($location, $data, $dbase){
		$dbase->where('Location_Code', $location)
				 ->where('PKFK_PCH_CountNum', $data['pcount'])
				 ->where('PKFK_CSH_CountSheetNo', $data['csheet'])
				 ->where('isOriginal', False)
				 ->delete('tblPROD_ItemCount');
	}

	public function createICDetails($location, $data){

		// Countsheet detail query
		$query_string = 'SELECT       	PKFK_PCH_Location_Code, PKFK_PCH_CountNum, PKFK_CSH_CountSheetNo, CSD_Count, PKFK_Location_Actual, PKFK_InvMaster_Code, CSD_ActualQty,
								 		CSD_OnHandQty
						 FROM           tblCountSheetdtl
						 WHERE        	(PKFK_PCH_Location_Code = \''.$location.'\') AND (PKFK_PCH_CountNum = \''.$data['pcount'].'\') AND (PKFK_CSH_CountSheetNo = \''.$data['csheet'].'\')';

		$result_csd = $this->DB->query($query_string)->result_array();

		foreach($result_csd as $ley => $ic_dtl){
			$save_ic_dtl = array(
				'Location_Code' 		=> $location,
				'PKFK_PCH_CountNum'  	=> $data['pcount'],
				'PKFK_CSH_CountSheetNo' => $data['csheet'],
				'CountNo' 				=> $ic_dtl['CSD_Count'],
				'PKFK_InvMaster_Code' 	=> $ic_dtl['PKFK_InvMaster_Code'],
				'ScanQty' 				=> $ic_dtl['CSD_ActualQty'],
				'OnhandQty' 			=> $ic_dtl['CSD_OnHandQty'],
				'PK_WSID' 				=> $this->session->userdata('current_user')['login-wsid'],
				'UserID' 				=> $this->session->userdata('current_user')['login-user'],
				'isOriginal' 			=> True,
				'ActualQty' 			=> $ic_dtl['CSD_ActualQty'] 
			);

			$this->DB->insert('tblPROD_ItemCount', $save_ic_dtl);
		}

		// $this->DB->where('PKFK_PCH_Location_Code', $location)
		// 		 ->where('PKFK_PCH_CountNum', $data['pcount'])
		// 		 ->where('PKFK_CSH_CountSheetNo', $data['csheet'])
		// 		 ->delete('tblCountSheetdtl');
	}

	public function on_update_item_count($location, $data, $dbase){

		if($data['scn-qty'] == 0 && $data['is-original'] == 'False'){
			$dbase->where('Location_Code', $location)
					 ->where('PKFK_InvMaster_Code', $data['item-code'])
					 ->where('PKFK_CSH_CountSheetNo', $data['csheet'])
					 ->where('PKFK_PCH_CountNum', $data['pcount'])
					 ->delete('tblPROD_ItemCount');
		}
		else{
			$dbase->where('Location_Code', $location)
					 ->where('PKFK_InvMaster_Code', $data['item-code'])
					 ->where('PKFK_CSH_CountSheetNo', $data['csheet'])
					 ->where('PKFK_PCH_CountNum', $data['pcount'])
					 ->set('ScanQty', $data['scn-qty'])
					 ->update('tblPROD_ItemCount');

		}

	}

	public function on_create_item_count($location, $data, $item_code, $dbase){

		$save_item_cnt = array(
			'Location_Code'  			=> $location,
			'PKFK_PCH_CountNum' 		=> $data['pcount'],
			'PKFK_CSH_CountSheetNo' 	=> $data['csheet'],
			'PKFK_InvMaster_Code' 		=> $item_code,
			'CountNo' 					=> $this->getLastLineNo($location, $data) + 1,
			'ScanQty' 					=> $data['scn-qty'],
			'OnhandQty' 				=> 0,
			'PK_WSID' 					=> $this->session->userdata('current_user')['login-wsid'],
			'UserID' 					=> $this->session->userdata('current_user')['login-user'],
			'isOriginal' 				=> False,
			'ActualQty' 				=> 0 
		);

		$dbase->insert('tblPROD_ItemCount', $save_item_cnt);
	}

	public function on_reset_item_count($location, $pcount, $csheet, $dbase){
		$query_string = 'UPDATE 	tblPROD_ItemCount
						 SET 		ScanQty = ActualQty
						 WHERE 		Location_Code = \''.$location.'\' AND PKFK_PCH_CountNum = \''.$pcount.'\' AND PKFK_CSH_CountSheetNo = \''.$csheet.'\'';

		$dbase->query($query_string);
	}

	public function on_save_item_count($location, $data, $ic_dtl, $total_scn, $dbase){

		$csheet_hdr 	= $this->getCountSheetLocationAndStatus($location, $data);
		$pcount_status 	= $this->getPCountStatus($location, $data['pcount']);
		$total_csheet 	= $this->getTotalCountsheetQtyByPCount($location, $data['pcount']);

		/* Header */

		// Update Countsheet Header
		$dbase->where('PKFK_PCH_Location_Code', $location)
				 ->where('PKFK_PCH_CountNum', $data['pcount'])
				 ->where('CSH_CountSheetNo', $data['csheet'])
				 ->set('CSH_TotalActualQty', $total_scn)
				 ->update('tblCountSheethdr');

		$dbase->where('FK_Location_Code', $location)
				 ->where('PCH_CountNum', $data['pcount'])
				 ->set('PCH_TotalActualQty', $total_csheet)
				 ->update('tblPlanCounthdr');

		/* Detail */

		$dbase->where('PKFK_PCH_Location_Code', $location)
				 ->where('PKFK_PCH_CountNum', $data['pcount'])
				 ->where('PKFK_CSH_CountSheetNo', $data['csheet'])
				 ->delete('tblCountSheetdtl');

		$current_line_no = 1;
		foreach ($ic_dtl as $key => $detail) {
			$check_item = $this->checkIfItemExist($location, $data['pcount'], $csheet_hdr['FK_Location_Code'], $detail['PKFK_InvMaster_Code']);

			if(!empty($check_item)){
				$ic_dtl[$key]['isExisting'] = 1;
				$ic_dtl[$key]['lineNo'] = 0;
			}
			else{
				$ic_dtl[$key]['isExisting'] = 0;
				$ic_dtl[$key]['lineNo'] = $this->getPCountLastLineNo($location, $data['pcount']) + $current_line_no;
				$current_line_no++;
			}
		}	 

		foreach ($ic_dtl as $key => $detail) {
			$save_csheet_dtl = array(
				'PKFK_PCH_Location_Code' 	=> $location,
				'PKFK_PCH_CountNum' 		=> $data['pcount'],
				'PKFK_CSH_CountSheetNo' 	=> $data['csheet'],
				'CSD_Count' 				=> $key + 1,
				'PKFK_Location_Actual' 		=> $csheet_hdr['FK_Location_Code'],
				'PKFK_InvMaster_Code' 		=> $detail['PKFK_InvMaster_Code'],
				'CSD_OnHandQty' 			=> $detail['OnhandQty'],
				'CSD_ActualQty' 			=> $detail['ScanQty'],
				'CSD_DiffQty' 				=> $detail['OnhandQty'] - $detail['ScanQty'],
			);

			$dbase->insert('tblCountSheetdtl', $save_csheet_dtl);
			

			if($detail['isExisting']){
				$dbase->where('PKFK_PCH_Location_Code', $location)
						 ->where('PKFK_PCH_CountNum', $data['pcount'])
						 ->where('FK_Location_Actual', $csheet_hdr['FK_Location_Code'])
						 ->where('FK_InvMaster_Code', $detail['PKFK_InvMaster_Code'])
						 ->set('PCD_ActualQty', $detail['ScanQty'])
						 ->update('tblPlanCountdtl');
			}
			else{
				$save_pcount_dtl = array(
					'PKFK_PCH_Location_Code' 	=> $location,
					'PKFK_PCH_CountNum' 		=> $data['pcount'],
					'PCD_Count' 				=> $detail['lineNo'],
					'FK_Location_Actual' 		=> $csheet_hdr['FK_Location_Code'],
					'FK_InvMaster_Code' 		=> $detail['PKFK_InvMaster_Code'],
					'PCD_HoldQty' 				=> 0,
					'PCD_ActualQty' 			=> $detail['ScanQty'] 
				);

				$dbase->insert('tblPlanCountdtl', $save_pcount_dtl);
			}
		}

		/* Clear temporary table */

		$dbase->where('Location_Code', $location)
				 ->where('PKFK_PCH_CountNum', $data['pcount'])
				 ->where('PKFK_CSH_CountSheetNo', $data['csheet'])
				 ->delete('tblPROD_ItemCount');

	}

	public function getCurrentScanQty($location, $item_code, $pcount, $csheet, $dbase){

		$query_string = 'SELECT        	ScanQty
						 FROM           tblPROD_ItemCount
						 WHERE        	(Location_Code = \''.$location.'\') AND (PKFK_PCH_CountNum = \''.$pcount.'\') AND (PKFK_CSH_CountSheetNo = \''.$csheet.'\') AND (PKFK_InvMaster_Code = \''.$item_code.'\')';

		if($this->sess->isLogin()){
			return $dbase->query($query_string)->row_array()['ScanQty'];
		}

	}

}