<?php 
 

class MAIN_Controller extends CI_Controller{ 
	
	protected $data = array();
	protected $sess = null;
	protected $is_secure = false;
	protected $DB;
	protected $location;
	protected $wsid;
	protected $loc_mask;
	protected $user_id;

	
	public function __construct(){
		parent::__construct();
		$this->sess = new Sys\Sess\SessionLoad();
		if($this->sess->isLogin()){
			if(isset($this->session->userdata()['current_user']['login-db'])){
            	$this->DB = $this->load->database($this->session->userdata()['current_user']['login-db'], true);
			}
            $this->location = $this->session->userdata('current_user')['login-location'];
            $this->loc_mask = $this->session->userdata('current_user')['login-mask'];
            $this->wsid = $this->session->userdata('current_user')['login-wsid'];
            $this->user_id = $this->session->userdata('current_user')['login-user'];
        }
	
	}	
	
	public function __destruct(){
		
	}


	
	protected function load_model($modelname){
		
		$this->load->helper('model_helper');				
		$model = generateModelName($modelname);
		if(!isset($this->$model)){
			$this->load->model($modelname, $model);
		}
		return $this->$model;
		
	}
	
	public function load_page($filename, $data, $bool = false){		
		$filename = (substr($filename, 0, 1) == '/')? substr($filename, 1) : $filename;
		
		if($this->sess->isLogin()){
			if(isset($this->session->userdata('current_user')['login-db'])){
				//Load access
				$this->load->view('components/header');
				$this->load->view('components/jslist');
				$this->load->view($filename, $data, $bool);	
				$this->load->view('components/footer');
			}
			else{
				session_destroy();
				$this->redirect(DOMAIN);
			}
		}
		else{
			$this->redirect(DOMAIN);
		}			
	}	
	
	public function redirect($url, $permanent = false){
		
		header('Location: ' . $url, true, $permanent ? 301 : 302);		
		exit();
	}
	

	public function print_r($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}

	public function print_lastquery(){
		echo "<pre>";
		print_r($this->db->last_query());
		echo "</pre>";
		die();	
	}

	public function set_error_message($title, $message, $type){
		$_SESSION['alert'] = array(
			'title' 	=>  $title,
			'message' 	=>  $message,
			'type' 		=>  $type 
		);
	}

}