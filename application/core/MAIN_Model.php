<?php 


class MAIN_Model extends CI_Model{

	protected $_table = '';
    protected $DB;
    protected $loc_mask;


	public function __construct(){
		parent::__construct();
        $this->sess = new Sys\Sess\SessionLoad();

        if($this->sess->isLogin()){
            $this->DB = $this->load->database($this->session->userdata()['current_user']['login-db'], true);
            $this->loc_mask = $this->session->userdata('current_user')['login-mask'];
        }
	}

	public function insert($data){
       return $this->db->insert($this->_table, $data);
    }

    public function update($data, $filter){
        $this->db->where($filter);
        return $this->db->update($this->_table, $data);
    }

    public function delete($filter){
        $this->db->where($filter);
        return $this->db->delete($this->_table);
    }

    // Added by Jekzel Leonidas
    public function print_r($data){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    public function print_lastquery(){
        echo "<pre>";
        print_r($this->db->last_query());
        echo "</pre>";
        die();
    }

    public function sql_hash($where){
        return 'CONVERT(VARCHAR(32),HashBytes(\'MD5\', '. $where .'),2)';
    }

    public function set_alert($type, $message){
        $_SESSION['alert'] = array(
                                'type' => $type, 
                                'message' => $message);
    }

	
}