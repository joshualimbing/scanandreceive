<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Scan_pack extends MAIN_Controller
{
	
	public function index(){
		$data = array();
		$location = $this->session->userdata('current_user')['login-location'];
		$data['workstation_id'] = $this->session->userdata('current_user')['login-wsid'];

		$this->load->model('scan_pack/scan_pack_model');

		if($this->sess->isLogin()){
			$data['picklist_no'] = $this->scan_pack_model->getPickListNo($location);
		}


		$this->load_page('scan_pack/index', $data);
	}

	public function item_entry(){

		$this->load->model('scan_pack/scan_pack_model');

		$data = array();
		$location = $this->session->userdata('current_user')['login-location'];
		$data['carton_no'] = $_GET['carton_no'];

		if(isset($_GET['barcode'])){
			$barcode = $_GET['barcode'];
			$data['header'] = $this->scan_pack_model->getPNHeaderbyBarcode($barcode, $location, $_GET['carton_no']);
		}

		if(isset($_GET['picklist_no'])){
			$pl = $_GET['picklist_no'];
			$data['header'] = $this->scan_pack_model->getPNHeaderbyPicklistNo($pl, $location, $_GET['carton_no']);
		}

		$data['carton'] = $this->scan_pack_model->checkCartonExist($location, $data['header']['PNH_Picknum'], $data['carton_no']);
		// $data['carton_total_qty'] = $this->scan_pack_model->getTotalCartonQty($location, $data['header']['PNH_Picknum'], $data['carton_no']);
		$data['details'] = $this->scan_pack_model->getPNDetails($data['header']['PNH_Picknum'], $location, $data['carton_no']);

		$this->load_page('scan_pack/form', $data);
	}

	public function show_carton(){

		$data = array();

		$this->load_page('scan_pack/form-show-carton', $data);

	}

	public function checkBarcodeifExisting(){
		$data = $this->input->post('barcode');
		$carton_no = $this->input->post('carton-no');
		$location = $this->session->userdata('current_user')['login-location'];

		$this->load->model('scan_pack/scan_pack_model');

		$barcode = $this->scan_pack_model->checkBarcode($location, $data);

		$check_carton 	= $this->scan_pack_model->checkCartonExist($location, $barcode['PNH_Picknum'], $carton_no);
		$scn_carton 	= $this->scan_pack_model->checkSCNCarton($location, $barcode['PNH_Picknum'], $carton_no);
		$current_user 	= $this->session->userdata('current_user')['login-user'];
		$wsid 			= $this->session->userdata('current_user')['login-wsid'];
		$last_user 		= $this->scan_pack_model->getLastUser($location, $barcode['PNH_Picknum'], $carton_no);
		$scn_carton 	= $this->scan_pack_model->checkSCNCarton($location, $data, $carton_no);
		$loc_destin		= $this->scan_pack_model->getLocDestination($this->location, $data);
		$ir_num 		= $this->scan_pack_model->getIRNumberByPickListNo($data, $this->location);
		$details 		= array(
							'picklist-no'  	=> $barcode['PNH_Picknum'],
							'carton-no' 	=> $carton_no,
							'loc-dest' 		=> $loc_destin 
						  );


		if(ALLOW_MULTIPLE_LOC == 0){

			$count_loc = $this->scan_pack_model->countLocation($location, $barcode['PNH_Picknum']);

			if(!empty($barcode)){
				$output = array(
					'success' => true,
					'message' => ''
				);
				if(!empty($count_loc)){
					$output = array(
						'success' 	=> false,
						'message' 	=> 'Cannot allow picklist with multiple locations',
						'type' 		=> 'info',
						'title' 	=> 'Information' 
					);
				}
				else{
					if(!empty($check_carton)){
						if($current_user == $last_user){
							if($check_carton['FK_Status_Code'] == 'CCP'){
								$output = array(
									'success' 	=> false,
									'message' 	=> 'Carton No. '.$carton_no.' on Barcode ID '.$barcode['PNH_Picknum'].' already exists',
									'type' 		=> 'error',
									'title' 	=> 'Error' 
								);
							}
						}
						else{
							if($check_carton['FK_Status_Code'] == 'CCP'){
								$output = array(
									'success' 	=> false,
									'message' 	=> 'Carton No. '.$carton_no.' on Picklist No. '.$barcode['PNH_Picknum'].' was already closed',
									'type' 		=> 'error',
									'title' 	=> 'Error' 
								);
							}
							else{
								$output = array(
									'success' 	=> false,
									'message' 	=> 'Carton No. '.$carton_no.' in Picklist No. '.$barcode['PNH_Picknum'].' is currently used by '.$last_user,
									'type' 		=> 'error',
									'title' 	=> 'Error' 
								);
							}
						}
					}
					else{
						$this->scan_pack_model->on_create_cartonHdr($details, $location, $ir_num, $wsid);
						$this->scan_pack_model->updateWSID($location, $data, $current_user);
					}
				}
			}
			else{
				$output = array(
					'success' 	=> false,
					'message' 	=> 'Barcode does not exist',
					'type' 		=> 'error',
					'title' 	=> 'Error' 
				);
			}


		}
		else{
			if(!empty($barcode)){
				$output = array(
					'success' => true,
					'message' => ''
				);

				if(!empty($check_carton)){
					if($current_user == $last_user){
						if($check_carton['FK_Status_Code'] == 'CCP'){
							$output = array(
								'success' 	=> false,
								'message' 	=> 'Carton No. '.$carton_no.' on Barcode ID '.$barcode['PNH_Picknum'].' was already closed',
								'type' 		=> 'error',
								'title' 	=> 'Error' 
							);
						}
					}
					else{
						if($check_carton['FK_Status_Code'] == 'CCP'){
							$output = array(
								'success' 	=> false,
								'message' 	=> 'Carton No. '.$carton_no.' on Picklist No. '.$barcode['PNH_Picknum'].' was already closed',
								'type' 		=> 'error',
								'title' 	=> 'Error' 
							);
						}
						else{
							$output = array(
								'success' 	=> false,
								'message' 	=> 'Carton No. '.$carton_no.' in Picklist No. '.$barcode['PNH_Picknum'].' is currently used by '.$last_user,
								'type' 		=> 'error',
								'title' 	=> 'Error' 
							);
						}
					}
				}
				else{
					$this->scan_pack_model->on_create_cartonHdr($details, $location, $ir_num, $wsid);
					$this->scan_pack_model->updateWSID($location, $data, $current_user);
				}
			}
			else{
				$output = array(
					'success' 	=> false,
					'message' 	=> 'Barcode does not exist',
					'type' 		=> 'error',
					'title' 	=> 'Error' 
				);
			}
		}


		echo json_encode($output);
	}

	public function getBreakdownDetails(){
		$picklist_no 	= $this->input->post('picklist-no');
		$carton_no 		= $this->input->post('carton-no');
		$location 		= $this->session->userdata('current_user')['login-location'];
		
		$this->load->model('scan_pack/scan_pack_model');

		$picnkote_dtl 	= $this->scan_pack_model->getPNDetails($picklist_no, $location, $carton_no);

		$output['picknote_dtl']  = $picnkote_dtl;

		echo json_encode($output);
	}

	public function getCartonHeader(){
		$picklist_no 	= $this->input->post('picklist-no');
		$location 		= $this->session->userdata('current_user')['login-location'];
		$upc_code 		= $this->input->post('upc-code');
		$loc_source 	= $this->input->post('loc-source');

		$this->load->model('scan_pack/scan_pack_model');

		$output = $this->scan_pack_model->getAllCartonHeaderByPND($location, $loc_source, $upc_code, $picklist_no);

		echo json_encode ($output);
	}

	public function getCartonDetails(){
		$picklist_no 	= $this->input->post('picklist-no');
		$location 		= $this->session->userdata('current_user')['login-location'];
		$carton_no		= $this->input->post('carton-no');

		$this->load->model('scan_pack/scan_pack_model');

		$output = $this->scan_pack_model->getCartonDetailsbyCartonNo($location, $picklist_no, $carton_no);

		echo json_encode($output);
	}

	public function getShowCartonDetails(){
		$picklist_no 	= $this->input->post('picklist-no');
		$location 		= $this->session->userdata('current_user')['login-location'];
		$output 		= array();
		$carton_no 		= '';

		$this->load->model('scan_pack/scan_pack_model');

		$pn_hdr		= $this->scan_pack_model->getPNHeaderbyPicklistNo($picklist_no, $location, $carton_no);
		$ctn_hdr 	= $this->scan_pack_model->getCartonHeaderbyPNH($location, $picklist_no);

		$output['pn_hdr'] 	= $pn_hdr;
		$output['ctn_hdr']	= $ctn_hdr;

		echo json_encode($output);

	}

	public function getPLSearchResults(){
		if (!$this->input->is_ajax_request()) return;
        $type = $_GET['filter-type'];
        $filter = $_GET['filter-search'];
        $action = $_GET['action'];

        $this->load->model('scan_pack/scan_pack_model');

        $data = array();        
        
        $data = $this->scan_pack_model->getAllBySearch($type, $filter);
       
        echo json_encode($data);
	}

	public function filterCarton(){
		$picklist_no 	= $this->input->post('picklist-no');
		$location 		= $this->session->userdata('current_user')['login-location'];
		$from 			= $this->input->post('from');
		$to 			= $this->input->post('to');

		$this->load->model('scan_pack/scan_pack_model');

		if($from == "" && $to == ""){
			$result	= $this->scan_pack_model->getCartonHeaderbyPNH($location, $picklist_no);
		}
		elseif($from != "" && $to != ""){
			$result = $this->scan_pack_model->getFilteredCarton($location, $picklist_no, $from, $to);
		}



		echo json_encode($result);
	}

	public function checkPicklistHasMultipleLoc(){
		$data = $this->input->post('picklist-no');
		$carton_no = $this->input->post('carton-no');
		$location = $this->session->userdata('current_user')['login-location'];

		$this->load->model('scan_pack/scan_pack_model');

		$check_carton 	= $this->scan_pack_model->checkCartonExist($location, $data, $carton_no);
		$current_user 	= $this->session->userdata('current_user')['login-user'];
		$wsid 			= $this->session->userdata('current_user')['login-wsid'];
		$last_user 		= $this->scan_pack_model->getLastUser($location, $data, $carton_no);
		$scn_carton 	= $this->scan_pack_model->checkSCNCarton($location, $data, $carton_no);
		$loc_destin		= $this->scan_pack_model->getLocDestination($this->location, $data);
		$ir_num 		= $this->scan_pack_model->getIRNumberByPickListNo($data, $this->location);
		$details 		= array(
							'picklist-no'  	=> $data,
							'carton-no' 	=> $carton_no,
							'loc-dest' 		=> $loc_destin 
						  );

		if(ALLOW_MULTIPLE_LOC == 0){

			$count_loc = $this->scan_pack_model->countLocation($location, $data);

			$output = array(
				'success' => true,
				'message' => ''
			);
			if(!empty($count_loc)){
				$output = array(
					'success' 	=> false,
					'message' 	=> 'Cannot allow picklist with multiple locations',
					'type' 		=> 'info',
					'title' 	=> 'Information' 
				);
			}
			else{

				if(!empty($check_carton)){
					if($current_user == $last_user){
						if($check_carton['FK_Status_Code'] == 'CCP'){
							$output = array(
								'success' 	=> false,
								'message' 	=> 'Carton No. '.$carton_no.' on Picklist No. '.$data.' was already closed',
								'type' 		=> 'error',
								'title' 	=> 'Error' 
							);
						}
					}
					else{
						if($check_carton['FK_Status_Code'] == 'CCP'){
							$output = array(
								'success' 	=> false,
								'message' 	=> 'Carton No. '.$carton_no.' on Picklist No. '.$data.' was already closed',
								'type' 		=> 'error',
								'title' 	=> 'Error' 
							);
						}
						else{
							$output = array(
								'success' 	=> false,
								'message' 	=> 'Carton No. '.$carton_no.' in Picklist No. '.$data.' is currently used by '.$last_user,
								'type' 		=> 'error',
								'title' 	=> 'Error' 
							);
						}
					}
				}
				else{
					$this->scan_pack_model->on_create_cartonHdr($details, $location, $ir_num, $wsid);
					$this->scan_pack_model->updateWSID($location, $data, $wsid);
				}
			}

		}
		else{
			$output = array(
				'success' => true,
				'message' => ''
			);
			if(!empty($check_carton)){
				if($current_user == $last_user){
					if($check_carton['FK_Status_Code'] == 'CCP'){
						$output = array(
							'success' 	=> false,
							'message' 	=> 'Carton No. '.$carton_no.' on Picklist No. '.$data.' was already closed',
							'type' 		=> 'error',
							'title' 	=> 'Error' 
						);
					}
				}
				else{
					if($check_carton['FK_Status_Code'] == 'CCP'){
						$output = array(
							'success' 	=> false,
							'message' 	=> 'Carton No. '.$carton_no.' on Picklist No. '.$data.' was already closed',
							'type' 		=> 'error',
							'title' 	=> 'Error' 
						);
					}
					else{
						$output = array(
								'success' 	=> false,
								'message' 	=> 'Carton No. '.$carton_no.' in Picklist No. '.$data.' is currently used by '.$last_user,
								'type' 		=> 'error',
								'title' 	=> 'Error' 
							);
					}
				}
			}
			else{
				$this->scan_pack_model->on_create_cartonHdr($details, $location, $ir_num, $wsid);
				$this->scan_pack_model->updateWSID($location, $data, $current_user);
			}
		}


		echo json_encode($output);
	}

	public function checkReqQty(){
		$data = $this->input->post();
		$location = $this->session->userdata('current_user')['login-location'];
		$this->load->model('scan_pack/scan_pack_model');

		$output = array(
				'success' => true,
				'message' => ''
			);
		$total = $this->scan_pack_model->totalReqQty($data['picklist-no'], $data['barcode'], $location, $this->DB);
		
		if($data['status'] == 'Scanning'){
			if($total <= 0){
				$output = array(
					'success' => false,
					'message' => 'Packed Qty cannot be greater than the Required Qty',
					'title'   => 'Error',
					'type'    => 'error' 
				);

			}

			$output['required_qty'] = $this->scan_pack_model->getPNDQtybyBarcode($data['picklist-no'], $data['barcode'], $location, $this->DB);
		}
		else{
			$pack_qty = $this->scan_pack_model->totalScnQty($data['picklist-no'], $data['barcode'], $location, $this->DB);
			
			if(($pack_qty - $data['qty']) < 0){
				$output = array(
					'success' => false,
					'message' => 'Packed Qty cannot be less than zero',
					'title'   => 'Error',
					'type'    => 'error' 
				);

			}

			$output['required_qty'] = $this->scan_pack_model->getPNDQtybyBarcode($data['picklist-no'], $data['barcode'], $location, $this->DB);
		}

		echo json_encode($output);
		
	}

	public function changePNHStatus(){
		$data = $this->input->post();

		$location = $this->session->userdata('current_user')['login-location'];
		$this->load->model('scan_pack/scan_pack_model');

		$carton 			= $this->scan_pack_model->checkCartonExist($location, $data['picklist-no'], $data['carton-no']);
		$ir_num 			= $this->scan_pack_model->getIRNumberByPickListNo($data['picklist-no'], $location);
		$wsid 				= $this->session->userdata('current_user')['login-wsid'];
		$pck_qty 			= $this->scan_pack_model->getCurrentPNDPackedQty($data['picklist-no'], $data['item-code'], $location, $data['loc-source']);
		$pick_qty 			= $this->scan_pack_model->getCurrentPNDPickQty($data['picklist-no'], $data['item-code'], $location, $data['loc-source']);
		$carton_dtl			= $this->scan_pack_model->checkCartonDetailExist($location, $data['picklist-no'], $data['carton-no'], $data['item-code'], $data['loc-source']);
		$pack_qty 			= $this->scan_pack_model->getPackQty($location, $data['picklist-no']);
		$ctn_pack_qty 		= $this->scan_pack_model->getCartonDetailPackedQty($location, $data['picklist-no'], $data['carton-no'], $data['item-code'], $data['loc-source']);
		$ctn_status 		= $this->scan_pack_model->checkSCNCarton($location, $data['picklist-no'], $data['carton-no']);

		$output = array(
				'success' => true,
				'message' => ''
			);

		if(empty($ctn_status)){
			$output = array(
				'success' => false,
				'message' => 'This carton is already closed',
				'title'   => 'Error',
				'type'    => 'error' 
			);
		}
		else{
			if($data['status'] == 'Scanning'){

				if($data['qty'] > ($pick_qty - $pck_qty)){
					$output = array(
						'success' => false,
						'message' => 'Packed Qty cannot be greater than Required Qty',
						'title'   => 'Error',
						'type'    => 'error' 
					);
					$cur_pck_qty 			= $this->scan_pack_model->getCurrentPNDPackedQty($data['picklist-no'], $data['item-code'], $location, $data['loc-source']);
					$cur_pick_qty 			= $this->scan_pack_model->getCurrentPNDPickQty($data['picklist-no'], $data['item-code'], $location, $data['loc-source']);
					
					$output['packed_qty']	= $this->scan_pack_model->getCartonDetailPackedQty($location, $data['picklist-no'], $data['carton-no'], $data['item-code'], $data['loc-source'])['CPD_PackedQty'];
					$output['required_qty'] = ($cur_pick_qty - $cur_pck_qty);
					
				}
				else{
					if(empty($carton)){
						$this->DB->trans_begin();

						$this->scan_pack_model->on_create_cartonDtl($data, $location, $wsid);

						if($this->DB->trans_status() === FALSE){
			                $this->DB->trans_rollback();
			                $output = array(
			                                    'success'=>false
			                                ,   'message'=>$this->db->error()
			                            );
					    }
				        else{
				                $this->DB->trans_commit();
				        }

					}
					else{
						$this->DB->trans_begin();

						$this->scan_pack_model->on_update_cartonHdr($data, $location, $ir_num, $wsid, $this->DB);
						if(!empty($carton_dtl)){
							$this->scan_pack_model->on_update_cartonDtl($data, $location, $wsid, $this->DB);
						}
						else{
							$this->scan_pack_model->on_create_cartonDtl($data, $location, $wsid, $this->DB);
						}

						// Change Status
						$this->DB->where('PNH_Location_Code', $location)->where('PNH_Picknum', $data['picklist-no'])->set('FK_Status_Code', 'SCN')->update('tblPROD_PickNote');


						// Change Picknote Header Total Packed Qty
						$this->DB->where('PNH_Location_Code', $location)
								 ->where('PNH_Picknum', $data['picklist-no'])
								 ->set('PNH_TotalPackedQty', $data['qty'] + $pack_qty)
								 ->set('PNH_TotalDelQty', $data['qty'] + $pack_qty)
								 ->update('tblPROD_PickNote');
						

						// Update Picknote Detail Packed Qty
						$this->DB->where('PKFK_PNH_LocCode', $location)
								 ->where('PKFK_PNH_Picknum', $data['picklist-no'])
								 ->where('PKFK_Location_Source', $data['loc-source'])
								 ->where('PKFK_InvMaster_Code', $data['item-code'])
								 ->set('PND_PackedQty', $pck_qty + $data['qty'])
								 ->update('tblPROD_PickNoteDetail');

						if($this->DB->trans_status() === FALSE){
			                $this->DB->trans_rollback();
			                $output = array(
			                                    'success'=>false
			                                ,   'message'=>$this->db->error()
			                            );
					    }
				        else{
				                $this->DB->trans_commit();
				        }
					}


					// $output['packed_qty'] = $pck_qty + $data['qty'];
					$cur_pck_qty 			= $this->scan_pack_model->getCurrentPNDPackedQty($data['picklist-no'], $data['item-code'], $location, $data['loc-source']);
					$cur_pick_qty 			= $this->scan_pack_model->getCurrentPNDPickQty($data['picklist-no'], $data['item-code'], $location, $data['loc-source']);
					
					$output['packed_qty']	= $this->scan_pack_model->getCartonDetailPackedQty($location, $data['picklist-no'], $data['carton-no'], $data['item-code'], $data['loc-source'])['CPD_PackedQty'];
					$output['required_qty'] = ($cur_pick_qty - $cur_pck_qty);
					
				}

			}
			else{

				if($pck_qty - $data['qty'] < 0){
					$output = array(
						'success' => false,
						'message' => 'Packed Qty cannot be less than zero',
						'title'   => 'Error',
						'type'    => 'error' 
					);
					$cur_pck_qty 			= $this->scan_pack_model->getCurrentPNDPackedQty($data['picklist-no'], $data['item-code'], $location, $data['loc-source']);
					$cur_pick_qty 			= $this->scan_pack_model->getCurrentPNDPickQty($data['picklist-no'], $data['item-code'], $location, $data['loc-source']);
					
					$output['packed_qty']	= $this->scan_pack_model->getCartonDetailPackedQty($location, $data['picklist-no'], $data['carton-no'], $data['item-code'], $data['loc-source'])['CPD_PackedQty'];
					$output['required_qty'] = ($cur_pick_qty - $cur_pck_qty);
					
				}
				else{
					$this->DB->trans_begin();

					$this->scan_pack_model->on_update_cartonHdr($data, $location, $ir_num, $wsid, $this->DB);
					$this->scan_pack_model->on_update_cartonDtl($data, $location, $wsid, $this->DB);


					// Change Status
					$this->DB->where('PNH_Location_Code', $location)->where('PNH_Picknum', $data['picklist-no'])->set('FK_Status_Code', 'SCN')->update('tblPROD_PickNote');

					// Change Picknote Header Total Packed Qty
					$this->DB->where('PNH_Location_Code', $location)
							 ->where('PNH_Picknum', $data['picklist-no'])
							 ->set('PNH_TotalPackedQty', $pack_qty - $data['qty'])
							 ->set('PNH_TotalDelQty', $pack_qty - $data['qty'])
							 ->update('tblPROD_PickNote');
					
					// Update Picknote Detail Packed Qty
					$this->DB->where('PKFK_PNH_LocCode', $location)
							 ->where('PKFK_PNH_Picknum', $data['picklist-no'])
							 ->where('PKFK_Location_Source', $data['loc-source'])
							 ->where('PKFK_InvMaster_Code', $data['item-code'])
							 ->set('PND_PackedQty', $pck_qty - $data['qty'])
							 ->update('tblPROD_PickNoteDetail');


					if($pack_qty <= 0){
						$this->DB->where('PNH_Location_Code', $location)->where('PNH_Picknum', $data['picklist-no'])->set('FK_Status_Code', 'PCK')->update('tblPROD_PickNote');
					}

					if($this->DB->trans_status() === FALSE){
		                $this->DB->trans_rollback();
		                $output = array(
		                                    'success'=>false
		                                ,   'message'=>$this->DB->error()
		                            );
				    }
			        else{
			                $this->DB->trans_commit();
			        }

					$carton_qty 		= $this->scan_pack_model->getCartonPackedQty($data['carton-no'], $data['picklist-no'], $location);
					$carton_dtl_qty		= $this->scan_pack_model->getCartonDetailPackedQty($location, $data['picklist-no'], $data['carton-no'], $data['item-code'], $data['loc-source']);


					if($carton_dtl_qty['CPD_PackedQty'] <= 0){
						$this->DB->trans_begin();
						
						$this->DB->where('PKFK_CPH_Location_Code', $location)
								 ->where('PKFK_PNH_Picknum', $data['picklist-no'])
								 ->where('PKFK_CPH_CartonNo', $data['carton-no'])
								 ->where('PKFK_InvMaster_Code', $data['item-code'])
								 ->where('CPD_Location_Source', $data['loc-source'])
								 ->delete('tblPROD_CartonPackedDetail');

						if($this->DB->trans_status() === FALSE){
			                $this->DB->trans_rollback();
			                $output = array(
			                                    'success'=>false
			                                ,   'message'=>$this->DB->error()
			                            );
					    }
				        else{
				                $this->DB->trans_commit();
				        }
					}

					// $output['packed_qty'] = $pck_qty - $data['qty'];
					$cur_pck_qty 			= $this->scan_pack_model->getCurrentPNDPackedQty($data['picklist-no'], $data['item-code'], $location, $data['loc-source']);
					$cur_pick_qty 			= $this->scan_pack_model->getCurrentPNDPickQty($data['picklist-no'], $data['item-code'], $location, $data['loc-source']);
					
					$output['packed_qty']	= $this->scan_pack_model->getCartonDetailPackedQty($location, $data['picklist-no'], $data['carton-no'], $data['item-code'], $data['loc-source'])['CPD_PackedQty'];
					$output['required_qty'] = ($cur_pick_qty - $cur_pck_qty);
					
				}


			}
		}
 	

		echo json_encode($output);
	}

	public function closeCarton(){

		$data = $this->input->post();
		$location = $this->session->userdata('current_user')['login-location'];
		$user = $this->session->userdata('current_user')['login-user'];

		$this->load->model('scan_pack/scan_pack_model');
		
		$ir_num = $this->scan_pack_model->getIRNumberByPickListNo($data['picklist-no'], $location);
		$carton_hdr = $this->scan_pack_model->getAllCartonHeaderByCartonNo($location, $data['picklist-no'], $data['carton-no']);
		$carton_dtl = $this->scan_pack_model->getAllGroupedCartonDetailByCartonNo($location, $data['picklist-no'], $data['carton-no']);
		
		$output = array(
				'success' => true,
				'message' => ''
			);

		if(empty($carton_hdr)){
			$output = array(
				'success' => false,
				'message' => 'Carton No. '.$data['carton-no'].' in Picklist No. '.$data['picklist-no'].' does not exist in the database',
				'title'   => 'Error',
				'type'    => 'error' 
			);
		}
		else{
			if($carton_hdr['FK_Status_Code'] == 'CCP'){
				$output = array(
					'success' => false,
					'message' => 'Carton No. '.$data['carton-no'].' in Picklist No. '.$data['picklist-no'].' is already closed',
					'title'   => 'Error',
					'type'    => 'error' 
				);
			}
			else{
				if(empty($carton_dtl)){
					$output = array(
						'success' => false,
						'message' => 'No Carton Details for Carton No. '.$data['carton-no'],
						'title'   => 'Error',
						'type'    => 'error' 
					);
				}
				else{

				}
			}
		}

		if($output['success']){

			$this->DB->trans_begin();

			$result = $this->scan_pack_model->on_close_carton($location, $carton_hdr, $carton_dtl, $this->DB);

			if($this->DB->trans_status() === FALSE){
                $this->DB->trans_rollback();
                $output = array(
                                    'success'=>false
                                ,   'message'=>$this->DB->error()
                                ,   'title'=>'Database error'
                                ,   'type'=>'error'
                            );
	        }
	        else{
                $this->DB->trans_commit();
	        }

		}



        echo json_encode($output);
	}

	public function closePL(){
		$picklist_no 	= $this->input->post('picklist-no');
		$location 		= $this->session->userdata('current_user')['login-location'];
		$carton_no		= $this->input->post('carton-no');

		$this->DB->where('FK_PNH_Picknum', $picklist_no)
				 ->where('CPH_PackedQty', 0)
				 ->set('FK_Status_Code', 'CAN')
				 ->update('tblPROD_CartonPacked');

		$output = array(
				'success' => true,
				'message' => ''
			);

		$this->load->model('scan_pack/scan_pack_model');

		$pn_hdr		= $this->scan_pack_model->getPNHeaderbyPicklistNo($picklist_no, $location);
		$pn_dtl 	= $this->scan_pack_model->getPNDetails($picklist_no, $location, $carton_no);
		$exst_ctn 	= $this->scan_pack_model->checkCartonsByPLIfExisting($picklist_no, $location);
		$cartons 	= $this->scan_pack_model->checkAllCartonsIfClosed($picklist_no, $location);

		if($exst_ctn <= 0){
			$output = array(
						'success' => false,
						'message' => 'No existing cartons',
						'title'   => 'Cannot close PL!',
						'type'    => 'error' 
					);
		}
		else{
			if($cartons > 0){
				$output = array(
						'success' => false,
						'message' => 'Close all cartons first before closing PL',
						'title'   => 'Cannot close PL!',
						'type'    => 'error' 
					);
			}
			else{
				$this->DB->trans_begin();

				$this->scan_pack_model->on_close_PL($picklist_no, $pn_hdr, $pn_dtl, $location, $this->DB);

				if($this->DB->trans_status() === FALSE){
		           	$this->DB->trans_rollback();
	               	$output = array(
                                'success'=>false
                            ,   'message'=>$this->DB->error()
                            ,   'title'=>'Database error'
                            ,   'type'=>'error'
                        );
		        }
		        else{
		            $this->DB->trans_commit();
		        }
			}
		}

        echo json_encode($output);

	}

}