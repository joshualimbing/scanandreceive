<?php 

defined('BASEPATH') or exit('No direct script access allowed');

class Item_count extends MAIN_Controller{

	public function index(){
		$data = array();
		$location = $this->session->userdata('current_user')['login-location'];
		$data['wsid'] = $this->session->userdata('current_user')['login-wsid'];

		$this->load_page('item_count/index', $data);

	}

	public function item_entry(){
		$data = array();
		$this->load->model('item_count/item_count_model');

		$data['pcount'] 	= $_GET['pcount'];
		$data['csheet'] 	= $_GET['csheet'];
		$data['actual_loc'] = $this->item_count_model->getCountsheetLoc($this->location, $data['pcount'], $data['csheet'], $this->DB);
		$data['details'] 	= $this->item_count_model->getICDetails($this->location, $data['pcount'], $data['csheet']);
		$data['total_scn']	= $this->item_count_model->getTotalScanQty($this->location, $data['pcount'], $data['csheet']);

		$this->load_page('item_count/form', $data);
	}

	public function getPlanCountNo(){
		if (!$this->input->is_ajax_request()) return;
        $type 	= $_GET['filter-type'];
        $filter = $_GET['filter-search'];

        $this->load->model('item_count/item_count_model');

        $data = array();        
        
        $data = $this->item_count_model->PCountSearchResults($this->location, $filter);
       
        echo json_encode($data);
	}

	public function getCountSheetNo(){
		if (!$this->input->is_ajax_request()) return;
        $type 	= $_GET['filter-type'];
        $filter = $_GET['filter-search'];
        $pcount = $_GET['filter-pcount'];

        $this->load->model('item_count/item_count_model');

        $data = array();        
        
        $data = $this->item_count_model->CSheetSearchResults($this->location, $pcount ,$filter, $this->DB);

        echo json_encode($data);
	}

	public function next(){
		$data 			= $this->input->post();
		$current_user 	= $this->session->userdata('current_user')['login-user'];

		$this->load->model('item_count/item_count_model');

		$last_user 	= $this->item_count_model->getLastUser($this->location, $data['pcount'], $data['csheet']);
		$check_ic 	= $this->item_count_model->checkICDetailsExist($this->location, $data['pcount'], $data['csheet']);

		$output 	= array(
						'success' => true,
						'message' => ''
					  );


		if(!empty($check_ic)){

			if(trim($current_user) != $last_user){
				$output = array(
					'success' 	=> false,
					'message' 	=> 'Countsheet '.$data['csheet'].' is currently used by '.$last_user,
					'title' 	=> 'Information',
					'type' 		=> 'info' 
				);
			}

		}
		else{

			$this->item_count_model->createICDetails($this->location, $data);

		}

		echo json_encode($output);

	}

	public function scanItem(){
		$data = $this->input->post();

		$this->load->model('item_count/item_count_model');

		$this->DB->trans_begin();

		$this->item_count_model->on_update_item_count($this->location, $data, $this->DB);

		if($this->DB->trans_status() === FALSE){
            $this->DB->trans_rollback();
            $output = array(
                                'success'=>false
                            ,   'message'=>$this->DB->error()
                            ,   'title'=>'Database Error'
                            ,   'type'=>'error'
                        );
	    }
        else{
            $this->DB->trans_commit();
        }

        $scan_quantity = $this->item_count_model->getCurrentScanQty($this->location, $data['item-code'], $data['pcount'], $data['csheet'], $this->DB);

        $output = ($scan_quantity == '' ? 0 : $scan_quantity);

        echo json_encode($output);
	}

	public function createICItem(){
		$data = $this->input->post();

		$this->load->model('item_count/item_count_model');

		$check_barcode = $this->item_count_model->checkBarcodeExist($data['upc-code']);

		$output = array(
			'success' => true,
			'message' => ''
		);

		if(empty($check_barcode)){
			$output = array(
				'success'  	=> false,
				'message' 	=> 'Item does not exist',
				'type' 		=> 'error',
				'title' 	=> 'Error'
			);
		}
		else{

			$item_code = $this->item_count_model->getItemCodeByBarcode($data['upc-code']);

			$this->DB->trans_begin();

			$this->item_count_model->on_create_item_count($this->location, $data, $item_code, $this->DB);

			if($this->DB->trans_status() === FALSE){
	            $this->DB->trans_rollback();
	            $output = array(
	                                'success'=>false
	                            ,   'message'=>$this->DB->error()
	                            ,   'title'=>'Database Error'
	                            ,   'type'=>'error'
	                        );
		    }
	        else{
	            $this->DB->trans_commit();
	        }

			$output['item_code'] = $item_code;
			$scan_quantity = $this->item_count_model->getCurrentScanQty($this->location, $item_code, $data['pcount'], $data['csheet'], $this->DB);
			$output['scn_qty'] = ($scan_quantity == '' ? 0 : $scan_quantity);

		}

		echo json_encode($output);
	}

	public function reset(){
		$data = $this->input->post();
		$isNotEqual = 0;

		$this->load->model('item_count/item_count_model');

		$output = array(
			'success' => true,
			'message' => ''
		);

		$ic_dtl = $this->item_count_model->getICDetails($this->location, $data['pcount'], $data['csheet']);

		foreach ($ic_dtl as $key => $detail) {
			if($detail['ScanQty'] != $detail['ActualQty']){
				$isNotEqual += 1;
			}
		}

		if($isNotEqual > 0){

			$this->DB->trans_begin();

			$this->item_count_model->on_reset_item_count($this->location, $data['pcount'], $data['csheet'], $this->DB);
			$this->item_count_model->delete_added_item_count($this->location, $data, $this->DB);

			if($this->DB->trans_status() === FALSE){
	            $this->DB->trans_rollback();
	            $output = array(
	                                'success'=>false
	                            ,   'message'=>$this->DB->error()
	                            ,   'title'=>'Database Error'
	                            ,   'type'=>'error'
	                        );
		    }
	        else{
	                $this->DB->trans_commit();
	        }

			$output['detail'] = $this->item_count_model->getICDetails($this->location, $data['pcount'], $data['csheet']);
			$output['total_scn'] = $this->item_count_model->getTotalScanQty($this->location, $data['pcount'], $data['csheet']);
		}
		else{
			$output = array(
				'success' 	=> false,
				'message' 	=> 'Table has already been reset',
				'title' 	=> 'Information',
				'type'  	=> 'info' 	
			);
		}

		echo json_encode($output);

	}

	public function saveCountSheet(){
		$data = $this->input->post();

		$this->load->model('item_count/item_count_model');

		$output = array(
			'success' => true,
			'message' => '' 
		);

		$ic_dtl 	= $this->item_count_model->getICDetails($this->location, $data['pcount'], $data['csheet']);
		$total_scn 	= $this->item_count_model->getTotalScanQty($this->location, $data['pcount'], $data['csheet']);

		if($total_scn <= 0){
			$output = array(
				'success' 	=> false,
				'message' 	=> 'No items to be posted',
				'title' 	=> 'Information',
				'type'  	=> 'info' 	
			);
		}
		else{

			$this->DB->trans_begin();

			$this->item_count_model->on_save_item_count($this->location, $data, $ic_dtl, $total_scn, $this->DB);

			if($this->DB->trans_status() === FALSE){
	            $this->DB->trans_rollback();
	            $output = array(
	                                'success'=>false
	                            ,   'message'=>$this->DB->error()
	                            ,   'title'=>'Database Error'
	                            ,   'type'=>'error'
	                        );
		    }
	        else{
	                $this->DB->trans_commit();
	        }
		}

		echo json_encode($output);

	}

}