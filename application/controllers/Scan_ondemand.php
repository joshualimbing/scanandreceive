<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Scan_ondemand extends MAIN_Controller
{

	
	public function index(){
		
		$data = array();
		$this->load->model('scan_ondemand/scan_ondemand_model');

		$loc_type = $this->scan_ondemand_model->getLocType($this->location);

		$data['wsid'] 				= $this->wsid;
		$data['dr_list'] 			= $this->scan_ondemand_model->getAllAvailableDR($this->location, $this->DB);
		$data['location_list']  	= $this->scan_ondemand_model->getLocationList($this->location);
		$data['user'] 				= trim($_SESSION['current_user']['login-user']);

		$this->load_page('scan_ondemand/index', $data);
	}

	public function item_entry(){
		$data = array();
		$this->load->model('scan_ondemand/scan_ondemand_model');

		$data['total_scn'] 			= $this->scan_ondemand_model->getTotalScanned($_GET['DRNo'], $this->DB);
		$data['details'] 			= $this->scan_ondemand_model->getDeliverDetails($_GET['DRNo'], $this->DB);
		// $data['location'] 		= $this->location;
		$data['loc_type'] 		= $this->scan_ondemand_model->getLocType($this->location);
		$data['bin_list']		= $this->scan_ondemand_model->getAllBinLocation();

		$this->load_page('scan_ondemand/form', $data);
	}

	public function searchDR(){
		$filter = $this->input->get('docno');
		$this->load->model('scan_ondemand/scan_ondemand_model');

		$output 			= $this->scan_ondemand_model->getPostedDR($this->location, $filter, $this->DB);

		echo json_encode($output);
	}

	public function getDetailsbyDR(){
		$data = $this->input->post();
		$this->load->model('scan_ondemand/scan_ondemand_model');

		$output = $this->scan_ondemand_model->getPostedDRDetails($data['dr-docno'], $this->DB);

		echo json_encode($output);
	}

	public function reprocess(){
		$data = $this->input->post('dr-docno');
		$this->load->model('scan_ondemand/scan_ondemand_model');

		$countPosted = $this->scan_ondemand_model->checkIfPosted($this->location, $data, $this->DB);

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		if($countPosted > 0){
			$output = array(
				'success'  	=> false,
				'message'  	=> 'DR is already Posted',
				'title'  	=> 'Information',
				'type'  	=> 'info' 
			);
		}
		else{

			$details = $this->scan_ondemand_model->getReprocessDetails($this->location, $data, $this->DB);

			$this->DB->trans_begin();

			$this->scan_ondemand_model->on_reprocess_DR($this->location, $data, $details, $this->DB);

			if($this->DB->trans_status() !== False){
				$this->DB->trans_commit();
			}
			else{
				$this->DB->trans_rollback();
			}
		}

		echo json_encode($output);

	}

	public function getHeaderData(){

		$doc_no = $this->input->post('dr-number');

		$this->DB->select('ODH_Remarks, PKFK_Location_Destin');
		$this->DB->where('ODH_DocNo', $doc_no);

		$query = $this->DB->get_compiled_select('tblPROD_ScanOndemandHeader');

		$result = $this->DB->query($query)->row_array();

		echo json_encode($result);

	}

	public function next(){

		$data = $this->input->post();
		$this->load->model('scan_ondemand/scan_ondemand_model');

		$output = array(
			'success'  => true,
			'message'  => ''
		);

		try {

			if($data['dr-number'] == 'NEW DR'){

				$loop_count = 1;
				$dr_docno = '';

				for($i=1; $i<=$loop_count; $i++){

					$docno =  $this->scan_ondemand_model->CreateODH_DocNo($this->location);
					$checkExist = $this->scan_ondemand_model->checkIfExisting($this->location, 'DROD-'.$dr_docno, $this->DB);

					if(!empty($checkExist)){
						$loop_count++;
					}
					else{
						$dr_docno = $docno;
						break;
					}
				}


				$this->DB->trans_begin();

				$this->scan_ondemand_model->on_create_DRODHeader($this->location, 'DROD-'.$dr_docno, $data['del-location'], $data['remarks'], $this->DB);

				if($this->DB->trans_status() !== False){
					$this->DB->trans_commit();
				}
				else{
					$this->DB->trans_rollback();
				}

				$output['dr_docno'] = 'DROD-'.$dr_docno;
			}
			else{

				$checkClose = $this->scan_ondemand_model->checkIfClosed($this->location, $data['dr-number'], $this->DB);

				if(!empty($checkClose)){
					throw new Exception("Document was already posted");
				}

				$output['dr_docno'] = $data['dr-number'];
			}
			
		} 
		catch (Exception $e) {
			$output = array(
				'success'  => true,
				'message'  => $e->getMessage(),
				'title'    => 'Error',
				'type'     => 'error'
			);
		}

		echo json_encode($output);


	}

	public function updateLocationSource(){
		$data = $this->input->post();

		$this->load->model('scan_ondemand/scan_ondemand_model');

		$this->scan_ondemand_model->updateLocSource($data, $this->DB);

	}

	public function checkAndgetBarcodeDetails(){
		$barcode 		= $this->input->post('upc-code');
		$scan_qty 		= $this->input->post('qty');
		$doc_no			= $this->input->post('doc-no');
		$del_location	= $this->input->post('del-location');
		$status 		= $this->input->post('status');

		$this->load->model('scan_ondemand/scan_ondemand_model');

		$check_barcode = $this->scan_ondemand_model->checkBarcodeifExisting($barcode, $this->DB);

		$output = array(
			'success' => true,
			'message' => ''
		);

		if(empty($check_barcode)){
			$output = array(
				'success' 	=> false,
				'message' 	=> 'Item does not exist',
				'title'	 	=> 'Information',
				'type' 		=> 'info' 	
			);
		}
		else{
			$item_code	= $this->scan_ondemand_model->getItemCodeByBarcode($barcode);
			$item_desc	= $this->scan_ondemand_model->getItemDescByBarcode($barcode);
			$loc_source = $this->scan_ondemand_model->getDefaultLocSource($item_code);
			$sr_barcode	= $this->scan_ondemand_model->checkifODDItemExists($doc_no, $item_code, $this->DB);
			$loc_type 	= $this->scan_ondemand_model->getLocType($this->location);

			if(empty($loc_source)){
				if($loc_type == 'STR'){
					$loc_source = $this->location;
				}
				else{
					$loc_source = '';
				}
			}

				
			$temp_del_dtl = array(
				'ODD_DocNo' 			=> $doc_no,
				'ODD_Location_Source' 	=> $loc_source,
				'PKFK_InvMaster_Code' 	=> $item_code,
				'ODD_ScanQty' 			=> ($status == "Scanning" ? $scan_qty : ($scan_qty * -1))
			);



			if(!empty($sr_barcode)){
				$this->DB->trans_begin();
				
				$this->scan_ondemand_model->on_update($item_code, $doc_no, ($status == "Scanning" ? $scan_qty : ($scan_qty * -1)),  $this->DB);

				if($this->DB->trans_status() === FALSE){
		            $this->DB->trans_rollback();
		            $output = array(
		                                'success'=>false
		                            ,   'message'=>$this->db->error()
		                        );
		            $barcode 		= '';
			    }
		        else{
		                $this->DB->trans_commit();
		        }
			}
			else{
				if($status == 'Scanning'){
					$this->DB->trans_begin();

					$this->scan_ondemand_model->on_save($temp_del_dtl, $this->DB);

					if($this->DB->trans_status() === FALSE){
			            $this->DB->trans_rollback();
			            $output = array(
			                                'success'=>false
			                            ,   'message'=>$this->db->error()
			                        );
			            $barcode 		= '';
				    }
			        else{
			                $this->DB->trans_commit();
			        }
				}
			}



			$output['item_code'] = $item_code;
			$output['loc_source'] = $loc_source;
			$output['item_desc'] = $item_desc;
			$scan_quantity = $this->scan_ondemand_model->getCurrentScanQty($item_code, $doc_no, $this->DB);
			$output['scn_qty'] 	= ($scan_quantity == '' ? 0 : $scan_quantity);
			// $barcode 		= '';
		}

		echo json_encode($output);

	}

	public function postDeliver(){
		$header = $this->input->post();
		$this->load->model('scan_ondemand/scan_ondemand_model');

		$details = $this->scan_ondemand_model->getDeliverDetails($header['doc-no'], $this->DB);
		$output = array(
			'success'	=> true,
			'message' 	=> '' 
		);

		if(empty($details)){
			$output = array(
				'success'	=> false,
				'message' 	=> 'No details',
				'type' 		=> 'info',
				'title' 	=> 'information'
			);
		}
		else{
			$remarks = $this->scan_ondemand_model->getRemarks($header['doc-no'], $this->DB);
			
			$this->DB->trans_begin();

			$this->scan_ondemand_model->on_post_deliver($this->location, $header, $remarks, $details, $this->DB);

			$this->DB->where('ODH_DocNo', $header['doc-no'])
					 ->set('ODH_Status_Code', 'CCP')
					 ->update('tblPROD_ScanOndemandHeader');

			if($this->DB->trans_status() === FALSE){
	            $this->DB->trans_rollback();
	            $output = array(
	                                'success'	=> false
	                            ,   'message'	=> 'Contact your System Administrator'
	                            ,   'title'		=> 'Error'
	                            ,   'type'		=> 'error'
	                        );
		    }
	        else{
	            $this->DB->trans_commit();
	        }
		}

		echo json_encode($output);

	}

	public function cancelDR(){
		$doc_no = $this->input->post('doc-no');

		$output = array(
			'success'	=> true,
			'message' 	=> '' 
		);

		$this->DB->trans_begin();

		$this->DB->where('ODH_DocNo', $doc_no)
				 ->set('ODH_Status_Code', 'CAN')
				 ->update('tblPROD_ScanOndemandHeader');

		if($this->DB->trans_status() === FALSE){
            $this->DB->trans_rollback();
            $output = array(
                                'success'	=> false
                            ,   'message'	=> 'Contact your IT Administrator'
                            ,   'title'		=> 'Error'
                            ,   'type'		=> 'error'
                        );
	    }
        else{
            $this->DB->trans_commit();
        }

        echo json_encode($output);

	}

}