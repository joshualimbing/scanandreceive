<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Encrypt\SGEncryption;

class User extends MAIN_Controller
{


	public function __construct(){
		parent::__construct();
	}
	
	public function index(){

		$this->load->model('user_model');

		$data = array();

		if(!$this->sess->isLogin()){
			$data['company'] = $this->user_model->getAllCompany();

			$this->load->view('user/login', $data);
		}
		else{
			$this->redirect('user/menu');
		}

	}

	public function menu(){

		$data = array();

		$this->load_page('user/menu', $data);
		
	}

	public function getCompanyDB(){

		$company_id = $this->input->post('company-id');

		$this->load->model('user_model');

		$DB_detail = $this->user_model->getDBDetailsByCompanyId($company_id);

		$current_db = array(
			'dsn'	=> '',
			'hostname' => $DB_detail['C_Default_BOServer'],
			'username' => $DB_detail['C_Default_BOUser'],
			'password' => $DB_detail['C_Default_BOPassword'],
			'database' => $DB_detail['C_Default_BODB'],
			'dbdriver' => 'sqlsrv',
			'dbprefix' => '',
			'pconnect' => FALSE,
			'db_debug' => (ENVIRONMENT !== 'production'),
			'cache_on' => FALSE,
			'cachedir' => '',
			'char_set' => 'utf8',
			'dbcollat' => 'utf8_general_ci',
			'swap_pre' => '',
			'encrypt' => FALSE,
			'compress' => FALSE,
			'stricton' => FALSE,
			'failover' => array(),
			'save_queries' => TRUE
		);

		$this->active_db = $current_db;

		$location_list = $this->user_model->getLocationbyCompany($this->active_db, $company_id);

		echo json_encode($location_list);

	}

	public function login(){

		$data = $this->input->post();

		$encryptor = new SGEncryption();
		$this->load->model('user_model');

		// $this->print_r($disabled);
		// die();

		if($data['company-id'] == "" || $data['username'] == "" || $data['password'] == "" || $data['location'] == ""){
			$output = array(
				'success' 		=> false,
				'message' 		=> 'Please fill out all the important fields',
				'title' 		=> 'Error',
				'type' 			=> 'error' 	
			);
		}
		else{

			$DB_detail = $this->user_model->getDBDetailsByCompanyId($data['company-id']);

			$current_db = array(
				'dsn'	=> '',
				'hostname' => $DB_detail['C_Default_BOServer'],
				'username' => $DB_detail['C_Default_BOUser'],
				'password' => $DB_detail['C_Default_BOPassword'],
				'database' => $DB_detail['C_Default_BODB'],
				'dbdriver' => 'sqlsrv',
				'dbprefix' => '',
				'pconnect' => FALSE,
				'db_debug' => (ENVIRONMENT !== 'production'),
				'cache_on' => FALSE,
				'cachedir' => '',
				'char_set' => 'utf8',
				'dbcollat' => 'utf8_general_ci',
				'swap_pre' => '',
				'encrypt'  => FALSE,
				'compress' => FALSE,
				'stricton' => FALSE,
				'failover' => array(),
				'save_queries' => TRUE
			);

			$output = array(
				'success' => true,
				'message' => ''
			);

			$user = $this->user_model->checkUsernameAndPassword($current_db, $data['username'], $encryptor->Encrypt($data['password']));
			$disabled = $this->user_model->checkDisabled($current_db, $data['username'], $encryptor->Encrypt($data['password']));

			if(empty($user)){
				$output = array(
					'success' 		=> false,
					'message' 		=> 'Invalid Username/Password',
					'title' 		=> 'Error',
					'type' 			=> 'error' 	
				);
			}
			else{
				if($disabled){
					$output = array(
						'success' 		=> false,
						'message' 		=> 'User is disabled',
						'title' 		=> 'Error',
						'type' 			=> 'error' 	
					);
				}
				else{

					$this->session->unset_userdata('alert');
					
					$data = array(	
									'user' 						=> $user['U_UserID'],
									'username'					=> $user['U_Name'],
									'company'					=> $data['company-id'],
									'location'					=> $data['location'],
									'loc-mask' 					=> $this->user_model->getLocMask($current_db, $data['location']),
									'workstation-id' 			=> $this->getMacAddress(),
									'database' 					=> $current_db 
							);			
					$this->sess->setLogin($data);
				}
			}
		}

		echo json_encode($output);

	}

	public function logout(){
		$this->sess->clearLogin();
		$this->redirect(DOMAIN);
	}

	public function getIPAddress(){

	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
		
	}

	public function getMacAddress(){
		$ipAddress=$this->getIPAddress();
		$macAddr=false;

		if($ipAddress == "::1"){
			$macAddr = $this->getSelfMacAddress();
		}
		else{
			#run the external command, break output into lines
			$arp=`arp -a $ipAddress`;
			$lines=explode("\n", $arp);

			#look for the output line describing our IP address
			foreach($lines as $line)
			{
			   $cols=preg_split('/\s+/', trim($line));
			   if ($cols[0]==$ipAddress)
			   {
			       $macAddr=$cols[1];
			   }
			}			
		}


		return $macAddr;
	}

	public function getSelfMacAddress(){
		ob_start();
	    system('ipconfig/all');
	    $mycom=ob_get_contents(); 
	    ob_clean(); 
	    $findme = "Physical";
	    $pmac = strpos($mycom, $findme); 
	    $mac=substr($mycom,($pmac+36),17);

	    return $mac;
	}

	public function checkSession(){
		$data = $this->input->post();
		

		$result = true;
		
		if($data['timer'] == 600000){
			unset($_SESSION['current_user']);
		}
		else{
			$result = false;
		}

		echo json_encode($result);
	}


}