<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Scan_received extends MAIN_Controller
{

	
	public function index(){
		
		$data = array();
		$this->load->model('scan_received/scan_received_model');

		$loc_type = $this->scan_received_model->getLocType($this->location);

		$data['wsid'] = $this->wsid;
		$data['transtype_list'] 	= $this->scan_received_model->getTransType($this->location, $loc_type);
		$data['location_list']  	= $this->scan_received_model->getLocationList();
		$this->load_page('scan_received/index', $data);
	}

	public function item_entry(){
		$data = array();
		$this->load->model('scan_received/scan_received_model');

		$data['total_recv'] 	= $this->scan_received_model->getTotalReceived($this->location, $_GET['pack_no'], $_GET['ref']);
		$data['details'] 		= $this->scan_received_model->getReceivingDetails($this->location, $_GET['recvtype'], $_GET['ref'], $_GET['pack_no']);
		$data['location'] 		= $this->location;
		$data['loc_type'] 		= $this->scan_received_model->getLocType($this->location);
		$data['bin_list']		= $this->scan_received_model->getAllBinLocation();

		$this->load_page('scan_received/form', $data);
	}

	public function getReferenceFrom(){
		if (!$this->input->is_ajax_request()) return;
        $type = $_GET['filter-type'];
        $filter = $_GET['filter-search'];
        $action = $_GET['action'];
        $recv_type = $_GET['recv-type'];

        $this->load->model('scan_received/scan_received_model');

        $data = array();        
        
        $data = $this->scan_received_model->LocCodeSearchResults($type, $filter, $recv_type);
       
        echo json_encode($data);
	}

	public function updateLocationSource(){
		$data = $this->input->post();

		$this->load->model('scan_received/scan_received_model');

		$this->scan_received_model->updateLocSource($data, $this->location);

	}

	public function checkAndgetBarcodeDetails(){
		$barcode 	= $this->input->post('upc-code');
		$ref_doc 	= $this->input->post('ref-doc');
		$remarks	= $this->input->post('remarks');
		$scan_qty 	= $this->input->post('qty');
		$recv_from	= $this->input->post('recv-from');
		$recv_type	= $this->input->post('recv-type');
		$pack_no	= $this->input->post('pack-no');
		$status 	= $this->input->post('status');
		$wsid 		= $this->session->userdata('current_user')['login-wsid'];

		$this->load->model('scan_received/scan_received_model');

		$check_barcode = $this->scan_received_model->checkBarcodeifExisting($barcode);

		$output = array(
			'success' => true,
			'message' => ''
		);

		if(empty($check_barcode)){
			$output = array(
				'success' 	=> false,
				'message' 	=> 'Item does not exist',
				'title'	 	=> 'Information',
				'type' 		=> 'info' 	
			);
		}
		else{
			$item_code	= $this->scan_received_model->getItemCodeByBarcode($barcode);
			$loc_source = $this->scan_received_model->getDefaultLocSource($item_code);
			$sr_barcode	= $this->scan_received_model->checkifSRBarcodeExists($this->location, $barcode, $pack_no, $ref_doc);
			$loc_type 	= $this->scan_received_model->getLocType($this->location);

			if(empty($loc_source)){
				if($loc_type == 'STR'){
					$loc_source = $this->location;
				}
				else{
					$loc_source = null;
				}
			}

				
			$temp_rcv_dtl = array(
				'PKFK_Location_Code' 	=> $this->location,
				'Location_Source' 		=> $loc_source,
				'PK_WSID' 				=> $wsid,
				'PKFK_InvMaster_Code' 	=> $item_code,
				'SI_UPCCode' 			=> $barcode,
				'SI_ScanQty' 			=> ($status == "Scanning" ? $scan_qty : ($scan_qty * -1)),
				'Doc_Reference'  		=> $ref_doc,
				'Doc_Remarks' 			=> $remarks,
				'Doc_ReceivingType' 	=> $recv_type,
				'Doc_ReceivedFrom' 		=> $recv_from,
				'Pack_No' 				=> $pack_no 
			);


			$this->DB->trans_begin();

			if(!empty($sr_barcode)){
				$this->scan_received_model->on_update($this->location, $item_code, $ref_doc, $recv_type, $recv_from, $pack_no, ($status == "Scanning" ? $scan_qty : ($scan_qty * -1)), $this->DB);
			}
			else{
				if($status == 'Scanning'){
					$this->scan_received_model->on_save($temp_rcv_dtl, $this->DB);
				}
			}

			if($this->DB->trans_status() === FALSE){
	            $this->DB->trans_rollback();
	            $output = array(
	                                'success'=>false
	                            ,   'message'=>$this->DB->error()
	                            ,   'title'=>'Database Error'
	                            ,   'type'=>'error'
	                        );
		    }
	        else{
	            $this->DB->trans_commit();
	        }

			$output['item_code'] = $item_code;
			$output['loc_source'] = $loc_source;
			$scan_quantity = $this->scan_received_model->getCurrentScanQty($this->location, $item_code, $ref_doc, $recv_type, $recv_from, $pack_no);
			$output['scn_qty'] = $scan_quantity;

		}

		echo json_encode($output);

	}

	public function postReceived(){
		$header = $this->input->post();
		$this->load->model('scan_received/scan_received_model');

		$details = $this->scan_received_model->getAllReceivingDetails($this->location, $header['pack-no'], $header['ref-doc']);

		$output = array(
			'success'	=> true,
			'message' 	=> '' 
		);

		if(empty($details)){
			$output = array(
				'success'	=> false,
				'message' 	=> 'No details',
				'type' 		=> 'info',
				'title' 	=> 'information'
			);
		}
		else{
			$this->DB->trans_begin();

			$this->scan_received_model->on_post_received($this->location, $header, $details, $this->DB);

			if($this->DB->trans_status() === FALSE){
	            $this->DB->trans_rollback();
	            $output = array(
	                                'success'=>false
	                            ,   'message'=>$this->DB->error()
	                            ,   'title'=>'Database Error'
	                            ,   'type'=>'error'
	                        );
		    }
	        else{
	                $this->DB->trans_commit();
	        }
		}

		echo json_encode($output);

	}

}