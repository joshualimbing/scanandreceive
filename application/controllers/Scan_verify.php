<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Scan_verify extends MAIN_Controller
{
	
	public function index(){
		$data = array();

		$this->load->model('scan_verify/scan_verify_model');
		$data['loc_type'] 		= $this->scan_verify_model->getLocType($this->location);
		$data['transtype'] 		= $this->scan_verify_model->getTransType($this->location, $data['loc_type']);
		$data['location_list']	= $this->scan_verify_model->getLocationList($this->location);
		$data['wsid'] 			= $this->wsid;

		$this->load_page('scan_verify/index', $data);
	}

	public function item_entry(){
		$data = array();

		$this->load->model('scan_verify/scan_verify_model');
		$data['loc_type'] 		= $this->scan_verify_model->getLocType($this->location);
		$data['doc_type']		= $_GET['doc_type'];
		$data['location']		= $this->location;
		$data['details']		= $this->scan_verify_model->getAllDetails($this->location, $_GET['doc_type'], $_GET['recv_from'], $_GET['ref_no'], $this->DB);
		$data['total_req']		= $this->scan_verify_model->getTotalRequiredQty($this->location, $_GET['doc_type'], $_GET['recv_from'], $_GET['ref_no']);	
		$data['total_scn']		= $this->scan_verify_model->getTotalScannedQty($this->location, $_GET['doc_type'], $_GET['recv_from'], $_GET['ref_no']);
		$data['bin_list']		= $this->scan_verify_model->getAllBinLocation();		


		$this->load_page('scan_verify/form', $data);
	}

	public function extractFile(){

		$this->load->model('scan_verify/scan_verify_model');

		$name = $_FILES['file']['name'];
		$filename_arr = explode('.', $_FILES['file']['name']);
		$filename = $filename_arr[0];
		$tmp_name = $_FILES['file']['tmp_name'];
		move_uploaded_file($tmp_name, 'upload/'.$name);

		$unzip = new ZipArchive();
		$out = $unzip->open('upload/'.$name);
		if ($out === TRUE) {
			$unzip->extractTo('upload');
			$unzip->close();
			
			$file = "upload/".$filename.".txt";
			$dest = "extracted/".$filename.".txt";

			if(file_exists($file)){

			    $fopen = fopen($file, 'r');

			    $fread = fread($fopen,filesize($file));

			    fclose($fopen);

			    $remove = "\n";

			    $split = explode($remove, $fread);

			    $array[] = null;
			    $tab = "\t";

			    foreach ($split as $string)
			    {
			        $row = explode($tab, $string);
			        array_push($array,$row);
			    }
			    

			    $sv_dtl 		= array();
			    $assoc_sv_arr 	= array();
			    $sv_hdr 		= array(); 


			    foreach ($array as $key => $value) {
			    	if(!empty($array)){
			    		if(!empty($array[$key][0])){
			    			$text = $array[$key][0];
			    			$item_code 	= substr($text, 140, 30);
			    			$upc_code 	= substr($text, 627, 30);
			    			$item_desc 	= substr($text, 450, 100);
			    			$req_qty 	= substr($text, 170, 5);
			    			$ref_no 	= substr($text, 80, 30);
			    			$recv_from 	= substr($text, 110, 30);
			    			$loc_code 	= substr($text, 0, 30);
			    			$doc_type 	= (substr($ref_no, 0, 2) == 'PL' ? 'PUL' : (substr($ref_no, 0, 2) == 'DR' ? 'DEL' : (substr($ref_no, 0, 2) == 'ST' ? 'STF' : '') ));

			    			$assoc_sv_arr['FK_InvMaster_Code'] 		= $item_code;
			    			$assoc_sv_arr['IM_UPCCode'] 			= $upc_code;
			    			$assoc_sv_arr['PKFK_Item_Desc'] 		= $item_desc;
			    			$assoc_sv_arr['RequiredQty'] 			= trim($req_qty);
			    			$assoc_sv_arr['ScanQty'] 				= 0;
			    			$assoc_sv_arr['ReferenceNo'] 			= $ref_no;
			    			$assoc_sv_arr['ReceivedFrom'] 			= $recv_from;
			    			$assoc_sv_arr['Loc_Source'] 			= $loc_code;
			    			$assoc_sv_arr['Doc_ReceivingType'] 		= $doc_type;

			    			array_push($sv_dtl, $assoc_sv_arr);
			    		}
			    	}
			    }

			    foreach ($sv_dtl as $key => $value) {
			    	$sv_hdr['doc-type'] 	= trim($value['Doc_ReceivingType']);
			    	$sv_hdr['ref-no']		= trim($value['ReferenceNo']);
			    	$sv_hdr['recv-from']	= trim($value['ReceivedFrom']);
			    	$sv_hdr['loc-code']		= trim($value['Loc_Source']);
			    	$sv_hdr['remarks']		= '';
			    	break;	
			    }

			    if($sv_hdr['loc-code'] == trim($this->location)){
			    	// STR Loc Type
						
					$check_pul = $this->scan_verify_model->checkReceived($sv_hdr['ref-no'], $sv_hdr['recv-from']);

					if(!empty($check_pul)){
						$this->set_error_message('Information', 'This Document was already received', 'info');
						header('Location: '.DOMAIN.'scan_verify');
					}
					else{
						$check_scan = $this->scan_verify_model->checkScanVerify($this->location, $sv_hdr['ref-no'], $sv_hdr['recv-from'], $sv_hdr['doc-type']);

						if(empty($check_scan)){

							if(empty($sv_dtl)){
								$this->set_error_message('Error', 'No Details found', 'error');
								header('Location: '.DOMAIN.'scan_verify');
							}
							else{
								$this->scan_verify_model->on_save_scan_verify($sv_dtl, $this->location, $sv_hdr);
								move_uploaded_file($file, $dest);
							    unlink('upload/'.$name);
								header('Location: '.DOMAIN.'scan_verify/item_entry?doc_type='.$sv_hdr['doc-type'].'&recv_from='.$sv_hdr['recv-from'].'&ref_no='.$sv_hdr['ref-no'].'&remarks='.$sv_hdr['remarks']);
							}


						}
						else{
							header('Location: '.DOMAIN.'scan_verify/item_entry?doc_type='.$sv_hdr['doc-type'].'&recv_from='.$sv_hdr['recv-from'].'&ref_no='.$sv_hdr['ref-no'].'&remarks='.$sv_hdr['remarks']);
						}
					}

					// End


			    }
			    else{
			    	$this->set_error_message('Error', 'This Document is not for this location', 'error');
			    	header('Location: '.DOMAIN.'scan_verify');
			    }

			}
			else{
				$this->set_error_message('Information', 'File does not exist', 'info');
				header('Location: '.DOMAIN.'scan_verify');
			}

		} else {
			$this->set_error_message('Error', 'Unzipping file failed', 'error');
		  	header('Location: '.DOMAIN.'scan_verify');
		}


	}

	public function getReferenceNo(){
		if (!$this->input->is_ajax_request()) return;
        $filter 	= $_GET['filter-search'];
        $doc_type 	= $_GET['filter-doctype'];
        $recv_from 	= $_GET['filter-recvfrom'];
        $action 	= $_GET['action'];

        $this->load->model('scan_verify/scan_verify_model');

        $data = array();        
        
        $data = $this->scan_verify_model->RefNoSearchResults($filter, $doc_type, $recv_from);
       
        echo json_encode($data);
	}

	public function getRecvFrom(){
		$doc_type = $this->input->post()['doc-type']; 

		$this->load->model('scan_verify/scan_verify_model');

		if($doc_type == 'POR'){
			$output = $this->scan_verify_model->getSupplierList();
		}
		else{
			$output = $this->scan_verify_model->getLocationList($this->location);
		}

		echo json_encode($output);
	}

	public function next(){
		$data = $this->input->post();

		$this->load->model('scan_verify/scan_verify_model');

		$output = array(
			'success' => true,
			'message' => ''
		);

		// PUL Doc Type

		if($data['doc-type'] == 'PUL'){
			$check_pul = $this->scan_verify_model->checkReceived($data['ref-no'], $data['recv-from']);

			if(!empty($check_pul)){
				$output = array(
					'success' 	=> false,
					'message' 	=> $data['ref-no'].' from '.$data['recv-from'].' was already received.',
					'title' 	=> 'Information',
					'type'  	=> 'info' 	
				);
			}
			else{
				$check_scan = $this->scan_verify_model->checkScanVerify($this->location, $data['ref-no'], $data['recv-from'], $data['doc-type']);

				if(empty($check_scan)){

					$detail = $this->scan_verify_model->getAllDetailsByDocType($data['doc-type'], $data['recv-from'], $data['ref-no'], $this->loc_mask);

					if(empty($detail)){
						$output = array(
							'success' 	=> false,
							'message' 	=> 'No Pullout details',
							'title' 	=> 'Information',
							'type'  	=> 'info' 	
						);
					}
					else{
						$this->scan_verify_model->on_save_scan_verify($detail, $this->location, $data);
					}


				}
			}
		}

		elseif($data['doc-type'] == 'POR'){
			
			$check_scan = $this->scan_verify_model->checkScanVerify($this->location, $data['ref-no'], $data['recv-from'], $data['doc-type']);

			if(empty($check_scan)){

				$detail = $this->scan_verify_model->getAllDetailsByDocType($data['doc-type'], $data['recv-from'], $data['ref-no'], $this->loc_mask);

				if(empty($detail)){
					$output = array(
						'success' 	=> false,
						'message' 	=> 'No Purchase Order details',
						'title' 	=> 'Information',
						'type'  	=> 'info' 	
					);
				}
				else{
					$this->scan_verify_model->on_save_scan_verify($detail, $this->location, $data);
				}


			}
		}

		// End

		echo json_encode($output);
	}

	public function scanItem(){
		$data = $this->input->post();

		$this->load->model('scan_verify/scan_verify_model');

		$this->DB->trans_begin();

		$this->scan_verify_model->on_update_scan_verify($this->location, $data, $this->DB);

		if($this->DB->trans_status() === FALSE){
            $this->DB->trans_rollback();
            $output = array(
                                'success'=>false
                            ,   'message'=>$this->DB->error()
                        );
	    }
        else{
                $this->DB->trans_commit();
        }

        $scan_quantity = $this->scan_verify_model->getCurrentScanQty($this->location, $data['item-code'], $data['ref-no'], $data['recv-from']);

        $output['scn_qty'] = ($scan_quantity == '' ? 0 : $scan_quantity);

        echo json_encode($output['scn_qty']);
		
	}

	public function updateLocationSource(){
		$data = $this->input->post();

		$this->load->model('scan_verify/scan_verify_model');

		$this->scan_verify_model->updateLocSource($data, $this->location);

	}

	public function CreateSVItem(){
		$data = $this->input->post();

		$this->load->model('scan_verify/scan_verify_model');

		$check_barcode = $this->scan_verify_model->checkBarcodeExist($data['upc-code']);

		$output = array(
			'success' => true,
			'message' => ''
		);

		if(empty($check_barcode)){
			$output = array(
				'success'  	=> false,
				'message' 	=> 'Item does not exist',
				'type' 		=> 'error',
				'title' 	=> 'Error'
			);
		}
		else{

			$item_code = $this->scan_verify_model->getItemCodeByBarcode($data['upc-code']);

			$this->DB->trans_begin();

			$this->scan_verify_model->on_create_scan_verify($this->location, $data, $item_code, $this->loc_mask, $this->DB);

			if($this->DB->trans_status() === FALSE){
	            $this->DB->trans_rollback();
	            $output = array(
	                                'success'=>false
	                            ,   'message'=>$this->DB->error()
	                        );
		    }
	        else{
	                $this->DB->trans_commit();
	        }

			$item_loc = $this->scan_verify_model->getItemActualLocation($item_code);

			$output['item_code'] 	= $item_code;
			$output['item_loc'] 	= $item_loc;
			$scan_quantity = $this->scan_verify_model->getCurrentScanQty($this->location, $item_code, $data['ref-no'], $data['recv-from']);

        	$output['scn_qty'] = ($scan_quantity == '' ? 0 : $scan_quantity);
		}


		echo json_encode($output);
	}

	public function reset(){
		$data = $this->input->post();

		$this->load->model('scan_verify/scan_verify_model');

		$sv_details = $this->scan_verify_model->checkScanVerify($this->location, $data['ref-no'], $data['recv-from'], $data['doc-type']);
		$total_scn = 0;

		$output = array(
			'success' => true,
			'message' => '' 
		);

		foreach ($sv_details as $key => $value) {
			$total_scn += $value['ScanQty'];
		}

		if($total_scn == 0){
			$output = array(
				'success' 	=> false,
				'message' 	=> 'Table has already been reset',
				'title' 	=> 'Information',
				'type'  	=> 'info' 	
			);
		}
		else{
			$this->DB->trans_begin();

			$new_data = $this->scan_verify_model->on_reset_scan_verify($this->location, $data, $this->DB);

			if($this->DB->trans_status() === FALSE){
	            $this->DB->trans_rollback();
	            $output = array(
	                                'success'=>false
	                            ,   'message'=>$this->DB->error()
	                        );
		    }
	        else{
	                $this->DB->trans_commit();
	        }

			$output['detail'] = $new_data;
		}


		echo json_encode($output);

	}

	public function postReceived(){
		$data = $this->input->post();

		$this->load->model('scan_verify/scan_verify_model');

		$output = array(
			'success' => true,
			'message' => '' 
		);

		$total_scn		= $this->scan_verify_model->getTotalScannedQty($this->location, $data['doc-type'], $data['recv-from'], $data['ref-no']);

		if($total_scn <= 0){
			$output = array(
				'success' 	=> false,
				'message' 	=> 'No items to be posted',
				'title' 	=> 'Information',
				'type'  	=> 'info' 	
			);
		}
		else{
			$detail = $this->scan_verify_model->getAllDetailsForPosting($this->location, $data['ref-no'], $data['recv-from'], $data['doc-type']);

			$this->DB->trans_begin();

			$this->scan_verify_model->on_post_received($this->location, $data, $detail, $total_scn, $this->DB);	

			if($this->DB->trans_status() === FALSE){
	            $this->DB->trans_rollback();
	            $output = array(
	                                'success'=>false
	                            ,   'message'=>$this->DB->error()
	                        );
		    }
	        else{
	                $this->DB->trans_commit();
	        }
		}

		echo json_encode($output);
	}
}