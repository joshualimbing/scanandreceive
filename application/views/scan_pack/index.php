
	<style>
		/* The switch - the box around the slider */
		.switch {
		  position: relative;
		  display: inline-block;
		  width: 60px;
		  height: 34px;
		}

		/* Hide default HTML checkbox */
		.switch input {
		  opacity: 0;
		  width: 0;
		  height: 0;
		}

		/* The slider */
		.slider {
		  position: absolute;
		  cursor: pointer;
		  top: 0;
		  left: 0;
		  right: 0;
		  bottom: 0;
		  background-color: #ccc;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		.slider:before {
		  position: absolute;
		  content: "";
		  height: 26px;
		  width: 26px;
		  left: 4px;
		  bottom: 4px;
		  background-color: white;
		  -webkit-transition: .4s;
		  transition: .4s;
		}

		input:checked + .slider {
		  background-color: #4EBD45;
		}

		input:focus + .slider {
		  box-shadow: 0 0 1px #4EBD45;
		}

		input:checked + .slider:before {
		  -webkit-transform: translateX(26px);
		  -ms-transform: translateX(26px);
		  transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
		  border-radius: 34px;
		}

		.slider.round:before {
		  border-radius: 50%;
		}
		#workstation-tbl tbody tr td{
			border: 0px;
		}
	</style>
	<body style="background: #DEDEE0">
		<section class="wrapper wrapper-content">

			<div class="container">
				<div class="row">
					<div class="table-responsive" style="margin-left: 20px; margin-top: 20px">
						<table class="table table-responsive" id="workstation-tbl" style="width: 80%">
							<tbody>
								<tr>
									<td style="font-weight: bold; width: 40%; font-size: 12px; padding-right: 0px; padding-top: 13px;">Work Station ID:</td>
									<td style="width: 60%; font-size: 14px"><?= $workstation_id ?></td>
								</tr>
								<tr>
									<td style="font-weight: bold; width: 40%; font-size: 12px; padding-top: 13px;">Receiving Date:</td>
									<td style="width: 60%; font-size: 14px"><?= date('m/d/Y') ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="container" style="background: #FFFFFF;">
				
				<div class="table-responsive">
					<table class="table table-borderless" style="margin-top: 10px" id="picklist-detail-list">
						<tbody>
							<tr>
								<td style="font-weight: bold; font-size: 14px; padding-top: 15px; padding-right: 0px; width: 40%">Carton No:</td>
								<td class="text-center"><input type="number" name="Carton_No" min="1" maxlength="3" oninput="maxLengthCheck(this)" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>
							</tr>
							<tr>
								<td style="font-weight: bold; font-size: 14px; padding-top: 15px; padding-right: 0px">Picklist No:</td>
								<td class="text-center">
									<select name="picklist" class="form-control picklist">
										<option></option>
										<?php foreach ($picklist_no as $pl): ?>
											<option value="<?= $pl['PNH_Picknum'] ?>"><?= $pl['PNH_Picknum'] ?></option>
										<?php endforeach ?>
									</select>
								</td>
							</tr>
						</tbody>
					</table>	
				</div>

				<div align="center" style="margin-top: 50px">
						<button class="btn btn-primary" id="next" disabled style="border-radius: 10px; width: 40%">Next</button>
						<a role="button" href="<?= DOMAIN ?>scan_pack/show_carton" class="btn text-light" style="border-radius: 10px; width: 40%; background: #929197">Show Carton</a>
				</div>
				<div align="center">
					<a role="button" href="<?= DOMAIN ?>user/menu" class="btn btn-primary" style="width: 80%; border-radius: 10px; margin-top: 20px; margin-bottom: 20px">Back</a>
				</div>
					
					

			</div>
		</section>
	</body>
<script src="<?= JS_DIR ?>app/scan_pack/index.js"></script>
