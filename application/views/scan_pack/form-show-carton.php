
	<style>

		@media screen and (max-width: 2560px){
			.overall-div{
				width: 80%;
				margin-left: 180px;
			}
		}
		
		@media screen and (max-width: 1440px){
			.overall-div{
				width: 100%;
				margin-left: 0px;
			}
		}

		@media screen and (max-width: 1024px){
			table tbody tr td.picklist-input{
				width: 80%!important;
			}
			table tbody tr td.picklist-label{
				width: 20%!important;
			}
		}

		@media screen and (max-width: 425px){
			table tbody tr td.picklist-input{
				width: 70%!important;
			}
			table tbody tr td.picklist-label{
				width: 30%!important;
			}
		}

		/*@media screen and (max-width: 375px){
			table tbody tr td.picklist-input{
				width: 70%!important;
			}
			table tbody tr td.picklist-label{
				width: 30%!important;
			}
			.picklist-detail{
				font-size: 12px;
			}
		}*/

		@media screen and (max-width: 370px){
			.close-carton-desc{
				margin-left: -18px!important;
			}
			.unpack-pl-desc{
				margin-left: -7px!important;
			}
		}
		
		@media screen and (max-width: 320px){
			table tbody tr td.picklist-input{
				width: 60%!important;
			}
			table tbody tr td.picklist-label{
				width: 40%!important;
			}
		}

		.picklist-detail{
			border: 0px;
		}
		#upc-table tbody tr td{
			border: 0px;
		}
	</style>
	<body style="background: #DEDEE0">
		<section class="wrapper wrapper-content" style="background: #DEDEE0">

				
				<div class="container" style="background: #DEDEE0; margin-top: 10px" align="center">
					
					<div class="form-horizontal">
						<div class="row">
							<div class="col-3">
								<div class="form-group">
									<a href="<?= DOMAIN.$this->uri->segment(1) ?>">
										<span class="fa fa-arrow-left" style="color: #3989C8; font-size: 20px"></span>
										<div style="font-size: 12px; color: #212529; user-select: none;">
											Back
										</div>
									</a>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group breakdown-btn">
									<span class="fa fa-list" style="color: #3989C8; font-size: 20px"></span>
									<div style="font-size: 12px; color: #212529; user-select: none;">
										Breakdown
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group close-pl" style="cursor: pointer">
									<span class="fa fa-archive" style="color: #3989C8; font-size: 20px"></span>
									<div style="font-size: 12px; user-select: none">
										Close PL
									</div>
								</div>
							</div>	
							<div class="col-3">
								<div class="form-group carton-btn">
									<span class="fa fa-folder" style="color: #3989C8; font-size: 20px"></span>
									<div style="font-size: 12px; margin-left: -5px; user-select: none;" class="close-carton-desc">
										Close Carton
									</div>
								</div>
							</div>
						</div>
					</div>

					<hr style="background-color: #FFFFFF; margin-top: -10px">

					<div class="form-horizontal">
						<div class="row">
							<div class="col-4">
								<div class="form-group">
									<span style="font-size: 10px">Order Qty:</span>
									<label class="pull-right pick-qty" style="font-weight: bold; font-size: 14px">0</label>
								</div>
							</div>
							<div class="col-4">
								<div class="form-group">
									<span style="font-size: 10px">Packed Qty:</span>
									<label class="pull-right pck-qty" style="font-weight: bold; font-size: 14px">0</label>
								</div>
							</div>
							<div class="col-4">
								<div class="form-group">
									<span style="font-size: 10px">Carton Qty:</span>
									<label class="pull-right carton-qty" style="font-weight: bold; font-size: 14px">0</label>
								</div>
							</div>
						</div>
					</div>

					<div class="table-responsive">
						<table class="table table-borderless picklist">
							<tbody>
								<tr>
									<td style="width: 10%; padding-right: 0px; padding-top: 15px!important" class="picklist-label">Picklist No.:</td>
									<td style="padding-left: 0px; width: 90%" class="picklist-input">
										<select name="PNH_Picknum" class="form-control">
											<option></option>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<hr style="background-color: #FFFFFF; margin-top: -10px">

					<div class="table-responsive" style="margin-top: -20px;">
						<table class="table table-borderless">
							<tbody>
								<tr>
									<td style="width: 40%; font-size: 12px; padding-top: 20px!important">Filter Carton</td>
									<td class="text-center" style="width: 5%; font-size: 10px; padding-top: 20px!important">From</td>
									<td style="width: 25%">
										<input type="number" name="From" readonly class="form-control" min="1" maxlength="3" oninput="maxLengthCheck(this)" style="font-size: 10px" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
									</td>
									<td class="text-center" style="width: 5%; font-size: 10px; padding-top: 20px!important">To</td>
									<td style="width: 25%">
										<input type="number" name="To" readonly class="form-control" min="1" maxlength="3" oninput="maxLengthCheck(this)" style="font-size: 10px" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

				</div>
				<div align="center" style="margin-top: -20px">
					Cartons
				</div>

				<div class="container" style="background: #FFFFFF">
					<div class="table-responsive" style="height: 150px; max-height: 150px">
						<table class="table table-hover table-bordered" style="margin-top: 20px" id="show-carton-header-list">
							<thead>
								<thead>
									<tr>
										<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Carton No.</th>
										<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Packed Qty</th>
										<th style="width: 60%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Carton Status</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</thead>
						</table>
					</div>
				</div>

				<div align="center">
					Items
				</div>

				<div class="container" align="center" style="background: #FFFFFF; height: 100%">
					<div class="table-responsive" style="height: 300px; max-height: 300px">
						<table class="table table-bordered table-condensed table-hover" style="margin-top: 20px; width: 100%" id="show-carton-detail-list">
							<thead>
								<tr>
									<th class="text-center" style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Barcode</th>
									<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Item Code</th>
									<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Category</th>
									<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Color</th>
									<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Size</th>
									<th class="text-center" style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Packed<br>Qty</th>
									<th class="text-center" style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Required<br>Qty</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
		<?php $this->load->view('scan_pack/breakdown-template'); ?>
		</section>
		<script src="<?= JS_DIR ?>app/scan_pack/form-show-carton.js"></script>
	</body>
