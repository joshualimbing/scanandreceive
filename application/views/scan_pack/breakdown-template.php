		<div class="modal fade" id="breakdown-detail" tabindex="-1" role="dialog" aria-hidden="true">
				<input type="hidden" id="line-no" value="0"/>
				<div class="modal-dialog modal-lg" style="width:95%">
				<div class="modal-content">
						<div class="modal-header" style="background-color: #DEDEE0; color:white; padding: 15px 0 15px 0">
							
						<span aria-hidden="true" id="close-matrix" class="fa fa-arrow-left close-breakdown" style="margin-left: 15px; color: #4998BF"></span>
						<span class="pull-right fa fa-refresh refresh-breakdown" style="margin-right: 17px; color: #60A25A"></span>
						
						</div>
						<!-- MODAL BODY -->
						<div class="modal-body" style="padding-bottom: 10px">
							<div class="container" style="background: #FFFFFF">
								<div class="table-responsive" style="height: 200px; max-height: 200px">
									<table class="table table-hover table-bordered" style="margin-top: 10px" id="picknote-detail-list">
										<thead>
											<thead>
												<tr>
													<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Barcode</th>
													<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Item Code</th>
													<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Category</th>
													<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Color</th>
													<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Size</th>
													<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Required<br>Qty</th>
													<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Packed Qty</th>
												</tr>
											</thead>
											<tbody>
												
											</tbody>
										</thead>
									</table>
								</div>
								
								
							</div>

							<div align="center" style="width: 100%; background: #DEDEE0; margin-top: 10px">
								Carton Details
							</div>

							<div class="container" style="background: #FFFFFF; height: 100%">
								<div class="table-responsive" style="height: 200px; max-height: 200px">
									<table class="table table-hover table-bordered" style="margin-top: 20px" id="carton-header-list">
										<thead>
											<thead>
												<tr>
													<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Carton No.</th>
													<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Packed Qty</th>
													<th style="width: 60%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Carton Status</th>
												</tr>
											</thead>
											<tbody>
												
											</tbody>
										</thead>
									</table>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>

