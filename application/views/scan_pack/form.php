	
 	<style>

		@media screen and (max-width: 2560px){
			.overall-div{
				width: 80%;
				margin-left: 180px;
			}
		}
		
		@media screen and (max-width: 1440px){
			.overall-div{
				width: 100%;
				margin-left: 0px;
			}
		}

		@media screen and (max-width: 375px){
			.picklist-detail{
				font-size: 12px;
			}
			#barcode-scanner video{
				width: 240px;
			}
		}

		@media screen and (max-width: 370px){
			.packed-qty{
				padding-left: 0px;			
			}
			.picklist-detail{
				font-size: 12px;
			}
			.close-carton-desc{
				margin-left: -18px!important;
			}
			.unpack-pl-desc{
				margin-left: -7px!important;
			}
			.close-carton{
				margin-right: -20px!important;
			}
			.fa-folder{
				margin-left: -20px
			}
			.unpack{
				margin-left: -10px
			}
		}
		.picklist-detail{
			border: 0px;
		}
		#upc-table tbody tr td{
			border: 0px;
		}
		.close-pl:hover{
			text-decoration: underline;
		}
		.close-carton:hover{
			text-decoration: underline;
		}
		#unpack-pl:hover{
			text-decoration: underline;
		}
		.breakdown:hover{
			text-decoration: underline;
		}
		/* Loading */
		.spinner {
		   position: sticky;
		   left: 40%;
		   top: 50%;
		   height:60px;
		   width:60px;
		   margin:0px auto;
		   z-index: 999999;
		   -webkit-animation: rotation .6s infinite linear;
		   -moz-animation: rotation .6s infinite linear;
		   -o-animation: rotation .6s infinite linear;
		   animation: rotation .6s infinite linear;
		   border-left:6px solid rgba(0,174,239,.15);
		   border-right:6px solid rgba(0,174,239,.15);
		   border-bottom:6px solid rgba(0,174,239,.15);
		   border-top:6px solid rgba(0,174,239,.8);
		   border-radius:100%;
		}

		@-webkit-keyframes rotation {
		   from {-webkit-transform: rotate(0deg);}
		   to {-webkit-transform: rotate(359deg);}
		}
		@-moz-keyframes rotation {
		   from {-moz-transform: rotate(0deg);}
		   to {-moz-transform: rotate(359deg);}
		}
		@-o-keyframes rotation {
		   from {-o-transform: rotate(0deg);}
		   to {-o-transform: rotate(359deg);}
		}
		@keyframes rotation {
		   from {transform: rotate(0deg);}
		   to {transform: rotate(359deg);}
		}
	</style>
	<body style="background: #FFFFFF">
		<section class="wrapper wrapper-content" style="background: #FFFFFF">
			<div class="spinner" style="display: none"></div>
			<div align="center" class="overall-div" style="background: #DEDEE0;">
				<div class="container" id="total-container" style="margin-top: 30px; margin-bottom: -30px; background: #DEDEE0" align="center">
					<div class="form-horizontal">
						<div class="row">
							<div class="col-6" style="margin-top: 20px">
								<div class="form-group">
									<span style="font-size: 12px">Total Order Qty:</span> 
									<label class="form-control-label" style="font-weight: bold" id="total-req"><?= number_format($header['PNH_TotalPickQty'] - $header['PNH_TotalPackedQty'], 0, ".", "") ?></label>
								</div>
							</div>
							<div class="col-6 packed-qty" style="margin-top: 20px">
								<div class="form-group">
									<span style="font-size: 12px">Total Packed Qty:</span> 
									<label class="form-control-label" style="font-weight: bold" id="total-pck"><?= number_format($header['TotalPackedQty'], 0, ".", "") ?></label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="container" style="background: #DEDEE0; margin-top: -10px" align="center">
					<hr style="color: #FFFFFF">
					<div class="form-horizontal">
						<div class="row">
							<div class="col-3"> 
								<div class="form-group" style="cursor: pointer" id="unpack-pl">
									<img class="unpack" src="<?= IMG_DIR ?>unpack-pl.png" alt="Unpack PL" height="20" width="30" style="user-select: none">
									<div style="font-size: 12px; user-select: none" class="unpack-pl-desc">
										Unpack PL
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group close-pl" style="cursor: pointer">
									<span class="fa fa-archive" style="color: #3989C8; font-size: 20px"></span>
									<div style="font-size: 12px; user-select: none">
										Close PL
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group close-carton" style="cursor: pointer">
									<span class="fa fa-folder" style="color: #3989C8; font-size: 20px"></span>
									<div style="font-size: 12px; margin-left: -5px; user-select: none" class="close-carton-desc">
										Close Carton
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="form-group breakdown" style="cursor: pointer">
									<span class="fa fa-list" style="color: #3989C8; font-size: 20px"></span>
									<div style="font-size: 12px; color: #212529; user-select: none">
										Breakdown
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>

				<div class="container" align="center" style="background: #FFFFFF">
					<div class="table-responsive">
						<table class="table table-borderless picklist-detail" id="picklist-header-list">
							<tbody>
								<tr>
									<td style="width: 30%; padding-right: 0px">Picklist No:</td>
									<td style="width: 20%; font-weight: bold"><?= $header['PNH_Picknum'] ?></td>
									<td style="width: 25%">Destination:</td>
									<td style="width: 25%; font-weight: bold"><?= $header['FK_RH_Location_Destin'] ?></td>
								</tr>
								<tr>
									<td style="width: 30%; padding-right: 0px; padding-top: 0px">Carton No:</td>
									<td style="width: 20%; font-weight: bold; padding-top: 0px"><?= $_GET['carton_no'] ?></td>
									<td style="width: 25%; padding-top: 0px">Status:</td>
									<td style="width: 25%; font-weight: bold; padding-top: 0px" class="scan-status">Scanning</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="container" align="center" style="margin-top: 10px; background: #DEDEE0">
					<div class="table-responsive">
						<table class="table table-borderless" id="upc-table">
							<tbody>
								<tr>
									<td style="width: 5%; padding-top: 20px">UPC:</td>
									<td class="text-center" style="width: 95%; padding-left: 0px">
										<input type="text" class="form-control" name="FK_IM_UPCCode" <?= ($carton['FK_Status_Code'] == 'CCP' ? 'readonly' : '') ?> style="width: 100%; font-size: 14px" maxlength="13" oninput="maxLengthCheck(this)">
									</td>
									<?php if (USE_MOBILE_CAMERA == 1): ?>
										<td>
											<button class="btn btn-primary" id="scanButton"><span class="fa fa-camera"></span></button>
										</td>
									<?php endif ?>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="form-horizontal" style="margin-top: -30px">
						<div class="row">
							<div class="col-6">
								<div class="table-responsive">
									<table class="table table-borderless">
										<tbody>
											<tr>
												<td style="width: 10%">Qty:</td>
												<td style="padding-left: 5px; width: 90%; padding-right: 25px">
													<input type="number" min="1" step="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="Barcode_Qty" class="form-control" style="width: 100%; font-size: 14px" value="1" <?= ($carton['FK_Status_Code'] == 'CCP' ? 'readonly' : '') ?>>
												</td>
											</tr>
										</tbody>
									</table>
								</div>		
							</div>
						</div>
					</div>
				</div>

				<div class="container" align="center" style="background: #FFFFFF">
					<div class="table-responsive" style="height: 200px; max-height: 200px">
						
						<table class="table table-bordered table-condensed table-hover" style="margin-top: 20px; width: 100%" id="picklist-detail-list">
							<thead>
								<tr>
									<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Location</th>
									<th style="width: 40%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Item Code</th>
									<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Color</th>
									<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Size</th>
									<th class="text-center" style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Packed<br>Qty</th>
									<th class="text-center" style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Required<br>Qty</th>
								</tr>
							</thead>
							<tbody>
								<?php if($carton['FK_Status_Code'] != 'CCP'): ?>
									<?php foreach ($details as $key => $value): ?>
										<tr class="picklist-details">
											<td style="font-size: 12px; padding: 5px" data-loc-source="<?= $value['PKFK_Location_Source'] ?>" data-loc-destin="<?= $value['PKFK_Location_Destin'] ?>"><?= $value['PKFK_Location_Source'] ?></td>
											<td style="font-size: 12px; padding: 5px" data-cat-code="<?= $value['FK_Category_Code'] ?>" data-upc-code="<?= $value['FK_IM_UPCCode'] ?>" data-item-code="<?= str_replace('"', '&quot;', $value['PKFK_InvMaster_Code']) ?>" data-item-desc="<?= $value['FK_IM_Item_Desc'] ?>"><?= $value['PKFK_InvMaster_Code'] ?></td>
											<td style="font-size: 12px; padding: 5px" data-color-code="<?= $value['FK_Color_Code'] ?>"><?= $value['FK_Color_Code'] ?></td>
											<td style="font-size: 12px; padding: 5px" data-size-code="<?= $value['FK_Size_Code'] ?>"><?= $value['FK_Size_Code'] ?></td>
											<td class="text-right" style="font-size: 12px; padding: 5px" name="pck-qty"><?= number_format($value['ScanQty'], 0, ".", "") ?></td>
											<td class="text-right" style="font-size: 12px; padding: 5px" data-item="<?= $value['PKFK_InvMaster_Code'] ?>" data-loc-barcode="<?= $value['PKFK_Location_Source'] ?> - <?= $value['FK_IM_UPCCode'] ?>" name="required-qty"><?= number_format($value['PND_PickQty'] - $value['PND_PackedQty'], 0, ".", "") ?></td>
										</tr>
									<?php endforeach ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>
					<div style="margin-top: 10px; margin-bottom: 30px">
						<button id="back" class="btn text-light" style="background: #297FBA; font-weight: bold; width: 40%">Back</button>
						<button id="show-carton" class="btn text-light" style="background: #929197; font-weight: bold; width: 40%">Show Carton</button>
					</div>
					<hr>
				</div>
			</div>
			<audio id="audio" src="<?= DOMAIN ?>audio/errmsg.wav"></audio>
		</section>
		<?php $this->load->view('scan_pack/breakdown-template'); ?>
		<?php $this->load->view('scan_pack/show-carton-template'); ?>
		<?php if (USE_MOBILE_CAMERA == 1): ?>
			<?php $this->load->view('scan_pack/scan-barcode-template'); ?>
		<?php endif ?>
		<script>
			var picklist 	= '<?= $header['PNH_Picknum'] ?>';
			var carton_no 	= '<?= $_GET['carton_no'] ?>';
			var loc_destin 	= '<?= $header['FK_RH_Location_Destin'] ?>';
		</script>
		<script src="<?= JS_DIR ?>app/scan_pack/form_02242021_0421PM.js"></script>
		<script src="<?= EXTENSION_JS ?>quagga/dist/quagga.js"></script>
	</body>