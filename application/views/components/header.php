<html>
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo IMG_DIR; ?>scan-and-receive.png" type="image/x-icon">
        <link rel="icon" href="<?php echo IMG_DIR; ?>scan-and-receive.png" type="image/x-icon">        
        <title>Scan & Receive</title>
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>bootstrap.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>font-awesome.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>datapicker/datepicker3.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>dataTables/datatables.min.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>daterangepicker/daterangepicker-bs3.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>select2/select2.min.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>sweetalert/sweetalert.css">
		<script src="<?= EXTENSION_JS ?>popper.min.js"></script>
		<script src="<?= JS_DIR ?>app/ref.js"></script>
	

	
		