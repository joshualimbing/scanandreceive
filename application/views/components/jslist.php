		<script>
			const global = {
				site_name : '<?php echo DOMAIN ?>'
			};
		</script>
		<script src="<?= EXTENSION_JS ?>jquery-3.1.1.min.js"></script>
		<script src="<?= EXTENSION_JS ?>bootstrap.js"></script>
		<script src="<?= JS_DIR ?>app/moment.js"></script>
		<script src="<?= EXTENSION_JS ?>datapicker/bootstrap-datepicker.js"></script>
		<script src="<?= EXTENSION_JS ?>dataTables/datatables.min.js"></script>
		<script src="<?= EXTENSION_JS ?>daterangepicker/daterangepicker.js"></script>
		<script src="<?= EXTENSION_JS ?>select2/select2.full.min.js"></script>
		<script src="<?= EXTENSION_JS ?>sweetalert/sweetalert.min.js"></script>
		<script src="<?= EXTENSION_JS ?>dataTables/dataTables.bootstrap4.min.js"></script>
		<script src="<?= EXTENSION_JS ?>jquerymd5.js"></script>
		<script src="<?= JS_DIR ?>app/session.js"></script>
	</head>

