<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
		<title>Menu</title>
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>/bootstrap.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>/font-awesome.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>/select2/select2.min.css">
		<script src="<?= EXTENSION_JS ?>/jquery-3.1.1.min.js"></script>
		<script src="<?= EXTENSION_JS ?>/popper.min.js"></script>
		<script src="<?= EXTENSION_JS ?>/bootstrap.js"></script>
		<script src="<?= EXTENSION_JS ?>/select2/select2.full.min.js"></script>
	</head>
	<style>
		@media screen and (max-width: 425px)
		.form-horizontal{
			margin-top: 40px
		}
	</style>
	<body style="background: #DEDEE0">
		<section class="wrapper wrapper-content">
			<div class="container">
				<div class="col-12">
					<div class="form-horizontal" style="margin-top: 70px">
						<div class="row">
							<div class="col-6">
								<a href="<?= DOMAIN ?>scan_pack">
									<div align="center" class="form-group" style="background: #A2C4DD; border-radius: 25px">
										<img src="<?= IMG_DIR ?>scan-pack.png" alt="Scan Pack" height="100">
										<div style="color: #FFFFFF">
											Scan Pack
										</div>
									</div>
								</a>
							</div>
							<div class="col-6">
								<a href="<?= DOMAIN ?>scan_received">
									<div align="center" class="form-group" style="background: #A2C4DD; border-radius: 25px">
										<img src="<?= IMG_DIR ?>scan-receive.png" alt="Scan Receive" height="50" style="margin-top: 30px">
										<div style="color: #FFFFFF; margin-top: 21px">
											Scan Receive
										</div>
									</div>
								</a>
							</div>
							<div class="col-6" style="margin-top: 10px">
								<a href="<?= DOMAIN ?>scan_verify">
									<div align="center" class="form-group" style="background: #A2C4DD; border-radius: 25px">
										<img src="<?= IMG_DIR ?>scan-verify.png" alt="Scan Verify" height="70" style="margin-top: 30px">
										<div style="color: #FFFFFF">
											Scan Verify
										</div>
									</div>
								</a>
							</div>
							<div class="col-6" style="margin-top: 10px">
								<a href="<?= DOMAIN ?>item_count">
									<div align="center" class="form-group" style="background: #A2C4DD; border-radius: 25px">
										<img src="<?= IMG_DIR ?>item-count.png" alt="Item Count" height="70" style="margin-top: 30px">
										<div style="color: #FFFFFF;">
											Item Count
										</div>
									</div>
								</a>
							</div>
							<div class="col-6 offset-3" style="margin-top: 10px">
						     	<a href="<?= DOMAIN ?>scan_ondemand">
									<div align="center" class="form-group" style="background: #A2C4DD; border-radius: 25px">
										<img src="<?= IMG_DIR ?>scan-on-demand.png" alt="Scan On-demand" height="70" style="margin-top: 30px">
										<div style="color: #FFFFFF;">
											Scan for DR
										</div>
									</div>
								</a>
						    </div>
						</div>
							<div align="center">
								<a href="<?= DOMAIN ?>user/logout" role="button" class="btn btn-primary" style="width: 90%; margin-top: 20px; margin-bottom: 20px;">Logout</a>
							</div>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>