<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
		<link rel="shortcut icon" href="<?php echo IMG_DIR; ?>scan-and-receive.png" type="image/x-icon">
        <link rel="icon" href="<?php echo IMG_DIR; ?>scan-and-receive.png" type="image/x-icon"> 
		<title>Scan & Receive</title>
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>bootstrap.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>font-awesome.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>select2/select2.min.css">
		<link rel="stylesheet" href="<?= EXTENSION_CSS ?>sweetalert/sweetalert.css">
		<script src="<?= EXTENSION_JS ?>jquery-3.1.1.min.js"></script>
		<script src="<?= EXTENSION_JS ?>popper.min.js"></script>
		<script src="<?= EXTENSION_JS ?>bootstrap.js"></script>
		<script src="<?= EXTENSION_JS ?>select2/select2.full.min.js"></script>
		<script src="<?= EXTENSION_JS ?>sweetalert/sweetalert.min.js"></script>
		<script>
			const global = {
				site_name : '<?php echo DOMAIN ?>'
			};
		</script>
	</head>
	<style>
		hr{
			color: #FFFFFF!important;
		}
		td{
			border: 0px;
		}
		@media screen and (max-width: 320px){
			.scan-n-recv thead th.scan-txt{
				font-size: 16px!important;
			}
			.scan-n-recv thead th img{
				height: 50px!important;
			}
		}
		@media screen and (max-width: 425px){
			.main-body{
				margin-top: 10px!important
			}
		}
	</style>
	<body style="background: #DEDEE0">
		<div align="center" class="main-body" style="margin-top: 50px">
			<img src="<?= IMG_DIR ?>/sg-logo-no-bg.png" alt="SG Logo" width="300">
			<hr>
			<div class="table-responsive" align="center style" style="width: 80%;">
				<table class="table table-borderless scan-n-recv">
					<thead>
						<th class="text-right"><img src="<?= IMG_DIR ?>scan-and-receive.png" alt="Scan and Receive" style="opacity: 0.5; height: 80px"></th>
						<th class="text-left scan-txt" style="font-weight: bold; font-family: Arial Black; font-size: 24px">Scan<br>& Receive</th>
					</thead>
				</table>
			</div>
			<div class="container">

					<div class="form-group">
						<select name="Company" style="width: 100%">
							<option></option>
							<?php foreach ($company as $key => $c): ?>
								<option value="<?= $c['C_PK_ID'] ?>"><?= $c['C_Name'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					
					<div class="form-group">
						<input type="text" class="form-control" readonly placeholder="Username" name="U_UserID">
					</div>

					<div class="form-group">
						<input type="password" class="form-control" readonly placeholder="Password" name="U_Password">
					</div>

					<div class="form-group">
						<select name="Location" disabled style="width: 100%">
							<option></option>
						</select>
					</div>
					<div class="button-submit" style="margin-bottom: 20px">
						<button type="submit" class="btn btn-primary" id="login">Login</button>
					</div>

			</div>
		</div>
		<script src="<?= JS_DIR ?>app/login.js"></script>
	</body>
</html>