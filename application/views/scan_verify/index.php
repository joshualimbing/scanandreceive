
	<style>
		
		@media screen and (max-width: 2560px){

		}

		@media screen and (max-width: 375px){
			.receive-detail{
				font-size: 12px;
			}
		}

		#workstation-tbl tbody tr td{
			border: 0px;
		}
	</style>
	<body style="background: #DEDEE0">
		<section class="wrapper wrapper-content">

			<div class="container">
				
				<div class="form-horizontal">
					<div class="row">
						

							<div class="col-12" style="padding: 0px">

								<div class="table-responsive" style="margin-left: 10px; margin-top: 20px">
									<table class="table table-responsive" id="workstation-tbl">
										<tbody>
											<tr>
												<td style="font-weight: bold; width: 42%; font-size: 12px; padding-right: 0px; padding-left: 0px">Work Station ID:</td>
												<td style="width: 80%; font-size: 14px"><?= $wsid ?></td>
											</tr>
											<tr>
												<td style="font-weight: bold; width: 40%; font-size: 12px; padding-right: 0px; padding-left: 0px">Receiving Date:</td>
												<td style="width: 60%; font-size: 14px"><?= date('m/d/Y') ?></td>
											</tr>
										</tbody>
									</table>
								</div>

							</div>	

							<?php if($loc_type == 'STR'): ?>
								<div class="col-2 import-doc">
									 <img src="<?= IMG_DIR ?>import.png" alt="Import Doc" height="40" width="40" style="margin-top: 40px!important" class="pull-right fake-btn" align="center">

									 <form class="file-upload-form" id="file-upload-form" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="<?= DOMAIN ?>scan_verify/extractFile">
									 	<input type="file" id="file-upload" name="file" style="display: none">
									 	<input type="submit" id="submit-form" style="display: none">
									 </form>
								</div>
							<?php endif ?>

						
					</div>
				</div>
				
			</div>

			
			
			<div class="container" style="background: #FFFFFF">
				
				<div class="table-responsive">
					<table class="table table-borderless receive-detail" style="margin-top: 10px">
						<tbody>
							<tr>
								<td style="font-weight: bold; width: 42%; padding-top: 20px">Document Type:</td>
								<td class="text-left" style="padding-left: 0px">
									<select class="select-doc-type form-control" <?= (trim($loc_type) == 'STR' ? 'disabled' : '') ?>>
										<option></option>
										<?php foreach ($transtype as $key => $value): ?>
											<option value="<?= $value['TT_Code'] ?>"><?= $value['TT_Code'] ?></option>
										<?php endforeach ?>
									</select>
								</td>
							</tr>
							<tr>
								<td style="font-weight: bold; padding-top: 20px">From:</td>
								<td class="text-left" style="padding-left: 0px">
									<select class="select-recv-from form-control" disabled>
										<option></option>
									</select>
								</td>
							</tr>
							<tr>
								<td style="font-weight: bold; padding-top: 20px">Reference No:</td>
								<td class="text-left" style="padding-left: 0px">
									<select class="select-ref-no form-control" disabled>
										<option></option>
									</select>
								</td>
							</tr>
							<tr>
								<td style="font-weight: bold; padding-top: 20px">Remarks:</td>
								<td class="text-center" style="padding-left: 0px">
									<textarea id="Doc_Remarks" cols="30" rows="5" class="form-control" <?= (trim($loc_type) == 'STR' ? 'readonly' : '') ?>></textarea>
								</td>
							</tr>
						</tbody>
					</table>	
				</div>

				<div align="center" style="margin-top: 20px">
						<button class="btn text-light back" style="border-radius: 10px; width: 40%; background: #B5B5C1; margin-bottom: 20px">Back</button>
						<button class="btn btn-primary next" style="border-radius: 10px; width: 40%; margin-bottom: 20px" <?= (trim($loc_type) == 'STR' ? 'disabled' : '') ?>>Next</button>
				</div>
					
					

			</div>
		</section>
	</body>
	<script src="<?= JS_DIR ?>app/scan_verify/index.js"></script>
	<?php if(!empty($_SESSION['alert'])): ?>
		<script>
			swal("<?= $_SESSION['alert']['title'] ?>", "<?= $_SESSION['alert']['message'] ?>", "<?= $_SESSION['alert']['type'] ?>");
		</script>
	<?php endif; ?>
	<?php unset($_SESSION['alert']) ?>