<div class="modal fade" id="bin-loc-detail" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-lg" style="width:95%">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #DEDEE0; color:black; padding: 15px 0 15px 0;">
					<span aria-hidden="true" id="close-matrix" class="fa fa-arrow-left close-list" style="margin-left: 15px; color: #4998BF"></span>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 50px" align="center">
					<div class="form-group">
						<label style="font-size: 12px; font-weight: bold;">Item Code:</label>
						<input type="text" class="form-control item-code-input" readonly>
					</div>
					<div class="form-group">
						<label style="font-size: 12px; font-weight: bold;">Location List:</label>
						<select name="Bin_Location" class="form-control" style="width: 100%">
							
						</select>
					</div>
				</div>
		</div>
	</div>
</div>