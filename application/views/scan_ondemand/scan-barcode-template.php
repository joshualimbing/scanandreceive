<div class="modal fade" id="scan-barcode" tabindex="-1" role="dialog" aria-hidden="true">
<input type="hidden" id="line-no" value="0"/>
<div class="modal-dialog modal-lg" style="width:95%">
<div class="modal-content">
		<div class="modal-header" style="background-color: #DEDEE0; color:white; padding: 15px 0 15px 0">
			
		<span aria-hidden="true" id="close-barcode" class="fa fa-arrow-left close-breakdown" style="margin-left: 15px; color: #4998BF"></span>
		
		</div>
		<!-- MODAL BODY -->
		<div class="modal-body" style="padding-bottom: 10px">
			<div class="container" style="background: #FFFFFF">
				<hr>
				<h3>Scan Here</h3>
				<div id="barcode-scanner" class="text-center"></div>
			</div>
		</div>
</div>
</div>
</div>