
	<style>
		@media screen and (max-width: 375px){
			.receive-detail{
				font-size: 12px;
			}
		}
	</style>
	<body style="background: #DEDEE0">
		<section class="wrapper wrapper-content">

			<div class="container">
				<div class="row">
					<div class="table-responsive" style="margin-left: 20px; margin-top: 20px">
						<table class="table table-responsive" id="workstation-tbl">
							<tbody>
								<tr>
									<td style="font-weight: bold; width: 40%; font-size: 12px">Work Station ID:</td>
									<td style="width: 60%; font-size: 14px"><?= $wsid ?></td>
								</tr>
								<tr>
									<td style="font-weight: bold; width: 40%; font-size: 12px">Delivery Date:</td>
									<td style="width: 60%; font-size: 14px"><?= date('m/d/Y') ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="container" style="background: #FFFFFF">
				
				<div class="table-responsive">
					<table class="table table-borderless receive-detail" style="margin-top: 10px">
						<tbody>
							<tr>
								<td style="font-weight: bold; padding-top: 20px">DR Number:</td>
								<td class="text-left" style="padding-left: 0px">
									<select class="select-dr form-control" required>
										<option></option>
										<option value="NEW DR" selected>NEW DR</option>
										<?php foreach ($dr_list as $key => $list): ?>
											<option value="<?= $list['ODH_DocNo'] ?>"><?= $list['ODH_DocNo'] ?></option>
										<?php endforeach ?>
									</select>
								</td>
							</tr>
							<tr>
								<td style="font-weight: bold; width: 35%; padding-top: 20px">Deliver To:</td>
								<td class="text-left" style="padding-left: 0px">
									<select class="select-del-to form-control" required>
										<option></option>
										<?php foreach ($location_list as $key => $list): ?>
											<option value="<?= $list['Loc_Code'] ?>"><?= $list['Description'] ?></option>
										<?php endforeach ?>
									</select>
								</td>
							</tr>
							<tr>
								<td style="font-weight: bold; padding-top: 20px">Remarks:</td>
								<td class="text-center" style="padding-left: 0px">
									<textarea name="" cols="30" rows="2" class="form-control" id="Doc_Remarks" placeholder="Enter Remarks"></textarea>
								</td>
							</tr>
						</tbody>
					</table>	
				</div>

				<div align="center" style="margin-top: 30px">
						<a role="button" href="<?= DOMAIN ?>user/menu" class="btn text-light" style="border-radius: 10px; width: 40%; background: #B5B5C1; margin-bottom: 20px">Back</a>
						<button class="btn btn-primary" id="next" style="border-radius: 10px; width: 40%; margin-bottom: 20px">Next</button>
						<?php if ($user == 'Admin'): ?>
							<!-- <button role="button" class="btn btn-danger text-light" id="reprocess" style="border-radius: 10px; width: 40%; margin-bottom: 20px">Reprocess DR</button> -->
						<?php endif ?>
				</div>
					
					

			</div>
		</section>
		<?php $this->load->view('scan_ondemand/reprocess-template') ?>
		<script src="<?= JS_DIR ?>app/scan_ondemand/index.js"></script>
	</body>