
	<style>

		/* Loading */
		.spinner {
		   position: sticky;
		   left: 40%;
		   top: 50%;
		   height:60px;
		   width:60px;
		   margin:0px auto;
		   z-index: 999999;
		   -webkit-animation: rotation .6s infinite linear;
		   -moz-animation: rotation .6s infinite linear;
		   -o-animation: rotation .6s infinite linear;
		   animation: rotation .6s infinite linear;
		   border-left:6px solid rgba(0,174,239,.15);
		   border-right:6px solid rgba(0,174,239,.15);
		   border-bottom:6px solid rgba(0,174,239,.15);
		   border-top:6px solid rgba(0,174,239,.8);
		   border-radius:100%;
		}

		@-webkit-keyframes rotation {
		   from {-webkit-transform: rotate(0deg);}
		   to {-webkit-transform: rotate(359deg);}
		}
		@-moz-keyframes rotation {
		   from {-moz-transform: rotate(0deg);}
		   to {-moz-transform: rotate(359deg);}
		}
		@-o-keyframes rotation {
		   from {-o-transform: rotate(0deg);}
		   to {-o-transform: rotate(359deg);}
		}
		@keyframes rotation {
		   from {transform: rotate(0deg);}
		   to {transform: rotate(359deg);}
		}

		@media screen and (max-width: 2560px){
			.overall-div{
				width: 80%;
				margin-left: 180px;
			}
		}
		
		@media screen and (max-width: 1440px){
			.overall-div{
				width: 100%;
				margin-left: 0px;
			}
		}

		@media screen and (max-width: 425px){
			.doc-header{
				font-size: 12px!important;
			}
			#barcode-scanner video{
				width: 240px;
			}
		}

		@media screen and (max-width: 375px){
			.picklist-detail{
				font-size: 12px;
			}
		}

		@media screen and (max-width: 370px){
			.packed-qty{
				padding-left: 0px;			}
			.picklist-detail{
				font-size: 12px;
			}
			.close-carton-desc{
				margin-left: -18px!important;
			}
			.unpack-pl-desc{
				margin-left: -7px!important;
			}
			.undo-scan{
				margin-left: -20px
			}
			.post{
				margin-left: -15px
			}
		}

		@media screen and (max-width: 320px){

		}

		.picklist-detail{
			border: 0px;
		}
		#upc-table tbody tr td{
			border: 0px;
		}
		.undo-btn:hover{
			text-decoration: underline;
		}
		.post-btn:hover{
			text-decoration: underline;
		}
		.scan-btn:hover{
			text-decoration: underline;
		}
		.back-btn:hover{
			text-decoration: underline;
		}
		.cancel-btn:hover{
			text-decoration: underline;
		}
		.null-loc:hover{
			cursor: pointer;
			text-decoration: underline;
		}
		.null-loc{
			color: #3989C8;
		}
		.custom-col {
		  flex: 0 0 20%!important;
		  max-width: 20%!important;
		}
	</style>
	<body style="background: #FFFFFF">
		<section class="wrapper wrapper-content" style="background: #FFFFFF">
			<div class="spinner" style="display: none"></div>
			<div align="center" class="overall-div" style="background: #DEDEE0; margin-top: 20px">
				
				<div class="container" style="margin-top: -20px;background: #DEDEE0" align="center">
					<div class="form-horizontal">
						<div class="row">
							<div class="col-6" style="margin-top: 20px">
								<div class="form-group">
									<span style="font-size: 12px">Total Scanned Qty:</span> 
									<label class="form-control-label tot-rec-qty" style="font-weight: bold"><?= number_format($total_scn, 0, ".", "") ?></label>
								</div>
							</div>
							<div class="col-6 packed-qty" style="margin-top: 20px">
								<div class="form-group">
									<span style="font-size: 12px">Status:</span> 
									<label class="form-control-label scn-status" style="font-weight: bold">Scanning</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="container" style="background: #DEDEE0;" align="center">
					<div class="form-horizontal" >
						<div class="row">
							<div class="custom-col"> 
								<div class="form-group back-btn" style="user-select: none; cursor: pointer">
									<span class="fa fa-arrow-left" style="color: #3989C8; font-size: 20px"></span>
									<div style="font-size: 12px">
										Back
									</div>
								</div>
							</div>
							<div class="custom-col">
								<div class="form-group scan-btn" style="user-select: none; cursor: pointer;">
									<img src="<?= IMG_DIR ?>scan-received-icon.png" alt="Scan Received" class="scan-recv" height="20" width="30" style="margin-left: -30px">
									<div style="font-size: 12px; margin-left: -26px">
										Scan Items
									</div>
								</div>
							</div>
							<div class="custom-col" style="margin-left: 5px">
								<div class="form-group undo-btn" style="user-select: none; cursor: pointer">
									<img src="<?= IMG_DIR ?>undo-scan.png" alt="Unpack PL" height="20" width="30" class="undo-scan">
									<div style="font-size: 12px" align="center" class="close-carton-desc">
										Undo Scan
									</div>
								</div>
							</div>
							<div class="custom-col" style="margin-left: -20px; margin-right: 15px">
								<div class="form-group cancel-btn" style="user-select: none; cursor: pointer">
									<span class="fa fa-times" style="color: #3989C8; font-size: 20px"></span>
									<div style="font-size: 12px; color: #212529">
										Cancel DR
									</div>
								</div>
							</div>		
							<div class="custom-col">
								<div class="form-group post-btn" style="user-select: none; cursor: pointer">
									<span class="fa fa-truck" style="color: #3989C8; font-size: 20px"></span>
									<div style="font-size: 12px; color: #212529">
										Create DR
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr style="background-color: #FFFFFF; margin-top: -5px">
				</div>

				

				<div class="container" align="center" style="background: #FFFFFF">
					<div class="table-responsive">
						<table class="table table-borderless picklist-detail">
							<tbody>
								<tr>
									<td class="doc-header" style="width: 10%; padding-right: 0px">Location:</td>
									<td class="doc-header" style="width: 45%; font-weight: bold"><?= $_SESSION['current_user']['login-location'] ?></td>
									<td class="doc-header" style="width: 15%">Destination:</td>
									<td class="doc-header" style="width: 20%; font-weight: bold"><?= $_GET['delLoc'] ?></td>
								</tr>
								<tr>
									<td class="doc-header" style="padding-right: 0px">DR No.:</td>
									<td class="doc-header" style="font-weight: bold; padding-right: 0px"><?= $_GET['DRNo'] ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="container" align="center" style="margin-top: 10px; background: #DEDEE0">
					<div class="table-responsive">
						<table class="table table-borderless" id="upc-table">
							<tbody>
								<tr>
									<td style="width: 5%; padding-top: 20px">UPC:</td>
									<td class="text-center" style="width: 95%; padding-left: 0px">
										<input type="text" class="form-control" style="width: 100%; font-size: 10px" name="FK_IM_UPCCode" maxlength="13" oninput="maxLengthCheck(this)">
									</td>
									<?php if (USE_MOBILE_CAMERA == 1): ?>
										<td>
											<button class="btn btn-primary" id="scanButton"><span class="fa fa-camera"></span></button>
										</td>
									<?php endif ?>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="form-horizontal" style="margin-top: -30px">
						<div class="row">
							<div class="col-6">
								<div class="table-responsive">
									<table class="table table-borderless">
										<tbody>
											<tr>
												<td style="width: 10%">Qty:</td>
												<td style="padding-left: 5px; width: 90%; padding-right: 25px">
													<input type="number" class="form-control" value="1" min="1" style="width: 100%; font-size: 10px" name="Barcode_Qty" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="3" oninput="maxLengthCheck(this)">
												</td>
											</tr>
										</tbody>
									</table>
								</div>		
							</div>
						</div>
					</div>
				</div>

				<div class="container" align="center" style="background: #FFFFFF">
					<div class="table-responsive" style="height: 300px; max-height: 300px">
						<table class="table table-bordered table-condensed table-hover" style="margin-top: 20px; width: 100%" id="deliver-detail-list">
							<thead>
								<tr>
									<th style="width: 30%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Item Code</th>
									<th style="width: 30%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Item Description</th>
									<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Location</th>
									<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Deliver Qty</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($details as $key => $detail): ?>
									<tr class="deliver-details">
										<td style="font-size: 12px; padding: 5px" data-barcode="<?= $detail['IM_UPCCode'] ?>"><?= $detail['PKFK_InvMaster_Code'] ?></td>
										<td style="font-size: 12px; padding: 5px"><?= $detail['IM_Item_Desc'] ?></td>
										<?php if($loc_type == 'STR'): ?>
											<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;"><?= $detail['ODD_Location_Source'] ?></td>
										<?php else: ?>
											<?php if ($detail['IM_ItemLocation'] !== NULL || $detail['IM_ItemLocation'] != ''): ?>
												<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;"><?= $detail['ODD_Location_Source'] ?></td>
											<?php else: ?>
												<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;" class="null-loc" data-item-code="<?= $detail['PKFK_InvMaster_Code'] ?>"><?= $detail['ODD_Location_Source'] ?></td>
											<?php endif; ?>
										<?php endif; ?>
										<td class="text-right" style="font-size: 12px; padding: 5px"><?= number_format($detail['ODD_ScanQty'], 0, ".", "") ?></td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
					<hr>
				</div>
			</div>
			<audio id="audio" src="<?= DOMAIN ?>audio/errmsg.wav"></audio>
		<?php $this->load->view('scan_ondemand/bin-location-template') ?>
		<?php if (USE_MOBILE_CAMERA == 1): ?>
			<?php $this->load->view('scan_ondemand/scan-barcode-template'); ?>
		<?php endif ?>
		</section>
		<script>
			var del_location 	= '<?= $_GET['delLoc'] ?>';
			var doc_no 			= '<?= $_GET['DRNo'] ?>';
			var loc_type 		= '<?= $loc_type ?>';
			var bin_list 		= <?= json_encode($bin_list) ?>;
		</script>
		<script src="<?= JS_DIR ?>app/scan_ondemand/form_02242021_0421PM.js"></script>
		<script src="<?= EXTENSION_JS ?>quagga/dist/quagga.js"></script>
	</body>