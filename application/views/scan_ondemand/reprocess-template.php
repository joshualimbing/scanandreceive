<div class="modal fade" id="reprocess-dr" role="dialog" aria-hidden="true">
		<input type="hidden" id="line-no" value="0"/>
		<div class="modal-dialog modal-lg" style="width:95%">
		<div class="modal-content">
				<div class="modal-header" style="background-color: #DEDEE0; color:black; padding: 15px 0 15px 0;">
					<span aria-hidden="true" id="close-matrix" data-dismiss="modal" class="fa fa-arrow-left close-list" style="margin-left: 15px; color: #4998BF; cursor: pointer;"></span>
				</div>
				<!-- MODAL BODY -->
				<div class="modal-body" style="padding-bottom: 50px" align="center">
					<div class="container">
						<div class="col-12">
							<div class="form-group row">
								<label class="col-md-3 col-12" style="font-size: 12px; font-weight: bold;">DR No.:</label>
								<div class="col-md-9 col-12">
									<select name="DH_DRNum" style="width: 100%">
									</select>
								</div>
							</div>
							<hr>
							<div class="table-responsive" style="height: 300px; max-height: 300px">
								<table class="table table-bordered table-condensed table-hover" style="margin-top: 20px; width: 100%" id="reprocess-list">
									<thead>
										<tr>
											<th style="width: 30%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Item Code</th>
											<th style="width: 30%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Item Description</th>
											<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Location</th>
											<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Deliver Qty</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							<div align="center">
								<button role="button" class="btn btn-danger text-light" id="reprocess-details" style="border-radius: 10px; width: 40%; margin-bottom: 20px"><span class="fa fa-refresh"></span> Reprocess</button>
							</div>
						</div>
				</div>
		</div>
	</div>
</div>
</div>