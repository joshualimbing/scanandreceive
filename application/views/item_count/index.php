
	<style>
		@media screen and (max-width: 375px){
			.item-count-detail{
				font-size: 12px;
			}
		}
	</style>
	<body style="background: #DEDEE0">
		<section class="wrapper wrapper-content">

			<div class="container">
				<div class="row">
					<div class="table-responsive" style="margin-left: 20px; margin-top: 20px">
						<table class="table table-responsive" id="workstation-tbl">
							<tbody>
								<tr>
									<td style="font-weight: bold; width: 40%; font-size: 12px">Work Station ID:</td>
									<td style="width: 60%; font-size: 14px"><?= $wsid ?></td>
								</tr>
								<tr>
									<td style="font-weight: bold; width: 40%; font-size: 12px">Receiving Date:</td>
									<td style="width: 60%; font-size: 14px"><?= date('m/d/Y') ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="container" style="background: #FFFFFF">
				
				<div class="table-responsive">
					<table class="table table-borderless item-count-detail" style="margin-top: 10px">
						<tbody>
							<tr>
								<td style="font-weight: bold; width: 40%; padding-top: 20px">Plan Count No:</td>
								<td class="text-left" style="padding-left: 0px">
									<select class="select-pcount-no form-control">
										<option></option>
									</select>
								</td>
							</tr>
							<tr>
								<td style="font-weight: bold; padding-top: 20px">Countsheet No:</td>
								<td class="text-left" style="padding-left: 0px">
									<select class="select-csheet-no form-control" disabled>
										<option></option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>	
				</div>

				<div align="center" style="margin-top: 80px">
						<a role="button" href="<?= DOMAIN ?>user/menu" class="btn text-light" style="border-radius: 10px; width: 40%; background: #B5B5C1; margin-bottom: 20px">Back</a>
						<button class="btn btn-primary" id="next" style="border-radius: 10px; width: 40%; margin-bottom: 20px">Next</button>
				</div>
					
					

			</div>
		</section>
		<script src="<?= JS_DIR ?>app/item_count/index.js"></script>
	</body>