
	<style>
		
		/* Loading */
		.spinner {
		   position: sticky;
		   left: 40%;
		   top: 50%;
		   height:60px;
		   width:60px;
		   margin:0px auto;
		   z-index: 999999;
		   -webkit-animation: rotation .6s infinite linear;
		   -moz-animation: rotation .6s infinite linear;
		   -o-animation: rotation .6s infinite linear;
		   animation: rotation .6s infinite linear;
		   border-left:6px solid rgba(0,174,239,.15);
		   border-right:6px solid rgba(0,174,239,.15);
		   border-bottom:6px solid rgba(0,174,239,.15);
		   border-top:6px solid rgba(0,174,239,.8);
		   border-radius:100%;
		}

		@-webkit-keyframes rotation {
		   from {-webkit-transform: rotate(0deg);}
		   to {-webkit-transform: rotate(359deg);}
		}
		@-moz-keyframes rotation {
		   from {-moz-transform: rotate(0deg);}
		   to {-moz-transform: rotate(359deg);}
		}
		@-o-keyframes rotation {
		   from {-o-transform: rotate(0deg);}
		   to {-o-transform: rotate(359deg);}
		}
		@keyframes rotation {
		   from {transform: rotate(0deg);}
		   to {transform: rotate(359deg);}
		}

		@media screen and (max-width: 2560px){
			.overall-div{
				width: 80%;
				margin-left: 180px;
			}
		}
		
		@media screen and (max-width: 1440px){
			.overall-div{
				width: 100%;
				margin-left: 0px;
			}
		}

		@media screen and (max-width: 425px){

		}

		@media screen and (max-width: 375px){
			.picklist-detail{
				font-size: 12px;
			}
			#barcode-scanner video{
				width: 240px;
			}
		}

		@media screen and (max-width: 370px){
			.packed-qty{
				padding-left: 0px;			}
			.picklist-detail{
				font-size: 12px;
			}
			.close-carton-desc{
				margin-left: -18px!important;
			}
			.unpack-pl-desc{
				margin-left: -7px!important;
			}
			.undo-scan{
				margin-left: -20px
			}
			.post{
				margin-left: -15px
			}
		}

		@media screen and (max-width: 320px){

		}

		.picklist-detail{
			border: 0px;
		}
		#upc-table tbody tr td{
			border: 0px;
		}
		.undo-btn:hover{
			text-decoration: underline;
		}
		.post-btn:hover{
			text-decoration: underline;
		}
		.scan-recv-btn:hover{
			text-decoration: underline;
		}
		.null-loc:hover{
			cursor: pointer;
			text-decoration: underline;
		}
		.null-loc{
			color: #3989C8;
		}
	</style>
	<body style="background: #FFFFFF">
		<section class="wrapper wrapper-content" style="background: #FFFFFF">
			<div class="spinner" style="display: none"></div>
			<div align="center" class="overall-div" style="background: #DEDEE0; margin-top: 20px">
				
				<div class="container" style="margin-top: -20px;background: #DEDEE0" align="center">
					<div class="form-horizontal">
						<div class="row">
							<div class="col-6" style="margin-top: 20px">
								<div class="form-group">
									<span style="font-size: 12px">Total Received Qty:</span> 
									<label class="form-control-label tot-rec-qty" style="font-weight: bold"><?= number_format($total_recv, 0, ".", "") ?></label>
								</div>
							</div>
							<div class="col-6 packed-qty" style="margin-top: 20px">
								<div class="form-group">
									<span style="font-size: 12px">Status:</span> 
									<label class="form-control-label scn-status" style="font-weight: bold">Scanning</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="container" style="background: #DEDEE0;" align="center">
					<div class="form-horizontal" >
						<div class="row">
							<div class="col-3">
								<a href="<?= DOMAIN ?>scan_received">
									<div class="form-group" style="user-select: none; cursor: pointer">
										<span class="fa fa-arrow-left" style="color: #3989C8; font-size: 20px"></span>
										<div style="font-size: 12px; color: #000000">
											Back
										</div>
									</div>
								</a>
							</div>
							<div class="col-3" style="margin-left: 5px">
								<div class="form-group scan-recv-btn" style="user-select: none; cursor: pointer">
									<img src="<?= IMG_DIR ?>scan-received-icon.png" alt="Scan Received" class="scan-recv" height="20" width="30" style="margin-left: -30px">
									<div style="font-size: 12px; margin-left: -26px">
										Scan Received
									</div>
								</div>
							</div>
							<div class="col-3" style="margin-left: -10px">
								<div class="form-group undo-btn" style="user-select: none; cursor: pointer">
									<img src="<?= IMG_DIR ?>undo-scan.png" alt="Unpack PL" height="20" width="30" class="undo-scan">
									<div style="font-size: 12px" align="center" class="close-carton-desc">
										Undo Scan
									</div>
								</div>
							</div>
							<div class="col-3" style="margin-left: 5px">
								<div class="form-group post-btn" style="user-select: none; cursor: pointer">
									<span class="fa fa-calendar-check-o" style="color: #3989C8; font-size: 20px; margin-left: -20px"></span>
									<div style="font-size: 12px; color: #212529; margin-left: -26px">
										Post Receive
									</div>
								</div>
							</div>	
						</div>
					</div>
					<hr style="background-color: #FFFFFF; margin-top: -5px">
				</div>

				

				<div class="container" align="center" style="background: #FFFFFF">
					<div class="table-responsive">
						<table class="table table-borderless picklist-detail">
							<tbody>
								<tr>
									<td style="width: 10%; padding-right: 0px">Type:</td>
									<td style="width: 30%; font-weight: bold"><?= $_GET['recvtype'] ?></td>
									<td style="width: 25%">Destination:</td>
									<td style="width: 25%; font-weight: bold"><?= $location ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="container" align="center" style="margin-top: 10px; background: #DEDEE0">
					<div class="table-responsive">
						<table class="table table-borderless" id="upc-table">
							<tbody>
								<tr>
									<td style="width: 5%; padding-top: 20px">UPC:</td>
									<td class="text-center" style="width: 95%; padding-left: 0px">
										<input type="text" class="form-control" style="width: 100%; font-size: 10px" name="FK_IM_UPCCode" maxlength="13" oninput="maxLengthCheck(this)">
									</td>
									<?php if (USE_MOBILE_CAMERA == 1): ?>
										<td>
											<button class="btn btn-primary" id="scanButton"><span class="fa fa-camera"></span></button>
										</td>
									<?php endif ?>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="form-horizontal" style="margin-top: -30px">
						<div class="row">
							<div class="col-6">
								<div class="table-responsive">
									<table class="table table-borderless">
										<tbody>
											<tr>
												<td style="width: 10%">Qty:</td>
												<td style="padding-left: 5px; width: 90%; padding-right: 25px">
													<input type="number" class="form-control" value="1" min="1" style="width: 100%; font-size: 10px" name="Barcode_Qty" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="3" oninput="maxLengthCheck(this)">
												</td>
											</tr>
										</tbody>
									</table>
								</div>		
							</div>
						</div>
					</div>
				</div>

				<div class="container" align="center" style="background: #FFFFFF">
					<div class="table-responsive" style="height: 300px; max-height: 300px">
						<table class="table table-bordered table-condensed table-hover" style="margin-top: 20px; width: 100%" id="received-detail-list">
							<thead>
								<tr>
									<th style="width: 40%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Barcode</th>
									<th style="width: 30%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Item Code</th>
									<th style="width: 20%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Location</th>
									<th style="width: 10%; background: #7DC3F4; font-size: 12px; padding-top: 1px; padding-bottom: 1px; vertical-align: middle!important;">Received<br>Qty</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($details as $key => $detail): ?>
									<tr class="received-details">
										<td style="font-size: 12px; padding: 5px" data-actual-location="<?= $detail['Location_Source'] ?>"><?= $detail['SI_UPCCode'] ?></td>
										<td style="font-size: 12px; padding: 5px"><?= $detail['PKFK_InvMaster_Code'] ?></td>
										<?php if($loc_type == 'STR'): ?>
											<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;"><?= $detail['Location_Source'] ?></td>
										<?php else: ?>
											<?php if ($detail['IM_ItemLocation'] !== NULL): ?>
												<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;"><?= $detail['Location_Source'] ?></td>
											<?php else: ?>
												<td style="font-size: 12px; padding: 5px; vertical-align: middle!important;" class="null-loc" data-item-code="<?= $detail['PKFK_InvMaster_Code'] ?>"><?= $detail['Location_Source'] ?></td>
											<?php endif; ?>
										<?php endif; ?>
										<td class="text-right" style="font-size: 12px; padding: 5px"><?= number_format($detail['SI_ScanQty'], 0, ".", "") ?></td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
					<hr>
				</div>
			</div>
			<audio id="audio" src="<?= DOMAIN ?>audio/errmsg.wav"></audio>
		<?php $this->load->view('scan_received/bin-location-template') ?>
		<?php if (USE_MOBILE_CAMERA == 1): ?>
			<?php $this->load->view('scan_received/scan-barcode-template'); ?>
		<?php endif ?>
		</section>
		<script>
			var ref_doc 	= '<?= $_GET['ref'] ?>';
			var remarks 	= '<?= $_GET['remarks'] ?>';
			var pack_no 	= '<?= $_GET['pack_no'] ?>';
			var recv_fr 	= '<?= $_GET['recvfrom'] ?>';
			var recv_type 	= '<?= $_GET['recvtype'] ?>';
			var loc_type 	= '<?= $loc_type ?>';
			var bin_list 	= <?= json_encode($bin_list) ?>;
		</script>
		<script src="<?= JS_DIR ?>app/scan_received/form.js"></script>
		<script src="<?= EXTENSION_JS ?>quagga/dist/quagga.js"></script>
	</body>