
	<style>
		@media screen and (max-width: 375px){
			.receive-detail{
				font-size: 12px;
			}
		}
	</style>
	<body style="background: #DEDEE0">
		<section class="wrapper wrapper-content">

			<div class="container">
				<div class="row">
					<div class="table-responsive" style="margin-left: 20px; margin-top: 20px">
						<table class="table table-responsive" id="workstation-tbl">
							<tbody>
								<tr>
									<td style="font-weight: bold; width: 40%; font-size: 12px">Work Station ID:</td>
									<td style="width: 60%; font-size: 14px"><?= $wsid ?></td>
								</tr>
								<tr>
									<td style="font-weight: bold; width: 40%; font-size: 12px">Receiving Date:</td>
									<td style="width: 60%; font-size: 14px"><?= date('m/d/Y') ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="container" style="background: #FFFFFF">
				
				<div class="table-responsive">
					<table class="table table-borderless receive-detail" style="margin-top: 10px">
						<tbody>
							<tr>
								<td style="font-weight: bold; width: 35%; padding-top: 20px">Receiving Type:</td>
								<td class="text-left" style="padding-left: 0px">
									<select class="select-recv-type form-control">
										<option></option>
										<?php foreach ($transtype_list as $key => $list): ?>
											<option value="<?= $list['TT_Code'] ?>"><?= $list['TT_Code'] ?></option>
										<?php endforeach ?>
									</select>
								</td>
							</tr>
							<tr>
								<td style="font-weight: bold; padding-top: 20px">Received From:</td>
								<td class="text-left" style="padding-left: 0px">
									<select class="select-recv-from form-control" disabled>
										<option></option>
									</select>
								</td>
							</tr>
							<tr>
								<td style="font-weight: bold; padding-top: 20px">Pack No.:</td>
								<td class="text-left" style="padding-left: 0px">
									<input type="number" class="form-control" name="Pack_No" min="1" step="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="3" oninput="maxLengthCheck(this)">
								</td>
							</tr>
							<tr>
								<td style="font-weight: bold; padding-top: 20px">Reference No.:</td>
								<td class="text-left" style="padding-left: 0px">
									<input type="text" class="form-control" name="Ref_No">
								</td>
							</tr>
							<tr>
								<td style="font-weight: bold; padding-top: 20px">Remarks:</td>
								<td class="text-center" style="padding-left: 0px">
									<textarea name="" cols="30" rows="2" class="form-control" id="Doc_Remarks"></textarea>
								</td>
							</tr>
						</tbody>
					</table>	
				</div>

				<div align="center" style="margin-top: 30px">
						<a role="button" href="<?= DOMAIN ?>user/menu" class="btn text-light" style="border-radius: 10px; width: 40%; background: #B5B5C1; margin-bottom: 20px">Back</a>
						<button class="btn btn-primary" id="next" style="border-radius: 10px; width: 40%; margin-bottom: 20px">Next</button>
				</div>
					
					

			</div>
		</section>
		<script src="<?= JS_DIR ?>app/scan_received/index_01042021_0515PM.js"></script>
	</body>