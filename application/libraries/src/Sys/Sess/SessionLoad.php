<?php
namespace Sys\Sess;

class SessionLoad{
	public function __construct(){
		if (session_status() == PHP_SESSION_NONE) session_start();
	}
	public function isLogin(){
		if(isset($_SESSION['current_user'])){
			return true;
		}
		else{
			return false;
		}
	}
	public function setLogin($data/*, $displayname*/){
		$_SESSION['current_user']['login-user'] 		= $data['user'];
		$_SESSION['current_user']['login-name'] 		= $data['username'];
		$_SESSION['current_user']['login-company'] 		= $data['company'];
		$_SESSION['current_user']['login-location'] 	= $data['location'];
		$_SESSION['current_user']['login-mask'] 		= $data['loc-mask'];
		$_SESSION['current_user']['login-wsid'] 		= $data['workstation-id'];
		$_SESSION['current_user']['login-db'] 			= $data['database'];
	}	
	public function clearLogin(){
		unset($_SESSION['current_user']);
	}
}